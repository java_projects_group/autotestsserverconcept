package web_server;

import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.Configuration.ClassList;

import file_server.CacheFile;
import file_server.FileCacheStorage;

class RequestHandlerEx extends AbstractHandler {
    /** **/
    final protected FileCacheStorage fileStorage = FileCacheStorage.getInstance();
    /** **/
    final String body;
    
    public RequestHandlerEx()  {
        this("HelloHandler class");
    }

    public RequestHandlerEx(String body) {
        this.body = body;
    	System.out.println("RequestHandler class created.");
    }
    
    public void handle(String target,
			           Request baseRequest,
			           HttpServletRequest request,
			           HttpServletResponse response) throws IOException,ServletException 
    {	
    	String fileName = target.substring(1);
    	System.out.println("File requested : " + fileName);
    	
    	if (true == this.fileStorage.isFileCached(fileName)) {
    		// TODO: Log something
		} 
    	
        CacheFile file = this.fileStorage.getFile(fileName);
        //System.out.println(file.bytesBufferLength() + " bytes read.");
        
        // obtains ServletContext
        ServletContext context = request.getServletContext();
        // gets MIME type of the file
        String mimeType = context.getMimeType(file.getAbsolutePath());
        if (mimeType == null) {        
            // set to binary type if MIME mapping not found
            mimeType = "application/octet-stream";
        }
        //System.out.println("MIME type: " + mimeType);
    	
        //mimeType = "application/octet-stream";
        
        response.setContentType(mimeType);
        response.setContentLength((int) file.bytesBufferLength());
        response.setStatus(HttpServletResponse.SC_OK);
      
        // obtains response's output stream
        OutputStream outStream = response.getOutputStream();
        outStream.write(file.getFileBytes(), 0, (int)file.bytesBufferLength());
        outStream.close();
        
        baseRequest.setHandled(true);
	} 
}

public class FileServerHttpOnly {
	public static void main(String[] args) throws Exception {
		// Initialize file storage:
		FileCacheStorage fileStorage = FileCacheStorage.getInstance();
		fileStorage.InitTest();
		
        final int httpPort = 80;
  
	    // Create a basic jetty server object without declaring the port. Since we 
        // are configuring connectors directly we'll be setting ports on those connectors.
        Server server = new Server();

        // HTTP Configuration:
        // HttpConfiguration is a collection of configuration information appropriate for http and https. The default scheme for http
        // is <code>http</code> of course, as the default for secured http is <code>https</code> but we show setting the scheme to show
        // it can be done. The port for secured communication is also set here.
        HttpConfiguration http_config = new HttpConfiguration();
        
        // HTTP connector:
        // The first server connector we create is the one for http, passing in the http configuration we configured above so it
        // can get things like the output buffer size, etc. We also set the port (8080) and configure an idle timeout.
        ServerConnector http = new ServerConnector(server, new HttpConnectionFactory(http_config));
        http.setPort(httpPort);
        http.setHost("100.114.17.254");
        http.setIdleTimeout(30000);

        // Set the connectors
        server.setConnectors(new Connector[] { http });
        
        // Set 'RequestHandler' to handle requests to 'FileListPage' context path:
        ContextHandler context = new ContextHandler("/resources");
        context.setHandler(new RequestHandlerEx());

        ContextHandlerCollection contexts = new ContextHandlerCollection();

		/** Creating the WebAppContext for the created content: **/
		WebAppContext webAppsContext = new WebAppContext();
		webAppsContext.setResourceBase("webapps");
		webAppsContext.setContextPath("/");

		/** Including the JSTL jars for the webapp: **/
		webAppsContext.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",".*/[^/]*jstl.*\\.jar$");

		/** Enabling the Annotation based configuration: **/
		ClassList classlist = ClassList.setServerDefault(server);
		classlist.addAfter("org.eclipse.jetty.webapp.FragmentConfiguration",
						   "org.eclipse.jetty.plus.webapp.EnvConfiguration",
						   "org.eclipse.jetty.plus.webapp.PlusConfiguration");
		classlist.addBefore("org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
							"org.eclipse.jetty.annotations.AnnotationConfiguration");
        
        /** Set a handlers: **/
		contexts.setHandlers(new Handler[] {context, webAppsContext});
        server.setHandler(contexts);

        // Start the server
        server.start();
        server.join();
	}
}
