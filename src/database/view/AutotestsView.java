//============================================================================
// Name        : AutotestsView.java
// Created on  : November 28, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Autotests view class
//============================================================================

package database.view;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import database.entry.AutotestsRecord;
import database.entry.TestStatus;
import database.entry.TestType;
import utilities.Utilities;

/** @author a.tokmakov **/
/** @class  AutotestsView. **/
public class AutotestsView {
	/** AutotestsRecord database entry instance. **/
	protected AutotestsRecord autotests_record = null;
	/** List of passed tests: **/
	protected ArrayList<String> testsPassed = new ArrayList<String>();
	/** List of failed tests: **/
	protected ArrayList<String> testsFailed = new ArrayList<String>();	
	/** Application level logger. **/
	protected final Logger applicationLogger = LogManager.getLogger("ApplicationLogger");
	
	/** AutotestsView class default constructor. **/
	public AutotestsView() {
		//TODO: Do something
	}
	
    /**
     * BuildView class constructor
     *
     * @param rec_autotest:
     *        AutotestsRecord class instance
     */		
	public AutotestsView(final AutotestsRecord rec_autotest) {
		this.autotests_record = rec_autotest;
	}
	
    /**
     * Initialize the inner lists of passed and failed tests.
     *
     * @return True in case of successful operation.
     * 		   False in case of any kind error.
     */
	public boolean parserResults()  {
		if (TestStatus.TestsCompleted != this.autotests_record.getTestStatus())
			return true;
		try {
			final JSONObject jsonObj = (JSONObject)new JSONParser().parse(this.autotests_record.getResultJson());
			JSONArray test_passed = (JSONArray)jsonObj.get("passed");
			JSONArray test_failed = (JSONArray)jsonObj.get("failed");		
			Arrays.asList(test_passed.toArray()).forEach(enrty -> testsPassed.add((String)enrty));
			Arrays.asList(test_failed.toArray()).forEach(enrty -> testsFailed.add((String)enrty));
			return true;
		} catch (final ParseException exception) {
			this.applicationLogger.error("Failed to parser JSON test results for AutotestsRecord record with UUID = " + autotests_record.getUuid());
			this.applicationLogger.error(Utilities.stackTraceToString(exception));
			return false;
		}
	}
	
	/**  @return the uuid. **/
	public String getUuid() {
		return this.autotests_record.getUuid();
	}
	
	/** @return the worker name. **/
	public String getWorkerName() {
		return this.autotests_record.getWorkerName();
	}
	
	/** @return the end_time. **/
	public LocalDateTime getEndTime() {
		return this.autotests_record.getEndTime();
	}

	/** @return the end_time. **/
	public LocalDateTime getStartTime() {
		return this.autotests_record.getStartTime();
	}
	
	/** @return the tests status. **/
	public TestStatus getTestStatus() {
		return this.autotests_record.getTestStatus();
	}

	/** @return the tests type. **/
	public TestType getTestsType() {
		return this.autotests_record.getTestsType();
	}
	
	/** @return the os. **/
	public String getOs() {
		return this.autotests_record.getOs();
	}

	/** @return the count of passed tests. **/
	public long getTestsPassed() {
		return this.autotests_record.getTestsPassed();
	}

	/** @return the count of failed tests. **/
	public long getTestsFailed() {
		return this.autotests_record.getTestsFailed();
	}

	/** @return the count of skipped tests. **/
	public long getTestsSkipped() {
		return this.autotests_record.getTestsSkipped();
	}
	
    /** Overrides toString() Object method:  **/
    @Override 
    public String toString() {
    	String text = "{ uuid : "  + this.getUuid() + 
		                 ",\n status : "  + this.getTestStatus() + 
		                 ", type : "  + this.getTestsType() + 				
		                 ", worker_name : "  + this.getWorkerName() + 
		                 ", os : "  + this.getOs() + 
		                 ",\n start_time : " + this.getStartTime() + 
		                 ", end_time : " + this.getEndTime();
         if (TestStatus.TestsCompleted == this.autotests_record.getTestStatus()) {
        	 text += ", \nTest resulsts: [Passed: " + this.getTestsPassed() + 
        			 				   ", Failed:" + this.getTestsFailed() + "]";
         }   
         return text + "}";
    }
	
	/** Overrides equals() Object method:  **/
	@Override 
	public boolean equals(Object obj) {
		AutotestsView toCompareWith = (AutotestsView)obj;
		return this.autotests_record.equals(toCompareWith.autotests_record);
	}
	
	public List<String> getPassedTests() {
		return this.testsPassed;
	}
	
	public List<String> getFailedTests() {
		return this.testsFailed;
	}
}
