//============================================================================
// Name        : UnittestsView.java
// Created on  : November 29, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Autotests view class
//============================================================================

package database.view;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import database.entry.UnitTestsRecord;
import utilities.Utilities;

/** @author a.tokmakov **/
/** @class  UnittestsView. **/
public class UnittestsView {
	/** UnitTestsRecord database entry instance. **/
	protected UnitTestsRecord unittest_record = null;
	/** List of passed tests: **/
	protected ArrayList<String> testsPassed = new ArrayList<String>();
	/** List of failed tests: **/
	protected ArrayList<String> testsFailed = new ArrayList<String>();	
	/** Application level logger. **/
	protected final Logger applicationLogger = LogManager.getLogger("ApplicationLogger");
	
	/** UnittestsView class default constructor. **/
	public UnittestsView() {
		//TODO: Do something
	}
	
    /**
     * BuildView class constructor
     *
     * @param rec_autotest:
     *        UnitTestsRecord class instance
     */		
	public UnittestsView(final UnitTestsRecord rec_autotest) {
		this.unittest_record = rec_autotest;
	}
	
    /**
     * Initialize the inner lists of passed and failed tests.
     *
     * @return True in case of successful operation.
     * 		   False in case of any kind error.
     */
	public boolean parserResults()  {
		try {
			final JSONObject jsonObj = (JSONObject)new JSONParser().parse(this.unittest_record.getResultJson());
			final Set<?> keys = jsonObj.keySet();
			for (final Object obj : keys) {
				final JSONObject results = (JSONObject)jsonObj.get(obj);
				
				String testName = (String)obj;
				//long passed = (long) results.get("passed");
				long failed = (long) results.get("failed");
				//long skipped = (long) results.get("skipped");
				
				if (0 == failed)
					this.testsPassed.add(testName);
				else
					this.testsFailed.add(testName);
			} 
			return true;
		} catch (final java.lang.ClassCastException castException) {
			this.applicationLogger.error(Utilities.stackTraceToString(castException));
			return false;		
		} catch (final ParseException parseException) {
			this.applicationLogger.error("Failed to parser JSON test results for UnitTestsRecord record with UUID = " + unittest_record.getUuid());
			this.applicationLogger.error(Utilities.stackTraceToString(parseException));
			return false;
		}
	}
	
	public String getUuid() {
		return this.unittest_record.getUuid();
	}
	
	public String getWorkerName() {
		return this.unittest_record.getWorkerName();
	}
	
	public LocalDateTime getStartTime() {
		return this.unittest_record.getStartTime();
	}
	
	public LocalDateTime getEndTime() {
		return this.unittest_record.getEndTime();
	}
	
	/** @return the count of passed tests. **/
	public long getTestsPassed() {
		return this.unittest_record.getTestsPassed();
	}

	/** @return the count of failed tests. **/
	public long getTestsFailed() {
		return this.unittest_record.getTestsFailed();
	}

	/** @return the count of skipped tests. **/
	public long getTestsSkipped() {
		return this.unittest_record.getTestsSkipped();
	}
	
	/** Overrides toString() Object method:  **/
	@Override 
	public String toString() {
    	String text = "{ uuid : "  + this.getUuid() + 
				", worker_name : "  + this.getWorkerName() + 
				",\n start_time : " + this.getStartTime() + 
				", end_time : " + this.getEndTime();
		text += ", \nTest results: [Passed: " + this.getTestsPassed() + 
					 			  ", Failed:" + this.getTestsFailed()  + 
					 			  ", Skipped:" + this.getTestsSkipped() + "]";
		return text + "}";
	}
	
	/** Overrides equals() Object method:  **/
	@Override 
	public boolean equals(Object obj) {
		return this.unittest_record.equals(((UnittestsView)obj).unittest_record);
	}
}
