//============================================================================
// Name        : BrowserBuilds.java
// Created on  : November 29, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : BrowserBuilds view class
//============================================================================

package database;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import database.entry.AutotestsRecord;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.entry.UnitTestsRecord;
import database.tables.AutotestsTable;
import database.tables.BuildsTable;
import database.tables.UnitTestsTable;
import database.view.AutotestsView;
import database.view.BuildView;
import database.view.UnittestsView;
import utilities.observer.Observable;

/** @author a.tokmakov **/
/** @class  BrowserBuilds. **/
public class BrowserBuilds extends Observable {
	/** Static BrowserBuilds class instance : **/
	private static volatile BrowserBuilds __instance = null;
	/** Table content. **/
	/** Synchronized (thread-safe) list backed by the ArrayList<BuildView>. **/
	protected List<BuildView> buildsList = Collections.synchronizedList(new ArrayList<BuildView>());
	/** BrowserBuilds content sorter. **/
	protected final BuildViewComparator comparator = new BuildViewComparator();
	/** Application level logger. **/
	protected final Logger applicationLogger = LogManager.getLogger("ApplicationLogger");
	
	/** Builds table. **/
	private BuildsTable buildsTable = null;
	/** Autotests table. **/
	private AutotestsTable autotestsTable = null;
	/** UnitTests table.**/
	private UnitTestsTable unitTestsTable = null;

	
	/** EntryComparator class : **/
	class BuildViewComparator implements Comparator<BuildView> {
		@Override
		public int compare(final BuildView build1, 
						   final BuildView build2) {
			return build2.getStartTime().compareTo(build1.getStartTime());
		}
	}
	
	/** BrowserBuilds the singleton table instance : **/  
	public static BrowserBuilds getTable() {
		if (null == __instance) {
			synchronized (BrowserBuilds.class) {
				if (null == __instance) {
					__instance = new BrowserBuilds();
				};
			}
		} return __instance;
	}

	/** BrowserBuilds default constructor. **/
	protected BrowserBuilds() {
		// TODO: Do something
		// TODO: Add application level logging
	}
	
    /**
     * Initialize the browser builds container.
     *
     * @return True in case of success, False otherwise
     */	
	public boolean Initialize() {
		// FIXME: Move tables Initialization somewhere else, maybe?
		//Initialize the BuildsTable table:
		this.buildsTable = BuildsTable.getTable();
		// Initialize the Autotests table:
		this.autotestsTable = AutotestsTable.getTable();
		//Initialize the UnitTests table:
		this.unitTestsTable = UnitTestsTable.getTable();
		
		// Read the entire builds table. For each specific build's 
		// the 'uuid' field parameter value we obtain the corresponding
		// auto and unit tests and link them to the current build object.
		for (final BuildRecord build_record : this.buildsTable.getList()) {
			BuildView build = new BuildView(build_record);
			
			/** Getting the UnitTest for this build. **/
			final List<UnitTestsRecord> unitTestsList = unitTestsTable.getRecordsByUUiD(build_record.getUuid());
			for (final UnitTestsRecord unit_test_record: unitTestsList) {
				UnittestsView unit_test_view = new UnittestsView(unit_test_record);
				if (true == unit_test_view.parserResults()) {
					build.addUnittest(unit_test_view);
				} else  {
					// TODO. Add Application level logs
				}
			}
			
			/** Getting the AutoTests for this build. **/
			final List<AutotestsRecord> autoTestsList = autotestsTable.getRecordsByUUiD(build_record.getUuid());
			for (final AutotestsRecord auto_test_record: autoTestsList) {
				AutotestsView autotest_view = new AutotestsView(auto_test_record);
				if (true == autotest_view.parserResults()) {
					build.addAutotest(autotest_view);
				} else  {
					// TODO. Add Application level logs
				}
			}
			
			applicationLogger.debug("Adding Build [uuid: " + build.getUuid() + ", isNightly: " + build.isNightly() + 
									", StartTime: " + build.getStartTime() + "] to builds table.");
			
			this.buildsList.add(build);
		}
		return true;
	}

	/***** JUST FOR TESTS. DELETE IT. *****/
	public void TEST() {
		for (final BuildView build: this.buildsList)
		{
			System.out.println("ID      : " + build.getId());
			System.out.println("UUid    : " + build.getUuid());
			System.out.println("Nightly : " + build.isNightly());
			
			System.out.println("Unit tests size : " + build.getUnittests().size());
			System.out.println("Unit auto size  : " + build.getAutotests().size());
		}
	}
	
	/** Checks if key of the build already exists in table. **/
	public boolean isEntryExists(final BuildView rec) {
		return this.buildsList.stream().anyMatch(entry -> entry.equals(rec));
	}
	
	/** Checks if key of the entry already exists in table.(By ID) **/
	public boolean isEntryExists_ByID(final BuildView rec) {
		return this.buildsList.stream().anyMatch(entry -> entry.getId() == rec.getId());
	}	

	/** Checks if key of the entry already exists in table. **/
	public boolean isEntryExists_ByUUID(final BuildView rec) {
		return this.buildsList.stream().anyMatch(entry -> 0 == entry.getUuid().compareTo(rec.getUuid()));
	}
	
	/** Adds record to cache. **/
	public boolean add(final BuildView entry) {
		if (true == this.isEntryExists(entry))
			return false;
		this.buildsList.add(entry);
		this.buildsList.sort(this.comparator);
		return true;
	}
	
    /**
     * Updates the element at the specified element
     *
     * @param  build, build to be updated with the corresponding uuid value
     * @return True in case of success, False otherwise
     * @throws UnsupportedOperationException if the {@code set} operation
     *         is not supported by this list
     * @throws ClassCastException if the class of the specified element
     *         prevents it from being added to this list
     * @throws NullPointerException if the specified element is null and
     *         this list does not permit null elements
     * @throws IllegalArgumentException if some property of the specified
     *         element prevents it from being added to this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */	
	public boolean update(final BuildView build) {
		if (false == this.isEntryExists(build))
			return false;

		final int index = this.buildsList.indexOf(build);
		if (-1 == index) {
			//TODO: Report this shit. No way this could happen!!
			return false;
		}
		this.buildsList.set(index, build);
		this.buildsList.sort(this.comparator);
		return true;
	}
	
	/** Clears the table cached data. **/	
	public void clear() {
		this.buildsList.clear();
	}
	
	/** Return TRUE if cache is empty and FALSE otherwise **/
	public boolean isEmpty() {
		return this.buildsList.isEmpty();
	}
	
    /**
     * Returns the number of elements in this list.  If this list contains
     * more than {@code Integer.MAX_VALUE} elements, returns
     * {@code Integer.MAX_VALUE}.
     * @return the number of elements in this list
     */	
	public int Size() {
		return this.buildsList.size();
	}
	
    /**
     * Returns the cache elements depending of BuildView.equals() result.
     *
     * @param  build - the BuildView object
     * @return BuildView object if its found.
     *         If no matches founds returns NULL. 
     */	
	public BuildView get(final BuildView build) {
		for (final BuildView element : buildsList)
			if (true == element.equals(build))
				return element;
		return null;
	}
	
    /**
     * Returns the cache elements identified by uuid value.
     *
     * @param  uuid - the BuildView uuid value
     * @return BuildView object if its found.
     *         If no matches founds returns NULL. 
     */		
	public BuildView get(final String uuid) {
		for (BuildView build : buildsList)
			if (0 == build.getUuid().compareTo(uuid))
				return build;
		return null;
	}
	
    /**
     * Returns the element at the specified position in this list.
     *
     * @param  index index of the element to return
     * @return the element at the specified position in this list
     * 		   Null in case if index value is incorrect
     */	
	public BuildView get(int index) {
		try {
			return this.buildsList.get(index);
		} catch (final IndexOutOfBoundsException exc) {
			return null;
		}
	}
	
    /**
     * Returns the cached records list
     *
     * @return the list of elements.
     */	
	public final List<BuildView> getList() {
		return this.buildsList;
	}
	
    /**
     * Returns the First cached element.
     *
     * @return the first element if cache collection.
     * 		   Null in case if cache is cold (empty).
     */		
	public BuildView getFirstElement() {
		if (true == this.buildsList.isEmpty())
			return null;
		return this.buildsList.get(0);
	}
	
    /**
     * Returns the Last cached element.
     *
     * @return the Last element if cache collection.
     * 		   Null in case if cache is cold (empty).
     */		
	public BuildView getLastElement() {
		if (true == this.buildsList.isEmpty())
			return null;
		return this.buildsList.get(this.buildsList.size() - 1);
	}
	
    /**
     * Returns the Last N cached elements.
     *
     * @return the list of N last cached elements.
     * 		   Null in case if cache is cold (empty) or N value exceed the cache size.
     */		
	public List<BuildView> getNLastElements(int n) {
		if (true == this.buildsList.isEmpty() || n > this.buildsList.size())
			return null;
		return this.buildsList.subList(buildsList.size() - n, buildsList.size());
	}
	
	   /**
     * Returns the First N cached elements.
     *
     * @return the list of N first cached elements.
     * 		   Null in case if cache is cold (empty) or N value exceed the cache size.
     */		
	public List<BuildView> getNFirstElements(int n) {
		if (true == this.buildsList.isEmpty() || n > this.buildsList.size())
			return null;
		return this.buildsList.subList(0, n);
	}	
	
    /**
     * Returns the records list with given UUID value.
     *
     * @return the list of records
     */		
	public List<BuildView> getRecordsByUUiD(final String uuid) {
		return this.buildsList.stream().filter(build -> build.getUuid().equals(uuid)).collect(Collectors.toList());
		//return this.buildsList.stream().filter(build -> 0 == build.getUuid().compareTo(uuid)).collect(Collectors.toList());
	}	

    /**
     * Write some applicator level logs with test
     * completion information
     *
     * @return none
     */	
	private void putTescCompletionLogs(final BuildView buildView) {
		this.applicationLogger.debug("============================================ Tests completed ============================================");
		this.applicationLogger.debug("BrowserBuilds: BuildView updated:\n" + buildView);
		
		final List<UnittestsView> unittests = buildView.getUnittests();
		for (UnittestsView unit_test_view: unittests) {
			this.applicationLogger.debug("Worker: " + unit_test_view.getWorkerName() + 
									     ". Start time: " + unit_test_view.getStartTime() +  ". End time: " + unit_test_view.getEndTime() + 
									     "\nResults: Passed: " + unit_test_view.getTestsPassed() + 
									     ", Failed: " + unit_test_view.getTestsFailed()+ 
									     ", Skipped: " + unit_test_view.getTestsSkipped());
		}
		
		final List<AutotestsView> autotests = buildView.getAutotests();
		for (AutotestsView auto_test_view: autotests)  {
			this.applicationLogger.debug("Worker: " + auto_test_view.getWorkerName() + 
				     ". Start time: " + auto_test_view.getStartTime() +  ". End time: " + auto_test_view.getEndTime() + 
				     ", OS: " + auto_test_view.getOs() + 
				     "\nResults: Passed: " + auto_test_view.getPassedTests().size()+ 
				     ", Failed: " + auto_test_view.getFailedTests().size());
			
		}
		this.applicationLogger.debug("=========================================================================================================");
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public boolean addBuild(final BuildRecord record) {
		final BuildView buildView = new BuildView(record);
		if (true == isEntryExists_ByUUID(buildView)) {
			this.applicationLogger.error("BrowserBuilds: " + "BuildView with uuid = '" + buildView.getUuid() + "' already exists.");
			return false;
		}
		
		this.buildsList.add(buildView);
		this.applicationLogger.debug("BrowserBuilds: BuildRecord added:\n" + buildView);
		
		/** Notify oObservers.**/
		this.notifyObservers(buildView);
		
		// Return TRUE
		return true;		
	}
	
	// TODO: Add description
	public boolean updateBuild(final BuildRecord record) {
		final BuildView newBuildView = new BuildView(record);
		BuildView buildView = this.get(newBuildView.getUuid());
		if (null == buildView) {
			this.applicationLogger.error("BrowserBuilds: BuildView with uuid = '" + newBuildView.getUuid() + "' do not exists yet in table.");
			return false;
		}
		
		buildView.setStatus(newBuildView.getStatus());
		if (BuildStatus.TestsCompleted == buildView.getStatus()) {
			buildView.setEndTime(newBuildView.getEndTime());
			this.putTescCompletionLogs(buildView);
		} else {
			this.applicationLogger.debug("BrowserBuilds: BuildView updated:\n" + buildView);
		}

		/** Notify oObservers.**/
		this.notifyObservers(buildView);
		
		// Return TRUE
		return true;		
	}
	
	
	
	public boolean addAutoTest(final AutotestsRecord record) {
		BuildView buildView = this.get(record.getUuid());
		if (null == buildView) {
			this.applicationLogger.error("BrowserBuilds: BuildView with uuid = '" + record.getUuid() + "' do not exists yet in table.");
			return false;
		}
		
		/** Add the auto test view to build.**/
		final AutotestsView autotest = new AutotestsView(record);
		if (true == autotest.parserResults()) {
			this.applicationLogger.debug("AutotestsView JSON results parsed successfully.");
		} else {
			this.applicationLogger.error("Failed to parse AutotestsView JSON results.");
		}
		buildView.addAutotest(autotest);
		
		this.applicationLogger.debug("BrowserBuilds: Autotest added:\n" + autotest);
		
		/** Notify oObservers.**/
		this.notifyObservers(buildView);
		
		// Return TRUE
		return true;	
	}
	
	public boolean updateAutoTest(final AutotestsRecord record) {
		BuildView buildView = this.get(record.getUuid());
		if (null == buildView) {
			this.applicationLogger.error("BrowserBuilds: BuildView with uuid = '" + record.getUuid() + "' do not exists yet in table.");
			return false;
		}
		
		final AutotestsView autotest = new AutotestsView(record);
		if (true == buildView.removeAutotest(autotest)) {
			/** Parse json results: **/
			if (true == autotest.parserResults()) {
				this.applicationLogger.debug("AutotestsView JSON results parsed successfully.");
			} else {
				this.applicationLogger.error("Failed to parse AutotestsView JSON results.");
			}
			/** Add the auto test view to build.**/
			buildView.addAutotest(autotest);
		}
		
		this.applicationLogger.debug("BrowserBuilds: Autotest updated:\n" + autotest);
		
		/** Notify oObservers.**/
		this.notifyObservers(buildView);
		
		// Return TRUE
		return true;	
	}
	
	public boolean addUnitTests(final UnitTestsRecord record) {
		BuildView buildView = this.get(record.getUuid());
		if (null == buildView) {
			this.applicationLogger.error("BrowserBuilds: BuildView with uuid = '" + record.getUuid() + "' do not exists yet in table.");
			return false;
		}
		
		/** Add the unit test view to build.**/
		final UnittestsView unitest = new UnittestsView(record);
		if (true == unitest.parserResults()) {
			this.applicationLogger.debug("UnittestsView JSON results parsed successfully.");
		} else {
			this.applicationLogger.error("Failed to parse UnittestsView JSON results.");
		}
		buildView.addUnittest(unitest);
		this.applicationLogger.debug("BrowserBuilds: Unittest updated:\n" + unitest);
		
		/** Notify oObservers.**/
		this.notifyObservers(buildView);
		
		// Return TRUE
		return true;
	}
	
	// TODO: This shall not be used actually.
	public boolean updateUnitTests(final UnitTestsRecord record) {
		BuildView buildView = this.get(record.getUuid());
		if (null == buildView) {
			this.applicationLogger.error("BrowserBuilds: BuildView with uuid = '" + record.getUuid() + "' do not exists yet in table.");
			return false;
		}
		return true;		
	}
}
