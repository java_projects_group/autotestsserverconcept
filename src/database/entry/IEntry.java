//============================================================================
//Name        : IEntry.java
//Created on  : September 12, 2019
//Author      : Tokmakov Andrey
//Version     : 1.0
//Copyright   : Your copyright notice
//Description : IEntry interface.
//============================================================================

package database.entry;

import java.time.LocalDateTime;

/** @author a.tokmakov **/
/** @interface ITableEntry. **/
public interface IEntry {
	/**  @return the uuid. **/
	public String getUuid();
	
	/** @param uuid the uuid to set. **/
	public void setUuid(final String uuid);
	
	/** @return the id. **/
	public long getId();
	
	/** @param id the id to set. **/
	public void setId(long id) ;
	
	/**  @return the start_time. **/
	public LocalDateTime getStartTime();
	
	/** @param start_time the start_time to set. **/
	public void setStartTime(final LocalDateTime start_time);
	
	/** @return the end_time. **/
	public LocalDateTime getEndTime();
	
	/** @param end_time the end_time to set. **/
	public void setEndTime(final LocalDateTime end_time);
}
