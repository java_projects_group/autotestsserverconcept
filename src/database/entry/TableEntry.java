//============================================================================
//Name        : TableEntry.java
//Created on  : September 12, 2019
//Author      : Tokmakov Andrey
//Version     : 1.0
//Copyright   : Your copyright notice
//Description : TableEntry abstract class definition.
//============================================================================

package database.entry;

import java.time.LocalDateTime;

/** @author a.tokmakov **/
/** @class  TableEntry. **/
public abstract class TableEntry implements IEntry {
	/* ID. */
	protected long id = 0;		
	/* UUID value. */
	protected String uuid = "";
	/* Build start timestamp. */
	protected LocalDateTime start_time = null;
	/* Build end timestamp. */
	protected LocalDateTime end_time = null;
	
	/** @return the id. **/
	@Override
	public long getId() {
		return id;
	}
	
	/** @param id the id to set. **/
	@Override
	public void setId(long id) {
		this.id = id;
	}
	
	@Override
	public String getUuid() {
		// TODO Auto-generated method stub
		return uuid;
	}

	@Override
	public void setUuid(final String uuid) {
		this.uuid = uuid;
	}
	
	@Override
	/**  @return the start_time. **/
	public LocalDateTime getStartTime() {
		return this.start_time;
	}
	
	@Override
	/** @param start_time the start_time to set. **/
	public void setStartTime(final LocalDateTime start_time) {
		this.start_time = start_time;
	}
	
	@Override
	/** @return the end_time. **/
	public LocalDateTime getEndTime() {
		return this.end_time;
	}
	
	@Override
	/** @param end_time the end_time to set. **/
	public void setEndTime(final LocalDateTime end_time) {
		this.end_time = end_time;
	}
	
	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (false == TableEntry.class.isAssignableFrom(obj.getClass())) {
            return false;
        }
        final TableEntry toCompareWith = (TableEntry) obj;
   
        /** If 'uuids' differs return False. **/
        if (false == toCompareWith.getUuid().equals(this.uuid))
        	return false;
        return true;
    }
}
