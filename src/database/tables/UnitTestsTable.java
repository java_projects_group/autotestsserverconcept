//============================================================================
// Name        : UnitTestsTable.java
// Created on  : October 16, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Unit tests table
//============================================================================

package database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import database.entry.UnitTestsRecord;

/** @author a.tokmakov.      **/
/** @class  UnitTestsTable. **/
public class UnitTestsTable extends DatabaseTable<UnitTestsRecord> {
	/** Static UnitTestsTable class instance : **/
	private static volatile UnitTestsTable __instance = null;
	
	/** UnitTests table the singleton table instance : **/  
	public static UnitTestsTable getTable() {
		if (null == __instance) {
			synchronized (UnitTestsTable.class) {
				if (null == __instance) {
					__instance = new UnitTestsTable();
				};
			}
		} 
		return __instance;
	}
	
	/** Sealed (protected) UnitTestsTable class default constructor : **/
	protected UnitTestsTable() {
		super("unit_tests",  "select * from unit_tests;");
		this.readTableFromDB();
	}
	
	/** Checks if key of the entry already exists in table. **/
	protected boolean isEntryKeyExists(final UnitTestsRecord record) {
		return this.cache.isEntryExists_ByID(record);
	}
	
	@Override
	protected void addToTableCache(final ResultSet resultSet) throws SQLException {
		// TODO : Handle SQL error and type cast errors
		UnitTestsRecord record = new UnitTestsRecord();
		
		// Initialize BuildRecord values.
		record.setId(resultSet.getLong(1));
		record.setUuid(resultSet.getString(2));
		record.setWorkerName(resultSet.getString(3));
		record.setStartTime(javaTimeFromSQLTime(resultSet.getTimestamp(4)));
		record.setEndTime(javaTimeFromSQLTime(resultSet.getTimestamp(5)));
		record.setResultJson(resultSet.getString(6));
		record.setTestsPassed(resultSet.getLong(7));
		record.setTestsFailed(resultSet.getLong(8));
		record.setTestsSkipped(resultSet.getLong(9));
		
		// Add to cache:
		this.cache.add(record);
	}
	
	@Override
	protected PreparedStatement buildInsertStatement(final UnitTestsRecord record, 
										  			 final Connection connection) throws SQLException {
		final String sql = "INSERT INTO unit_tests(build_id,uuid,worker_name,start_time,end_time,results,passed,failed,skipped) VALUES(?,?,?,?,?,?,?,?,?);";
		PreparedStatement insertStatement  = connection.prepareStatement(sql);
		insertStatement.setLong(1, record.getId());
		insertStatement.setString(2, record.getUuid());
		insertStatement.setString(3, record.getWorkerName());
		insertStatement.setTimestamp(4, sqlTimeFromJavaTime(record.getStartTime()));
		insertStatement.setTimestamp(5, sqlTimeFromJavaTime(record.getEndTime()));
		insertStatement.setString(6, record.getResultJson());
		insertStatement.setLong(7, record.getTestsPassed());
		insertStatement.setLong(8, record.getTestsFailed());
		insertStatement.setLong(9, record.getTestsSkipped());
		return insertStatement;
	}
	
	@Override
	protected PreparedStatement buildUpdateStatement(final UnitTestsRecord record, 
			 								 	     final Connection connection) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}	
	
	public boolean add(final UnitTestsRecord record) {
		if (true == this.insertRecord(record)) {
			this.cache.add(record);
			this.applicationLogger.debug("UnitTestsTable: Record added.\n" + record);
			this.browserBuilds.addUnitTests(record);
			return true;
		}
		return false;
	}

	/** Read table data. **/
	@Override
	public boolean readTable() {
		return this.readTableFromDB();
	}	

	@Override
	protected long getNextInsertedRecordID() {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void main(String[] args) {
		UnitTestsTable table  = UnitTestsTable.getTable();
		
		UnitTestsRecord testRecort  = new UnitTestsRecord();
		testRecort.setId(234);
		testRecort.setUuid("2203a85b-24f9-4966-b491-a5d5649a4be1");
		
		table.add(testRecort);
	}	
}
