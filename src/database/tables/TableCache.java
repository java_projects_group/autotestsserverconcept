//============================================================================
// Name        : TableCache.java
// Created on  : September 12, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : TableCache class
//============================================================================

package database.tables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import database.entry.TableEntry;

/** @author a.tokmakov. **/
/** @class  TableCache. **/
public class TableCache<Entry extends TableEntry> {
	/** Table content. **/
	/** Synchronized (thread-safe) list backed by the ArrayList<Entry>. **/
	protected List<Entry> recordsList = Collections.synchronizedList(new ArrayList<Entry>());
	/** **/
	protected EntryComparator comparator = new EntryComparator();
	
	/** EntryComparator class : **/
	class EntryComparator implements Comparator<Entry> {
		@Override
		public int compare(final Entry entry1, 
						   final Entry entry2) {
			return entry2.getStartTime().compareTo(entry1.getStartTime());
		}
	}
	
	/** TableCache default constructor. **/
	public TableCache() {
		//TODO: Do something
	}
	
	/** Checks if key of the entry already exists in table. **/
	public boolean isEntryExists(final Entry rec) {
		return recordsList.stream().anyMatch(entry -> entry.equals(rec));
	}
	
	/** Checks if key of the entry already exists in table.(By ID) **/
	public boolean isEntryExists_ByID(final Entry rec) {
		return recordsList.stream().anyMatch(entry -> entry.getId() == rec.getId());
	}	

	/** Checks if key of the entry already exists in table. **/
	public boolean isEntryExists_ByUUID(final Entry rec) {
		return recordsList.stream().anyMatch(entry -> 0 == entry.getUuid().compareTo(rec.getUuid()));
	}
	
	/** Adds record to cache. **/
	public boolean add(final Entry entry) {
		if (true == this.isEntryExists(entry))
			return false;
		this.recordsList.add(entry);
		this.recordsList.sort(this.comparator);
		return true;
	}
	
    /**
     * Updates the element at the specified element
     *
     * @param  entry, entry to be updated with the corresponding uuid value
     * @return True in case of success, False otherwise
     * @throws UnsupportedOperationException if the {@code set} operation
     *         is not supported by this list
     * @throws ClassCastException if the class of the specified element
     *         prevents it from being added to this list
     * @throws NullPointerException if the specified element is null and
     *         this list does not permit null elements
     * @throws IllegalArgumentException if some property of the specified
     *         element prevents it from being added to this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
     */	
	public boolean update(final Entry entry) {
		if (false == this.isEntryExists(entry))
			return false;

		final int index = this.recordsList.indexOf(entry);
		if (-1 == index) {
			//TODO: Report this shit. No way this could happen!!
			return false;
		}
		this.recordsList.set(index, entry);
		this.recordsList.sort(this.comparator);
		return true;
	}
	
	/** Clears the table cached data. **/	
	public void clear() {
		this.recordsList.clear();
	}
	
	/** Return TRUE if cache is empty and FALSE otherwise **/
	public boolean isEmpty() {
		return this.recordsList.isEmpty();
	}
	
    /**
     * Returns the number of elements in this list.  If this list contains
     * more than {@code Integer.MAX_VALUE} elements, returns
     * {@code Integer.MAX_VALUE}.
     * @return the number of elements in this list
     */	
	public int Size() {
		return this.recordsList.size();
	}
	
    /**
     * Returns the cache elements depending of Entry.equals() result.
     *
     * @param  entry - the Entry object
     * @return Entry object if its found.
     *         If no matches founds returns NULL. 
     */	
	public Entry get(final Entry entry) {
		for (final Entry element : recordsList)
			if (true == element.equals(entry))
				return element;
		return null;
	}
	
    /**
     * Returns the cache elements identified by uuid value.
     *
     * @param  uuid - the Entry uuid value
     * @return Entry object if its found.
     *         If no matches founds returns NULL. 
     */		
	public Entry get(final String uuid) {
		for (Entry entry : recordsList)
			if (0 == entry.getUuid().compareTo(uuid))
				return entry;
		return null;
	}
	
    /**
     * Returns the element at the specified position in this list.
     *
     * @param  index index of the element to return
     * @return the element at the specified position in this list
     * 		   Null in case if index value is incorrect
     */	
	public Entry get(int index) {
		try {
			return this.recordsList.get(index);
		} catch (final IndexOutOfBoundsException exc) {
			return null;
		}
	}
	
    /**
     * Returns the cached records list
     *
     * @return the list of elements.
     */	
	public final List<Entry> getList() {
		return this.recordsList;
	}
	
    /**
     * Returns the First cached element.
     *
     * @return the first element if cache collection.
     * 		   Null in case if cache is cold (empty).
     */		
	public Entry getFirstElement() {
		if (true == this.recordsList.isEmpty())
			return null;
		return this.recordsList.get(0);
	}
	
    /**
     * Returns the Last cached element.
     *
     * @return the Last element if cache collection.
     * 		   Null in case if cache is cold (empty).
     */		
	public Entry getLastElement() {
		if (true == this.recordsList.isEmpty())
			return null;
		return this.recordsList.get(this.recordsList.size() - 1);
	}
	
    /**
     * Returns the Last N cached elements.
     *
     * @return the list of N last cached elements.
     * 		   Null in case if cache is cold (empty) or N value exceed the cache size.
     */		
	public List<Entry> getNLastElements(int n) {
		if (true == this.recordsList.isEmpty() || n > this.recordsList.size())
			return null;
		return this.recordsList.subList(recordsList.size() - n, recordsList.size());
	}
	
	   /**
     * Returns the First N cached elements.
     *
     * @return the list of N first cached elements.
     * 		   Null in case if cache is cold (empty) or N value exceed the cache size.
     */		
	public List<Entry> getNFirstElements(int n) {
		if (true == this.recordsList.isEmpty() || n > this.recordsList.size())
			return null;
		return this.recordsList.subList(0, n);
	}	
	
    /**
     * Returns the records list with given UUID value.
     *
     * @return the list of records
     */		
	public List<Entry> getRecordsByUUiD(final String uuid) {
		return this.recordsList.stream().filter(entry -> entry.getUuid().equals(uuid)).collect(Collectors.toList());
		//return this.recordsList.stream().filter(entry -> 0 == entry.getUuid().compareTo(uuid)).collect(Collectors.toList());
	}
}
