//============================================================================
// Name        : InMemoryDatabaseTable.java
// Created on  : September 02, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Database table class
//============================================================================

package database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import database.entry.TableEntry;

/** @author a.tokmakov **/
/** @class  DatabaseTable. **/
public abstract class InMemoryDatabaseTable<Entry extends TableEntry> 
					  extends DatabaseTable<Entry> {
	
	/** InMemoryDatabaseTable class constructor : **/
	protected InMemoryDatabaseTable(final String tableName,
					                final String sqlStatement) {
		super(tableName, sqlStatement);
	}
	
	/** InMemoryDatabaseTable class constructor : **/
	protected InMemoryDatabaseTable(final String tableName,
					                final String sqlStatement,
					                int timeout) {
		super(tableName, sqlStatement, timeout);
	}
	
	@Override
	protected long getNextInsertedRecordID() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = connection.prepareStatement("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA=? AND TABLE_NAME=?;");
			statement.setString(1, dbConnectionPool.getSchemaName());
			statement.setString(2, this.getTableName());
			databaseLogger.debug(statement);
			resultSet = statement.executeQuery();
			if (true == resultSet.next()) {
				return resultSet.getLong(1);
			}
		} catch (SQLException exc) {
			databaseLogger.error(exc.getMessage());
			databaseLogger.error(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
		return -1;
	}
	
	protected boolean readTableFromDB() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = connection.prepareStatement(this.getSelectStatement());
			resultSet = statement.executeQuery();
			
			databaseLogger.debug("Reading table  " + this.getTableName() + "....");
			databaseLogger.debug(statement);
			
			while (resultSet.next()) 
				this.readRecord(resultSet);
		} catch (SQLException exc) {
			databaseLogger.error(exc.getMessage());
			databaseLogger.error(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
		return true;
	}
	
	public boolean add(Entry record) {
		if (true == isEntryKeyExists(record)) {
			applicationLogger.error("Record with uuid = " + record.getUuid() + "exists");
			return false;
		}
		if (true == this.insertRecord(record)) {
			// Add record to cache
			this.cache.add(record);
			
			// TODO. Fix this
			// this.browserBuilds.addBuild(record);
			
			return true;
		}
		return false;
	}
	
	public boolean update(Entry record) {
		Entry entry = this.cache.get(record.getUuid());
		if (null == entry){
			databaseLogger.error("Record with uuid = '" + record.getUuid() + "' do not exists");
			return false;
		}
		if (true == this.updateRecord(record, entry)) {
			this.cache.update(record);
			record.setId(entry.getId());
			return true;
		}
		return false;
	}	
	
	protected boolean insertRecord(Entry record) {
		final long id = this.getNextInsertedRecordID();
		if (-1 == id) { 
			// TODO. Trace error
			return false;
		}

		Connection connection = null;
		PreparedStatement statement = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = this.addRecord(record, connection);

			databaseLogger.debug("Insert data to table " + this.getTableName());
			databaseLogger.debug(statement);
			
			int result = statement.executeUpdate();
			if (result > 0) {
				/** Set ID: **/
				record.setId(id);
				/** Return True. **/
				return true;
			}
		} catch (SQLException exc) {
			databaseLogger.error(exc.getMessage());
			databaseLogger.error(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		} return false;
	}
	
	protected boolean updateRecord(Entry record, final Entry entry) {
		Connection connection = null;
		PreparedStatement statement = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = this.updateRecord(record, entry,  connection);
			
			databaseLogger.debug("Updating data to table " + this.getTableName() );
			databaseLogger.debug(statement);
			
			int result = statement.executeUpdate();
			if (result > 0) {
				// Return true
				return true;
			}
		} catch (SQLException exc) {
			databaseLogger.error(exc.getMessage());
			databaseLogger.error(Arrays.toString(exc.getStackTrace()));

		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		} return false;
	}
	
	/** Read table data. **/
	@Override
	public boolean readTable() {
		return this.readTableFromDB();
	}	

	/** Abstract method, reads table data from database. **/
	protected abstract void readRecord(ResultSet resultSet) throws SQLException;
	
	/** Abstract method, returns the SQL PreparedStatement for table record insertion. **/
	protected abstract PreparedStatement addRecord(Entry build, 
												   Connection connection) throws SQLException;
	
	/** Abstract method, returns the SQL PreparedStatement for table record update. **/
	protected abstract PreparedStatement updateRecord(Entry build, 
													  final Entry entry,
												      Connection connection) throws SQLException;
	
	/** Checks if key of the entry already exists in table. **/
	protected abstract boolean isEntryKeyExists(final Entry build); 	
}
