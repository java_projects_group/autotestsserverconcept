//============================================================================
// Name        : DatabaseTable.java
// Created on  : September 02, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Database table class
//============================================================================

package database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import database.BrowserBuilds;
import database.entry.TableEntry;

/** @author a.tokmakov **/
/** @class  DatabaseTable. **/
public abstract class DatabaseTable<Entry extends TableEntry> implements ITable {
	/** Database connection pool manager: **/
	protected database.connection.DatabaseConnectionPool dbConnectionPool = database.connection.DatabaseConnectionPool.getInstance();
	/** Table content. **/
	protected TableCache<Entry> cache = new TableCache<Entry>();
	/** Specific table name. **/
	protected String tableName = "";
	/** SQL select statement. **/
	protected String sqlSelectStatement = "";
	/** Table table invalidation timeout : **/
	protected int timeout = DatabaseTable.TABLE_CACHE_TTL; 
	/** Timestamp of last Database read. **/
	protected long lastDBRead = 0;
	/** BrowserBuilds table instance : **/
	protected BrowserBuilds browserBuilds = BrowserBuilds.getTable();
	
	/** Database logger. **/
	protected final Logger databaseLogger = LogManager.getLogger(DatabaseTable.class);
	/** Application level logger. **/
	protected final Logger applicationLogger = LogManager.getLogger("ApplicationLogger");
	
	/** Default cache time to live. **/
	protected static final int TABLE_CACHE_TTL = 300 * 1000;
	
	/** InMemoryDatabaseTable class constructor : **/
	protected DatabaseTable(final String tableName,
					        final String sqlStatement) {
		this(tableName,
			 sqlStatement, 
			 DatabaseTable.TABLE_CACHE_TTL);
	}	
	
	/** DatabaseTable class constructor : **/
	protected DatabaseTable(final String tableName,
					        final String sqlStatement,
					        int timeout) {
		this.tableName = tableName;
		this.sqlSelectStatement = sqlStatement;
		this.timeout = timeout;
	}
	
	@Override
	public String getTableName() {
		return this.tableName;
	}

	@Override
	public String getSelectStatement() {
		return this.sqlSelectStatement;
	}
	
	/** Delete table data. **/
	public void clearTable() {
		cache.clear();
	}
	
    /**
     * Converts java.sql.Timestamp object to a LocalDateTime.
     * 
     * The conversion creates a {@code LocalDateTime} that represents the
     * same year, month, day of month, hours, minutes, seconds and nanos
     * date-time value as this {@code Timestamp} in the local time zone.
     * 
     * @param  timestamp - java.sql.Timestamp objected
     * @return a LocalDateTime object representing the same date-time value
     *           NULL in case if timestamp value is null.
     */	
	public Timestamp sqlTimeFromJavaTime(final LocalDateTime time) {
		if (null == time)
			return null;
		return Timestamp.valueOf(time);
	}
	
    /**
     * Converts java.sql.Timestamp object to a LocalDateTime.
     * 
     * The conversion creates a {@code LocalDateTime} that represents the
     * same year, month, day of month, hours, minutes, seconds and nanos
     * date-time value as this {@code Timestamp} in the local time zone.
     * 
     * @param  timestamp - java.sql.Timestamp objected
     * @return a LocalDateTime object representing the same date-time value
     *           NULL in case if timestamp value is null.
     */
	public LocalDateTime javaTimeFromSQLTime(final Timestamp timestamp) {
		if (null == timestamp)
			return null;
		return timestamp.toLocalDateTime();
	}
	
    /**
     * Returns the ID of the next successfully inserted record
     *
     * @param     None
     * @return    The ID of the next record to be inserted.
     * @exception Method should catch and handle exceptions.
     */
	protected abstract long getNextInsertedRecordID();
	
    /**
     * Method parses the java.sql.ResultSet object to corresponding
     * Entry type object instance of the derived class.
     *
     * @param  resultSet - java.sql.ResultSet objected just
     * 					   read from database
     * @exception SQLException if the columnIndex is not valid;
     * 			  If a database access error occurs or this method is 
     * 			  called on a closed result set
     */		
	protected abstract void addToTableCache(final ResultSet resultSet) throws SQLException;
	
    /**
     * @method buildInsertStatement
     * Abstract method which should be implemented in the derived class
     * Method builds the SQL insert statement of java.sql PreparedStatement type.
     * 
     * Overridden method instance should implement generation login from specific 
     * table entry to valid SQL insert statement to be used in insertRecord method.
     *
     * @param entry - the derived class database entry type instance
     * 				  Should extends the Entry type.
     * @param connection - java.sql.Connection objected
     * 					   The existing database connection handle.
     * @exception SQLException if the columnIndex is not valid;
     * 			  If a database access error occurs or this method is 
     * 			  called on a closed result set
     */	
	protected abstract PreparedStatement 
		buildInsertStatement(final Entry entry, final Connection connection) 
							 throws SQLException;
	
    /**
     * @method buildUpdatetatement
     * Abstract method which should be implemented in the derived class
     * Method builds the SQL update statement of java.sql PreparedStatement type.
     * 
     * Overridden method instance should implement generation login from specific 
     * table entry to valid SQL update statement to be used in updateRecord method.
     *
     * @param entry - the derived class database entry type instance
     * 				  Should extends the Entry type.
     * @param connection - java.sql.Connection objected
     * 					   The existing database connection handle.
     * @exception SQLException if the columnIndex is not valid;
     * 			  If a database access error occurs or this method is 
     * 			  called on a closed result set
     */	
	protected abstract PreparedStatement 
		buildUpdateStatement(final Entry entry, final Connection connection) 
							 throws SQLException;
	
    /**
     * @method get
     * Returns the cache elements depending of Entry.equals() result.
     *
     * @param  entry - the Entry object
     * @return Entry object if its found.
     *         If no matches founds returns NULL. 
     */	
	public Entry get(final Entry entry) {
		return this.cache.get(entry);
	}
	
    /**
     * @method get
     * Returns the cache elements identified by uuid value.
     *
     * @param  uuid - the Entry uuid value
     * @return Entry object if its found.
     *         If no matches founds returns NULL. 
     */		
	public Entry get(final String uuid) {
		return this.cache.get(uuid);
	}
	
    /**
     * @method get
     * Returns the element at the specified position in this list.
     *
     * @param  index index of the element to return
     * @return the element at the specified position in this list
     * 		   Null in case if index value is incorrect
     */	
	public Entry get(int index) {
		return this.cache.get(index);
	}
	
    /**
     * @method isEmpty
     * Checks if the table is emptry or not.
     *
     * @return TRUE if cache is empty and FALSE otherwis
     */	
	public boolean isEmpty() {
		return this.cache.isEmpty();
	}
	
    /**
     * @method Size
     * Returns the number of elements in this list.  If this list contains
     * more than {@code Integer.MAX_VALUE} elements, returns
     * 
     * {@code Integer.MAX_VALUE}.
     * @return the number of elements in this list
     */	
	public int Size() {
		return this.cache.Size();
	}
	
    /**
     * @method getList
     * Returns the cached records list
     *
     * @return the list of elements.
     */	
	public final List<Entry> getList() {
		return this.cache.getList();
	}
	
    /**
     * @method getFirstElement
     * Returns the First table record.
     *
     * @return the First record in table.
     * 		   Null in case if the table is empty.
     */	
	public Entry getFirstElement() {
		return this.cache.getFirstElement();
	}
	
    /**
     * @method getLastElement
     * Returns the Last table record.
     *
     * @return the Last record in table.
     * 		   Null in case if the table is empty.
     */		
	public Entry getLastElement() {
		return this.cache.getLastElement();
	}
	
    /**
     * @method getNLastElements
     * Returns the Last N table records.
     *
     * @return the list of N last records in the table.
     * 		   Null in case if the table is empty or N value exceed the table size.
     */		
	public List<Entry> getNLastElements(int n) {
		return this.cache.getNLastElements(n);
	}
	
    /**
     * @method getNFirstElements
     * Returns the First N table records.
     *
     * @return the list of N first records in the table.
     * 		   Null in case if the table is empty or N value exceed the table size.
     */		
	public List<Entry> getNFirstElements(int n) {
		return this.cache.getNFirstElements(n);
	}	
	
    /**
     * @method getRecordsByUUiD
     * Returns the records list with given UUID value.
     *
     * @return the list of records
     */		
	public List<Entry> getRecordsByUUiD(final String uuid) {
		return this.cache.getRecordsByUUiD(uuid);
	}
	
    /**
     * @method readTableFromDB
     * Reads the whole table, parse every ResultSet to the 
     * corresponding table entry object and then adds them to the table cache.
     *
     * @param  void
     * @exception none
     * @return TRUE if cache is empty and 
     * 		   FALSE otherwise
     */		
	protected boolean readTableFromDB() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = connection.prepareStatement(this.getSelectStatement());
			resultSet = statement.executeQuery();
			
			databaseLogger.debug("Reading table  " + this.getTableName() + "....");
			databaseLogger.debug(statement);
			
			while (resultSet.next()) {
				this.addToTableCache(resultSet);
			}
		} catch (final SQLException sqlException) {
			databaseLogger.error(sqlException.getMessage());
			databaseLogger.error(Arrays.toString(sqlException.getStackTrace()));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (final SQLException exception) {
				exception.printStackTrace();
			}
		}
		return true;
	}
	
    /**
     * @method insertRecord.
     * Performs the database insertion to the database.
     * 
     * @param entry - The specific database table entry object instance
     * 				  of the derived class.
     * @exception none
     * @return TRUE if cache is empty and 
     * 		   FALSE otherwise
     */	
	protected boolean insertRecord(final Entry entry) {
		Connection connection = null;
		PreparedStatement statement = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = this.buildInsertStatement(entry, connection);

			databaseLogger.debug("Insert data to table " + this.getTableName());
			databaseLogger.debug(statement);
			
			return statement.executeUpdate() > 0 ? true : false;
		} catch (SQLException exc) {
			databaseLogger.error(exc.getMessage());
			databaseLogger.error(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		} 
		// If we have not entered before, make if False...
		return false;
	}
	
    /**
     * @method updateRecord.
     * Performs the database update to the database.
     * 
     * @param entry - The specific database table entry object instance
     * 				  of the derived class.
     * @exception none
     * @return TRUE if cache is empty and 
     * 		   FALSE otherwise
     */	
	protected boolean updateRecord(final Entry entry) {
		Connection connection = null;
		PreparedStatement statement = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = this.buildUpdateStatement(entry,  connection);
			
			databaseLogger.debug("Updating data to table " + this.getTableName());
			databaseLogger.debug(statement);
			
			int result = statement.executeUpdate();
			if (result > 0) {
				// Return true
				return true;
			}
		} catch (SQLException exc) {
			databaseLogger.error(exc.getMessage());
			databaseLogger.error(Arrays.toString(exc.getStackTrace()));

		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		} 
		return false;
	}
}
