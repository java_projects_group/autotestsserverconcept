//============================================================================
// Name        : BuildsTable.java
// Created on  : September 02, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Builds table
//============================================================================

package database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import database.BrowserBuilds;
import database.entry.BuildRecord;
import database.entry.BuildStatus;

/** @author a.tokmakov **/
/** @class  BuildsTable. **/
public class BuildsTable extends DatabaseTable<BuildRecord> {
	/** Static BuildsTable class instance : **/
	private static volatile BuildsTable __instance = null;
	
	/** Builds table the singleton table instance : **/  
	public static BuildsTable getTable() {
		if (null == __instance) {
			synchronized (BuildsTable.class) {
				if (null == __instance) {
					__instance = new BuildsTable();
				};
			}
		} return __instance;
	}	

	/** Sealed (Private) BuildsTable class default constructor : **/
	protected BuildsTable() {
		super("builds",  "select * from builds;");
		this.readTableFromDB();
	}
	
	
	/** Read table data. **/
	public boolean readTable() {
		return this.readTableFromDB();
	}
	
	/** Checks if key of the entry already exists in table. **/
	protected boolean isEntryKeyExists(final BuildRecord record) {
		return this.cache.isEntryExists(record);
	}
	
	public boolean update(BuildRecord record) {
		final BuildRecord build = this.cache.get(record.getUuid());
		if (null == build){
			databaseLogger.error("Record with uuid = '" + record.getUuid() + "' do not exists");
			return false;
		}
		if (true == this.updateRecord(record)) {
			this.cache.update(record);
			record.setId(build.getId());
			this.applicationLogger.debug("BuildsTable: Record updated to .\n" + record);
			this.browserBuilds.updateBuild(record);
			return true;
		}
		return false;
	}	
	
	public boolean add(final BuildRecord record) {
		if (true == isEntryKeyExists(record)) {
			applicationLogger.error("Record with uuid = " + record.getUuid() + "exists");
			return false;
		}
		if (true == this.insertRecord(record)) {
			// Add record to cache
			this.cache.add(record);
			// TODO. Fix this
			this.applicationLogger.debug("BuildsTable: Record added.\n" + record);
			this.browserBuilds.addBuild(record);
			return true;
		}
		return false;
	}
	
	@Override
	protected void addToTableCache(final ResultSet resultSet) throws SQLException {
		// TODO : Handle SQL error and type cast errors
		BuildRecord record = new BuildRecord();
		
		// Initialize BuildRecord values.
		record.setId(resultSet.getLong(1));
		record.setUuid(resultSet.getString(2));
		record.setStartTime(javaTimeFromSQLTime(resultSet.getTimestamp(3)));
		record.setEndTime(javaTimeFromSQLTime(resultSet.getTimestamp(4)));
		record.setNightly(resultSet.getBoolean(5));
		record.setStatus(BuildStatus.fromString(resultSet.getString(6)));
		record.setBuildnumber(resultSet.getLong(7));
		record.setBrowserVersion(resultSet.getString(8));
		record.setBranch(resultSet.getString(9));
		record.setRepository(resultSet.getString(11));
		record.setRevision(resultSet.getString(12));
		final String ownersStr = resultSet.getString(10);
		if (null != ownersStr && false == ownersStr.isEmpty()) {
			String[] owners = resultSet.getString(10).replace("[", " ").replace("]", " ").replace("\"", "").strip().split(",");
			record.setOwners(Arrays.asList(owners));
		}
		// Add to cache:
		this.cache.add(record);
	}
	
	@Override
	protected PreparedStatement buildInsertStatement(final BuildRecord record, 
										  			 final Connection connection) throws SQLException {
		final String sql = "INSERT INTO builds(uuid,start_time,end_time,is_nightly,status,buildnumber,browser_version,branch,owners,repository,revision) " +
						   "VALUES(?,?,?,?,?,?,?,?,?,?,?);";
		PreparedStatement insertStatement  = connection.prepareStatement(sql);
		insertStatement.setString(1, record.getUuid());
		insertStatement.setTimestamp(2, sqlTimeFromJavaTime(record.getStartTime()));
		insertStatement.setTimestamp(3, sqlTimeFromJavaTime(record.getEndTime()));
		insertStatement.setBoolean(4, record.isNightly());
		insertStatement.setString(5, record.getStatus().toString());
		insertStatement.setLong(6, record.getBuildnumber());
		insertStatement.setString(7, record.getBrowserVersion());
		insertStatement.setString(8, record.getBranch());
		insertStatement.setString(9, record.getOwnersJson());
		insertStatement.setString(10, record.getRepository());
		insertStatement.setString(11, record.getRevision());		
		return insertStatement;
	}
	
	@Override
	protected PreparedStatement buildUpdateStatement(final BuildRecord record, 
											 		 final Connection connection) throws SQLException {
		final String sql = "UPDATE builds set end_time=?, is_nightly=?,status=?,buildnumber=?,browser_version=?,branch=? "
                               + ",owners=?,repository=?,revision=? where uuid=?;";
		PreparedStatement insertStatement  = connection.prepareStatement(sql);
		insertStatement.setTimestamp(1, sqlTimeFromJavaTime(record.getEndTime()));
		insertStatement.setBoolean(2, record.isNightly());
		insertStatement.setString(3, record.getStatus().toString());
		insertStatement.setLong(4, record.getBuildnumber());
		insertStatement.setString(5, record.getBrowserVersion());
		insertStatement.setString(6, record.getBranch());
		insertStatement.setString(7, record.getOwnersJson());
		insertStatement.setString(8, record.getRepository());
		insertStatement.setString(9, record.getRevision());	
		insertStatement.setString(10, record.getUuid());
		return insertStatement;
	}
	
	///////////////////////////////////////////////////////////////
	
	@Override
	protected boolean insertRecord(BuildRecord record) {
		final long id = this.getNextInsertedRecordID();
		if (-1 == id) { 
			// TODO. Trace error
			return false;
		}
		if (true == super.insertRecord(record)) {
			record.setId(id);
			/** Return True. **/
			return true;
		}
		return false;
	}
	
	@Override
	protected long getNextInsertedRecordID() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = connection.prepareStatement("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA=? AND TABLE_NAME=?;");
			statement.setString(1, dbConnectionPool.getSchemaName());
			statement.setString(2, this.getTableName());
			databaseLogger.debug(statement);
			resultSet = statement.executeQuery();
			if (true == resultSet.next()) {
				return resultSet.getLong(1);
			}
		} catch (SQLException exc) {
			databaseLogger.error(exc.getMessage());
			databaseLogger.error(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
		return -1;
	}
}
