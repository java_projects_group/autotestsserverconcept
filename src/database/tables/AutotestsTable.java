//============================================================================
// Name        : AutotestsTable.java
// Created on  : October 16, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Unit tests table
//============================================================================

package database.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import database.entry.AutotestsRecord;
import database.entry.TestStatus;
import database.entry.TestType;

/** @author a.tokmakov.      **/
/** @class  AutotestsTable. **/
public class AutotestsTable extends DatabaseTable<AutotestsRecord> {
	/** Static AutotestsTable class instance : **/
	private static volatile AutotestsTable __instance = null;
	
	/** UnitTests table the singleton table instance : **/  
	public static AutotestsTable getTable() {
		if (null == __instance) {
			synchronized (AutotestsTable.class) {
				if (null == __instance) {
					__instance = new AutotestsTable();
				};
			}
		} 
		return __instance;
	}
	
	/** Sealed (protected) AutotestsTable class default constructor : **/
	protected AutotestsTable() {
		super("auto_tests",  "select * from auto_tests;");
		this.readTableFromDB();
	}
	
	/** Checks if key of the entry already exists in table. **/
	protected boolean isEntryKeyExists(final AutotestsRecord record) {
		return this.cache.isEntryExists_ByID(record);
	}
	
	@Override
	protected void addToTableCache(final ResultSet resultSet) throws SQLException {
		// TODO : Handle SQL error and type cast errors
		AutotestsRecord record = new AutotestsRecord();
		
		// Initialize AutotestsRecord values.
		record.setId(resultSet.getLong(1));
		record.setUuid(resultSet.getString(2));
		record.setTestsType(TestType.fromString(resultSet.getString(3)));
		record.setTestStatus(TestStatus.fromString(resultSet.getString(4)));
		record.setWorkerName(resultSet.getString(5));
		record.setOs(resultSet.getString(6));
		record.setStartTime(javaTimeFromSQLTime(resultSet.getTimestamp(7)));
		record.setEndTime(javaTimeFromSQLTime(resultSet.getTimestamp(8)));
		record.setResultJson(resultSet.getString(9));
		record.setTestsPassed(resultSet.getLong(10));
		record.setTestsFailed(resultSet.getLong(11));
		record.setTestsSkipped(resultSet.getLong(12));
		
		// Add to cache:
		this.cache.add(record);
	}
	
	@Override
	protected PreparedStatement buildInsertStatement(final AutotestsRecord record, 
										  			 final Connection connection) throws SQLException {
		final String sql = "INSERT INTO auto_tests(build_id,uuid,test_type,status,worker_name,os,start_time,end_time,results,passed,failed,skipped) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?);";
		
		// Initialize SQL insert statement
		PreparedStatement statement  = connection.prepareStatement(sql);
		statement.setLong(1, record.getId());
		statement.setString(2, record.getUuid());
		statement.setString(3, record.getTestsType().toString());
		statement.setString(4, record.getTestStatus().toString());
		statement.setString(5, record.getWorkerName());
		statement.setString(6, record.getOs());
		statement.setTimestamp(7, sqlTimeFromJavaTime(record.getStartTime()));
		statement.setTimestamp(8, sqlTimeFromJavaTime(record.getEndTime()));
		statement.setString(9, record.getResultJson());
		statement.setLong(10, record.getTestsPassed());
		statement.setLong(11, record.getTestsFailed());
		statement.setLong(12, record.getTestsSkipped());
		return statement;
	}
	
	@Override
	protected PreparedStatement buildUpdateStatement(final AutotestsRecord recAutoTest, 
											 		 final Connection connection) throws SQLException {
		final String sql = "UPDATE auto_tests SET status=?,end_time=?,results=?,passed=?,failed=?,skipped=? "
				+ "WHERE uuid=? AND test_type=? AND worker_name=? AND os=? AND start_time=?;";
		// Initialize SQL update statement
		PreparedStatement statement  = connection.prepareStatement(sql);
		statement.setString(1, recAutoTest.getTestStatus().toString());
		statement.setTimestamp(2, sqlTimeFromJavaTime(recAutoTest.getEndTime()));
		statement.setString(3, recAutoTest.getResultJson());
		statement.setLong(4, recAutoTest.getTestsPassed());
		statement.setLong(5, recAutoTest.getTestsFailed());
		statement.setLong(6, recAutoTest.getTestsSkipped());
		// After WHERE:
		statement.setString(7, recAutoTest.getUuid());
		statement.setString(8, recAutoTest.getTestsType().toString());
		statement.setString(9, recAutoTest.getWorkerName());
		statement.setString(10, recAutoTest.getOs());
		statement.setTimestamp(11, sqlTimeFromJavaTime(recAutoTest.getStartTime()));
		return statement;
	}
	
	public boolean update(final AutotestsRecord record) {
		if (null == this.cache.get(record)) {
			databaseLogger.error("Failed to find corrresponding record in DB Cache\n" + record);
			return false;
		} else if (true == this.updateRecord(record)) {
			this.applicationLogger.debug("AutotestsTable: Record updated to .\n" + record);
			this.browserBuilds.updateAutoTest(record);
			return this.cache.update(record);
		}
		return false;
	}	
	
	public boolean add(final AutotestsRecord record) {
		if (null != this.cache.get(record)) {
			databaseLogger.error("Record already exist in DB Cache\n" + record);
			return false;
		} else if (true == this.insertRecord(record)) {
			this.cache.add(record);
			this.browserBuilds.addAutoTest(record);
			this.applicationLogger.debug("AutotestsTable: Record added  .\n" + record);
			return true;
		}
		return false;
	}

	/** Read table data. **/
	@Override
	public boolean readTable() {
		return this.readTableFromDB();
	}	

	@Override
	protected long getNextInsertedRecordID() {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void main(String[] args) {
		// TODO: Move somewhere else
		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");
		
		AutotestsTable table  = AutotestsTable.getTable();
		
		AutotestsRecord testRecord  = new AutotestsRecord();
		testRecord.setId(261);
		testRecord.setUuid("1203a85b-24f9-4966-b491-a5d5649a4be1");
		testRecord.setTestsType(TestType.UpdaterTests);
		//testRecord.setTestStatus(TestStatus.TestsStarted);
		testRecord.setWorkerName("UpdateInstallTesterWin8x64");
		testRecord.setOs("Windows-8.1-6.3.9600-SP0");
		testRecord.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		
		testRecord.setTestStatus(TestStatus.TestsCompleted);
		testRecord.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		testRecord.setResultJson("{}");
		
		//table.add(testRecord);
		table.update(testRecord);
	}			
}
