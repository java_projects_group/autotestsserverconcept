//============================================================================
// Name        : ITable.java
// Created on  : June 24, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : ITable interface.
//============================================================================

package database.tables;

/** @author a.tokmakov **/
/** @interface ITable. **/
public interface ITable {
	/** Returns the table name : **/
	public String getTableName();
	
	/** SQL select statement : **/
	public String getSelectStatement();
	
	/** Read table data. **/
	public boolean readTable();
	
	/** Delete table data. **/
	//public void clearTable();
	
	/** Checks if table data is valid/still actual : **/
	//public boolean validateTable();
	
	/** Reads table data from database : **/
	//public boolean readTableFromDatabase();
}
