//============================================================================
// Name        : DatabaseConnectionPool.java
// Created on  : June 20, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : DatabaseConnectionPool class
//============================================================================

package database.connection;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import configuration.Configuration;

/** @class DatabaseConnectionPool : */
public class DatabaseConnectionPool {
	/** PostgresDatabaseConnectionPool static instance. **/
	private static DatabaseConnectionPool __instance = null;	
	/** Configuration keeper: **/
	private static final Configuration config = Configuration.getConfiguration();
	/** Postgres SQL Database engine class name. **/
	@SuppressWarnings("unused")
	private static final String driverClassNamePostgres = "org.postgresql.Driver";
	/** MySQL database engine class name. **/
	private static final String driverClassNameMySql = "com.mysql.cj.jdbc.Driver";	
	/** Initial number of connections that are created when the pool is started. **/
	private static final int CONN_POOL_INITIAL_SIZE = 5;
	/** Database connection pool max size. **/
	private static final int CONN_POOL_SIZE_MAX = 10;
	/** Database logger. **/
	protected final Logger logger = LogManager.getLogger(DatabaseConnectionPool.class);	
	/** Apache BasicDataSource class instance. **/
	private BasicDataSource basicDataSource = new BasicDataSource();
	
	/** Database server host IP address. **/
	private String dbHost = config.getDbHost();
	/** Database server connection port. **/	
	private int dbPort = config.getDbPort();
	/** Database scheme name. **/	
	private String dbScheme = config.getDbScheme();	
	/** Database authorization user name. **/
	private String dbUserLogin = config.getDbLogin();
	/** Database authorization password. **/
	private String dbUserPassword = config.getDbPassword();
	/** Use SSL to connect to Database. **/
	private boolean useSSL = false;
	
	/**  **/
	protected DatabaseConnectionPool() {
		// Set database server host.
		this.dbHost = config.getDbHost();
		// Set database server connection port.
		this.dbPort = config.getDbPort();
		// Set database scheme
		this.dbScheme = config.getDbScheme();
		// Set database authorization user name.
		this.dbUserLogin = config.getDbLogin();
		// Set database authorization password
		this.dbUserPassword = config.getDbPassword();
		// Set database driver name
		basicDataSource.setDriverClassName(driverClassNameMySql);
		// Set database url
		basicDataSource.setUrl(this.getConnectionStringMySql());
		// Set database user
		basicDataSource.setUsername(dbUserLogin);
		// Set database password
		basicDataSource.setPassword(dbUserPassword);
		// Set the initial number of connections that are created when the pool is started. 
		basicDataSource.setInitialSize(CONN_POOL_INITIAL_SIZE);
		// Set the maximum number of active connections that can be allocated 
		// from this pool at the same time, or negative for no limit.
		basicDataSource.setMaxTotal(CONN_POOL_SIZE_MAX);
		// Set the maximum number of connections that can remain idle in the pool,
		// without extra ones being released, or negative for no limit.
		basicDataSource.setMaxIdle(10);
		
		logger.debug("Using DB class : " + basicDataSource.getDriverClassName());
		logger.debug("Database URL : "   + basicDataSource.getUrl());
		logger.debug("Database credatials : " + basicDataSource.getUsername() + "@" + basicDataSource.getPassword());
		logger.debug("Set the initial number of connections to " + basicDataSource.getInitialSize());
		logger.debug("Set the maximum number of active connections to " + basicDataSource.getMaxTotal());
		logger.debug("Set the maximum number of connections that can remain idle to " + basicDataSource.getMaxTotal());
	}

	/**  **/  
	public static DatabaseConnectionPool getInstance() {
		if (null == __instance) {
			synchronized (DatabaseConnectionPool.class) {
				if (null == __instance) {
					__instance = new DatabaseConnectionPool();
				}
			}
		}
		return __instance;
	}
	
	public String getSchemaName() {
		return this.dbScheme;
	}

	public java.sql.Connection getConnection() throws java.sql.SQLException {
		return basicDataSource.getConnection();
	}
	
    protected String getConnectionStringMySql() {
    	String connStr = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbScheme;
    	connStr +="?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    	if (false == this.useSSL) {
    		connStr +="&useSSL=false";
    	}
    	return connStr;
    }
    
    protected String getConnectionStringPostgres() {
    	return "jdbc:postgresql://" + dbHost + ":" + dbPort + "/" + dbScheme;
    }
}
