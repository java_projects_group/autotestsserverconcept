package tests;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.json.simple.parser.ParseException;

import database.BrowserBuilds;
import database.connection.DatabaseConnectionPool;
import database.entry.AutotestsRecord;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.entry.TestStatus;
import database.entry.TestType;
import database.entry.UnitTestsRecord;
import database.tables.AutotestsTable;
import database.tables.BuildsTable;
import database.tables.UnitTestsTable;
import database.view.AutotestsView;
import database.view.BuildView;
import database.view.UnittestsView;

public class Tests {
	
	public static void Sleep(int milliseconds) {
		try {
			Thread.currentThread();
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public static void ReadDataBaseTests() 
	{
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			connection = DatabaseConnectionPool.getInstance().getConnection();
			String sql = "select run_id from buildbot_test_runs where worker_name = 'WorkerTesterWin7x64' and is_nightly=false" + 
						 " order by ts_start desc limit 1;";
			statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				System.out.println((long)resultSet.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void  TimeTests() {
		/*
		LocalDateTime locTime = LocalDateTime.now();
		Date start_time = new Date();
		Timestamp timestamp = Timestamp.valueOf(locTime);
		

		System.out.println(locTime);
		System.out.println(start_time);
		System.out.println(timestamp);
		*/
		
		/*
		LocalDateTime locTime1 = LocalDateTime.now();
		LocalDateTime locTime2 = LocalDateTime.now();
		
		System.out.println(locTime1);
		System.out.println(locTime2);
		
		
		System.out.println(locTime1.getYear());
		System.out.println(locTime1.getMonthValue());
		System.out.println(locTime1.getDayOfMonth());
		System.out.println(locTime1.getSecond());		
		System.out.println(locTime1.getHour());
		System.out.println(locTime1.getMinute());
		System.out.println(locTime1.getSecond());
		System.out.println(locTime1.getNano());
		*/
		
		
		final String timeStr = "2019-08-28 10:02:29";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		
		LocalDateTime dateTime = LocalDateTime.parse(timeStr, formatter);
		System.out.println(dateTime);
		
		LocalDateTime dateTime2 = LocalDateTime.parse("2019-08-28 10:03:29", formatter);
		System.out.println(dateTime2);
		
		
		System.out.println(dateTime.compareTo(dateTime2));
		
		Timestamp sqlTime = Timestamp.valueOf(LocalDateTime.now());
		System.out.println(sqlTime);
		
		LocalDateTime locTime = sqlTime.toLocalDateTime();
		System.out.println(locTime);
		
	}
	
	// ==========================================================================================================//
	//                                 AUTOTESTS TABLE  TESTS                                                    //
	// ==========================================================================================================//
	public static void AutoTestsTable() {
		AutotestsTable table  = AutotestsTable.getTable();
	
		
		
		List<AutotestsRecord> recList = table.getList();
		for (final AutotestsRecord rec : recList) {
			System.out.println(rec.getId() + "  " + rec.getUuid() + "  " + rec.getWorkerName());
		}
	
	}
	
	// ==========================================================================================================//
	//                                 BUILDS TABLE  TESTS                                                       //
	// ==========================================================================================================//
	public static void BuildsTable_TESTS() {
		BuildsTable tblBuilds  = BuildsTable.getTable();
		AutotestsTable tblAutoTests  = AutotestsTable.getTable();
		UnitTestsTable tblUnitTests = UnitTestsTable.getTable();
	
		
		
		/*List<BuildRecord> recList = table.getList();
		for (final BuildRecord build : recList) {
			System.out.println(build.getId() + "  " + build.getUuid() + "  " + build.isNightly() + "  " + build.getStartTime());
		}*/
		
		BuildRecord build = tblBuilds.getLastElement();
		List<AutotestsRecord> autoTestsList = tblAutoTests.getRecordsByUUiD(build.getUuid());
		List<UnitTestsRecord> unitTestsList = tblUnitTests.getRecordsByUUiD(build.getUuid());
		
		
		System.out.println(build);
		
		
		System.out.println("Size = " + autoTestsList.size());
		for (AutotestsRecord rec : autoTestsList)
			System.out.println(rec);
		
		
		System.out.println("Size = " + unitTestsList.size());
		for (UnitTestsRecord rec : unitTestsList)
			System.out.println(rec);
		
		
		///////////////////////
		
		long startTime = System.nanoTime();
		
		build = tblBuilds.getLastElement();
		autoTestsList = tblAutoTests.getRecordsByUUiD(build.getUuid());
		unitTestsList = tblUnitTests.getRecordsByUUiD(build.getUuid());

		long estimatedTime = System.nanoTime() - startTime;
		System.out.println("Time elapsed: " + estimatedTime + "nano seconds");
		
		
		///////////////////////
		
		 startTime = System.nanoTime();
		
		build = tblBuilds.getLastElement();
		autoTestsList = tblAutoTests.getRecordsByUUiD(build.getUuid());
		unitTestsList = tblUnitTests.getRecordsByUUiD(build.getUuid());

		estimatedTime = System.nanoTime() - startTime;
		System.out.println("Time elapsed: " + estimatedTime + "nano seconds");
		double seconds = (double)estimatedTime / 1_000_000_000.0;
		System.out.println("Time elapsed: " + seconds + " seconds");
	}
	
	
	public static void BuildsTable_TESTS_Sort() {
		BuildsTable tblBuilds  = BuildsTable.getTable();
		
		/*
		for (BuildRecord br : tblBuilds.getList()){
			System.out.println(br);
		}
		*/
		
		for (BuildRecord br : tblBuilds.getNFirstElements(20)) {
			System.out.println(br);
			BuildView buildView = new BuildView(br);
		}
			

	}

	public static void BrowserBuilds_Tests() {
		BrowserBuilds builds = BrowserBuilds.getTable();
		
		builds.Initialize();
		builds.TEST();
		
		Sleep(3000);
	}
	
	public static void BuildRecordTest() {
		BuildRecord rec = new BuildRecord();
		rec.setUuid("23242342341421342134234242342");
		rec.setStatus(BuildStatus.TestsCompleted);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		
		rec.addOwner("Owner1");
		rec.addOwner("Owner2");
		rec.addOwner("Owner3");
		rec.addOwner("Owner4");
		
		System.out.println(rec.getOwnersJson());
	}
	
	
	public static void AutoTestView_TEST() {
		//BuildsTable tblBuilds  = BuildsTable.getTable();
		AutotestsTable tblAutoTests  = AutotestsTable.getTable();
		//UnitTestsTable tblUnitTests = UnitTestsTable.getTable();
		
		final String uuid = "d541db8a-b85d-4e82-8712-6a6b04d414c4";
		List<AutotestsRecord> autoTestsList = tblAutoTests.getRecordsByUUiD(uuid);
		
		AutotestsRecord rec = autoTestsList.get(0);
		//System.out.println(rec);
		
		AutotestsView av = new AutotestsView(rec);
		av.parserResults();

		
	}
	
	public static void UnittestsView_TEST() {
		//BuildsTable tblBuilds  = BuildsTable.getTable();
		//AutotestsTable tblAutoTests  = AutotestsTable.getTable();
		UnitTestsTable tblUnitTests = UnitTestsTable.getTable();
		
		final String uuid = "811874b5-31c2-4c93-9b95-b9d2ff0a5949";
		List<UnitTestsRecord> unitTestsList = tblUnitTests.getRecordsByUUiD(uuid);
		
		UnitTestsRecord rec = unitTestsList.get(4);
		//System.out.println(rec);
		
		UnittestsView utv = new UnittestsView(rec);
		utv.parserResults();

	}
	
	public static void BuildView_TEST() {
		BuildsTable tblBuilds  = BuildsTable.getTable();
		
		final String uuid = "811874b5-31c2-4c93-9b95-b9d2ff0a5949";
		List<BuildRecord> build = tblBuilds.getRecordsByUUiD(uuid);
		
		BuildView buildView = new BuildView(build.get(0));
	}
	

	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		
		//Tests.Test_BB_TestRuns_Table();

		
		//Tests.BuildBotTestRunTable_TEST();
		
		//System.out.println("***************");
		
		//Tests.BuildBotTestRunTable_TEST_GetLast_N_Records();
		
		//BBTestRunResultsTable_Test();
		
		//TablesTest();
		
		//DBAdaptor_Test();
		
		//ReadDataBaseTests();
		
		//BuildRecord rec = new BuildRecord();
		//System.out.println(rec);
		
		// Tests.TimeTests();
		
		
		//Tests.BuildRecordTest();
		
		// AutoTestsTable();
		
		// BuildsTable_TESTS();
		// BuildsTable_TESTS_Sort();
		
		BrowserBuilds_Tests();
		
		// AutoTestView_TEST();
		
		// UnittestsView_TEST();
		
		// BuildView_TEST();
	}
}
