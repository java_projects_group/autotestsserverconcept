package tests;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import database.entry.AutotestsRecord;
import database.entry.BuildRecord;
import database.entry.TableEntry;
import database.entry.TestStatus;
import database.entry.TestType;
import database.tables.TableCache;


public class TableCacheTests {
	
	
	public static void TableCache_Test() {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		
		BuildRecord rec1 = new BuildRecord();
		rec1.setId(1);
		rec1.setUuid("212121212");
		rec1.setRevision("Revision111");
		
		BuildRecord rec2 = new BuildRecord();
		rec2.setId(1);
		rec2.setUuid("212121212");
		rec2.setRevision("Revision222");
		
		BuildRecord rec3 = new BuildRecord();
		rec3.setId(1);
		rec3.setUuid("212121212");
		rec3.setRevision("Revision2221111");
		
		BuildRecord rec4 = new BuildRecord();
		rec4.setId(3);
		rec4.setUuid("212121212");
		rec4.setRevision("Revision22211112323232323");

		
		cache.add(rec1);
		
		for (final BuildRecord entry: cache.getList())
			System.out.println(entry);
		
		
		System.out.println("-------------------\n\n");
		
		cache.update(rec2);
		
		for (final BuildRecord entry: cache.getList())
			System.out.println(entry);
		
		System.out.println("-------------------\n\n");
		
		cache.update(rec3);
		
		for (final BuildRecord entry: cache.getList())
			System.out.println(entry);
		
		System.out.println("-------------------\n\n");		
		
		cache.add(rec4);
		
		for (final BuildRecord entry: cache.getList())
			System.out.println(entry);		
	}
	
	public static void AutotestsTable_Tests() {
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");
		//AutotestTableCache cache = new AutotestTableCache();
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		
		AutotestsRecord rec  = new AutotestsRecord();
		rec.setId(26);
		rec.setUuid("1203a85b-24f9-4966-b491-a5d5649a4be1");
		rec.setTestsType(TestType.UpdaterTests);
		rec.setWorkerName("UpdateInstallTesterWin8x64");
		rec.setOs("Windows-8.1-6.3.9600-SP0");
		rec.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		rec.setTestStatus(TestStatus.TestsCompleted);
		rec.setEndTime(LocalDateTime.parse("10/10/2019 22:33:44", formatter));
		rec.setResultJson("{}");
		
		AutotestsRecord rec2  = new AutotestsRecord();
		rec2.setId(26);
		rec2.setUuid("1203a85b-24f9-4966-b491-a5d5649a4be1");
		rec2.setTestsType(TestType.UpdaterTests);
		rec2.setTestStatus(TestStatus.TestsStarted);
		rec2.setWorkerName("UpdateInstallTesterWin8x64");
		rec2.setOs("Windows-8.1-6.3.9600-SP0");
		rec2.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		
		AutotestsRecord rec3  = new AutotestsRecord();
		rec3.setId(261);
		rec3.setUuid("1203a85b-24f9-4966-b491-a5d5649a4be1");
		rec3.setTestsType(TestType.UpdaterTests);
		rec3.setTestStatus(TestStatus.TestsStarted);
		rec3.setWorkerName("UpdateInstallTesterWin8x64");
		rec3.setOs("Windows-8.1-6.3.9600-SP0");
		rec3.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		

		cache.add(rec2);
		cache.add(rec3);
		cache.update(rec);
		
		
		for (final AutotestsRecord entry: cache.getList())
			System.out.println(entry);

	}

	public static void main(String[] args) {
		//TableCache_Test();
		AutotestsTable_Tests();

	}

}
