package html_builder;

import java.util.List;

import database.old.TestRun;
import database.old.BuildBotTestRunTable;

public class HtmlBuilder {
	/** BuildBotTestRunTable. **/
	protected static BuildBotTestRunTable bbTestRunsTable = BuildBotTestRunTable.getTable();
	
	private static final String nightlyTestsName = "Nightly tests";
	private static final String commitTestsName  = "Commit tests";
	
	protected String getBBTestTypeName(final TestRun testRun) {
		if (true == testRun.isNightyTests()) {
			return nightlyTestsName;
		} else {
			return commitTestsName;
		}
	}
	
	protected String getBBTestTimeLabel(final TestRun testRun) {
		String testRunTimeStr = testRun.getTsStartAsString() + " - " + testRun.getTsEndAsString();
		return testRunTimeStr;
	}
	
	public String buildFooter() {
		String footer = "";
		footer += "<footer>\n";
		footer += "   <div class=\"footer-area\">\n";
		footer += "       <p>� Copyright 2018. All right reserved. Template by <a href=\"https://colorlib.com/wp/\">Colorlisb</a>.</p>\n";
		footer += "   </div>\n";
		footer += "</footer>\n";
		return footer;
	}
	
	
	public String getTimeLineChart() {
		String chart = "<h4 class=\"header-title\">Timeline (Test runs)</h4>";
		chart += "<div class=\"timeline-area\">";
	
		List<TestRun> testRuns = bbTestRunsTable.getLastTestRuns(10);
		for (TestRun tRun : testRuns) {
			chart += "<div class=\"timeline-task\">";
			chart += "<div class=\"icon bg1\"><i class=\"fa fa-envelope\"></i></div>";
			chart += "<div class=\"tm-title\"><h4>";
			chart += this.getBBTestTypeName(tRun);
			chart += "</h4><span class=\"time\"><i class=\"ti-time\"></i>";
			chart += this.getBBTestTimeLabel(tRun);
			chart += "</span></div>";
			chart += "<p><b>WorkerName</b>: " + tRun.getWorkerName() + "</p>";
			chart += "<p><b>Status</b>: " + " Some tests failed (Test string)" + "</p>";
			chart += "</div>";
		}	
		chart += "</div>";
		return chart;
	}
}
