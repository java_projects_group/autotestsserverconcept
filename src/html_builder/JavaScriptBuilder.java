package html_builder;

import java.util.List;
import java.util.StringJoiner;

import database.old.TestRunStats;
import database.old.DBAdaptor;

class JSBuilderBase {
	/** **/
	public String addJSTags(final String jsScript)
	{
		String js = "<script type=\"text/javascript\">";
		js += jsScript;
		js += "</script>";
		return js;
	}
}

/** JavaScriptBuilder class : **/
public class JavaScriptBuilder extends JSBuilderBase  {
	
	/** DatabaseAdaptor class instance : **/
	protected static DBAdaptor dba = new DBAdaptor();
	
	// Build java script Highcharts text:
	public String getHighchart1() {
		TestRunStats stats = dba.getLastCommitTestRunStats();
		
		String javaScript = "var pieColors = (function() {";
		javaScript += "var colors = [],";
		javaScript += "base = Highcharts.getOptions().colors[0],";
		javaScript += "i;";
		javaScript += "for (i = 0; i < 10; i += 1) {";
		javaScript += "colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());";
		javaScript += "}";
		javaScript += "return colors;";
		javaScript += "}());";

		javaScript += "Highcharts.chart('highpiechart44', {";
		javaScript += "chart: {";
		javaScript += "plotBackgroundColor: null,";
		javaScript += "plotBorderWidth: null,";
		javaScript += "plotShadow: false,";
		javaScript += "type: 'pie'";
		javaScript += "},";
		javaScript += "title: {";
		javaScript += "text: 'Last commit test run'";
		javaScript += "},";
		javaScript += "tooltip: {";
		javaScript += "pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'";
		javaScript += "},";
		javaScript += "plotOptions: {";
		javaScript += "pie: {";
		javaScript += "allowPointSelect: true,";
		javaScript += "cursor: 'pointer',";
		javaScript += "colors: pieColors,";
		javaScript += "dataLabels: {";
		javaScript += "style: { \"color\": \"contrast\", \"fontSize\": \"11px\", \"fontWeight\": \"bold\", \"textOutline\": \"\" },";
		javaScript += "enabled: true,";
		javaScript += "format: '<b>{point.name}</b><br>{point.percentage:.1f} %',";
		javaScript += "distance: -50,";
		javaScript += "filter: {";
		javaScript += "property: 'percentage',";
		javaScript += "operator: '>',";
		javaScript += "value: 4";
		javaScript += "}";
		javaScript += "}";
		javaScript += "}";
		javaScript += "},";
		javaScript += "series: [{";
		javaScript += "name: 'Share',";
		javaScript += "data: [";
		javaScript += "{ name: 'Passed', y: " + stats.getTestsPassed() + " },";
		javaScript += "{ name: 'Failed', y: " + stats.getTestFailed() + " },";
		javaScript += "{ name: 'Skipped', y: " + stats.getTestSkipped() + " }";
		javaScript += "]";
		javaScript += "}]";
		javaScript += "});";
		return addJSTags(javaScript);
	}
	
	// Build java script Highcharts text:
	public String getHighchart2() {
		TestRunStats stats = dba.getNightlyLastTestRunStats();
		String javaScript = "Highcharts.chart('highpiechart55', {";
		javaScript += "chart: {";
		javaScript += "plotBackgroundColor: null,";
		javaScript += "plotBorderWidth: null,";
		javaScript += "plotShadow: false,";
		javaScript += "type: 'pie'";
		javaScript += "},";
		javaScript += "title: {";
		javaScript += "text: 'Last nightly tests results'";
		javaScript += "},";
		javaScript += "tooltip: {";
		javaScript += "pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'";
		javaScript += "},";
		javaScript += "plotOptions: {";
		javaScript += "pie: {";
		javaScript += "allowPointSelect: true,";
		javaScript += "cursor: 'pointer',";
		javaScript += "dataLabels: {";
		javaScript += "enabled: false,";
		javaScript += "format: '<b>{point.name}</b>: {point.percentage:.1f} %',";
		javaScript += "style: {";
		javaScript += "color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || '#444',";
		javaScript += "\"textOutline\": \"\"";
		javaScript += "}";
		javaScript += "}";
		javaScript += "}";
		javaScript += "},";
		javaScript += "series: [{";
		javaScript += "name: 'Brands',";
		javaScript += "colorByPoint: true,";
		javaScript += "data: [{";
		javaScript += "name: 'Passed',";
		javaScript += "y: " + stats.getTestsPassed();
		javaScript += ", sliced: false,";
		javaScript += "selected: false";
		javaScript += "}, {";
		javaScript += "name: 'Failed',";
		javaScript += "y:  " + stats.getTestFailed();
		javaScript += "}, {";
		javaScript += "name: 'Skipped',";
		javaScript += "y: " + stats.getTestSkipped();
		javaScript += "}]";
		javaScript += "}]";
		javaScript += "});";
		return addJSTags(javaScript);
	}
	
	public String getLinechart1() {
		StringJoiner data = new StringJoiner(",");
		//final List<TestRunStats> allTestRunStats = dba.getAllTestStats();
		final List<TestRunStats> allTestRunStats = dba.getAllCommitTestStats();
		@SuppressWarnings("unused")
		int i = 0;
		for (final TestRunStats runStat : allTestRunStats) {
			i++;
			data.add("{\"year\": " + runStat.getRunId() + 
						",\"Passed\": " + runStat.getTestsPassed() + 
						",\"Failed\": " + runStat.getTestFailed() + 
						",\"Skipped\": " + runStat.getTestSkipped()  + "}");
		}
		
		String javaScript = "var chart = AmCharts.makeChart(\"amlinechart4\", {";
		javaScript += "\"type\": \"serial\",";
		javaScript += "\"theme\": \"light\",";
		javaScript += "\"legend\": {";
		javaScript += "\"useGraphSettings\": true";
		javaScript += "},";
		javaScript += "\"dataProvider\": ";
		javaScript += "[" + data.toString() + "]";
		javaScript += ",\"startDuration\": 0.5,";
		javaScript += "\"graphs\": [{";
		javaScript += "\"balloonText\": \"[[value]] tests passed\",";
		javaScript += "\"bullet\": \"round\",";
		javaScript += "\"hidden\": true,";
		javaScript += "\"title\": \"Passed\",";
		javaScript += "\"valueField\": \"Passed\",";
		javaScript += "\"fillAlphas\": 0,";
		javaScript += "\"lineColor\": \"#31ef98\",";
		javaScript += "\"lineThickness\": 2,";
		javaScript += "\"negativeLineColor\": \"#17e285\",";
		javaScript += "}, {";
		javaScript += "\"balloonText\": \"[[value]] tests failed\",";
		javaScript += "\"bullet\": \"round\",";
		javaScript += "\"title\": \"Failed\",";
		javaScript += "\"valueField\": \"Failed\",";
		javaScript += "\"fillAlphas\": 0,";
		javaScript += "\"lineColor\": \"#9656e7\",";
		javaScript += "\"lineThickness\": 2,";
		javaScript += "\"negativeLineColor\": \"#c69cfd\"";
		javaScript += "}, {";
		javaScript += "\"balloonText\": \"[[value]] tests skipped\",";
		javaScript += "\"bullet\": \"round\",";
		javaScript += "\"title\": \"Skipped\",";
		javaScript += "\"valueField\": \"Skipped\",";
		javaScript += "\"fillAlphas\": 0,";
		javaScript += "\"lineColor\": \"#31aeef\",";
		javaScript += "\"lineThickness\": 2,";
		javaScript += "\"negativeLineColor\": \"#31aeef\",";
		javaScript += "}],";
		javaScript += "\"chartCursor\": {";
		javaScript += "\"cursorAlpha\": 0,";
		javaScript += "\"zoomable\": false";
		javaScript += "},";
		javaScript += "\"categoryField\": \"year\",";
		javaScript += "\"categoryAxis\": {";
		javaScript += "\"gridPosition\": \"start\",";
		javaScript += "\"axisAlpha\": 0,";
		javaScript += "\"fillAlpha\": 0.05,";
		javaScript += "\"fillColor\": \"#000000\",";
		javaScript += "\"gridAlpha\": 0,";
		javaScript += "\"position\": \"top\"";
		javaScript += "},";
		javaScript += "\"export\":{";
		javaScript += "\"enabled\": false";
		javaScript += "}";
		javaScript += "});";
		return addJSTags(javaScript);
	}
}
