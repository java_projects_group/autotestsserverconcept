//============================================================================
// Name        : KafkaMessagesConsumer.java
// Created on  : July 23, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : JsonMessageParser class
//=====================KafkaConsumer======================================================

package consumer;

import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;

// Kafka imports:
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

// Logging
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import configuration.Configuration;

/** @class KafkaMessagesConsumer class : **/
public class KafkaMessagesConsumer implements IConsumer {
	/** Run server flag : **/
	private volatile boolean __runServer = false;
	/** List of kafka topics to listen : **/
	private List<String> topics = new ArrayList<String>();
	/** List of kafka cluster nodes: **/
	private List<String> nodes = new ArrayList<String>();	
	/** Consumer thread handle. **/
	private Thread consumerThread = null;
	/** Kafka messages handler thread. **/
	private Thread handlerThread = null;
	/** Concurrent kafka messages queue: **/
	private ConcurrentLinkedQueue<ConsumerRecord<String, String>> messagesQueue = new ConcurrentLinkedQueue<ConsumerRecord<String, String>>();
	/** Service configuration keeper. **/
	protected final Configuration confiuration = Configuration.getConfiguration();
	
	/** KafkaMessagesConsumer logger. **/
	protected final Logger consumerLogger = LogManager.getLogger(this.getClass().getName());
	/** Application level logger. **/
	protected final Logger applicationLogger = LogManager.getLogger("ApplicationLogger");
	
	/** KafkaMessagesConsumer class constructor: **/
	public KafkaMessagesConsumer() {
		/** Add Kafka nodes: **/
		confiuration.getKafkaNodes().forEach(node -> this.nodes.add(node));

		/** Add Kafka topics to watch.: **/
		topics.add(this.confiuration.getKafkaTestManagementTopic());
		topics.add(this.confiuration.getKafkaTestStatisticsTopic());
		
		/** Creating the consumer thread. **/
		this.consumerThread = new Thread(new MessagesConsumer());
		
		/** Creating the kafka messages handler thread. **/
		this.handlerThread = new Thread(new KakfaMessagesHandler());		
	}	

	/** Starting the consumer service : **/
	@Override
	public void start() throws InterruptedException {
		/** **/
		this.__runServer = true;
		/** Starting the consumer thread. **/
		this.consumerThread.start();
		/** Starting the kafka messages handler thread. **/
		this.handlerThread.start();
		/** Wait4for consumerThread done. **/
		consumerThread.join();
	}

	/** Stops the consumer service : **/
	@Override
	public void stop() throws InterruptedException {
		this.__runServer = false;
	}
	
	/** Returns the name of consumer : **/
	@Override
	public String getName() {
		return this.getClass().getName();
	}

	/** Returns the consumer description: **/
	@Override
	public String getDescription() {
		return "Reads messages from Kafka cluster.";
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                         KakfaMessagesHandler                                                      //
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	

	/** Internal KakfaMessagesHandler class : **/
	private class KakfaMessagesHandler implements Runnable {
		/** Messages handler factory builder instance. **/
		private MessageHandlersFactory handlerFactory = new MessageHandlersFactory();
		/** Request parser instance. **/
		private IRequestParser parser = null;
		/** **/
		private final static int sleepTimeout = 1000;
		/** **/
		@SuppressWarnings("unused")
		private int counter = 0;
		
		/** KakfaMessagesHandler class constructor. **/
		protected KakfaMessagesHandler() {
			// TODO
		}
		
		/** Messages handler method. **/
		private void handleMessage(final ConsumerRecord<String, String> kafkaRecord) {
			try {
				this.parser = handlerFactory.getHandler(kafkaRecord);
				if (false == this.parser.handleRequest(kafkaRecord)) {
					applicationLogger.error("Failed to handle message");
				}
			} catch (final MessageFormatExeption exception) {
				consumerLogger.error(exception.getMessage());

				applicationLogger.error(exception.getMessage());
				applicationLogger.error(utilities.Utilities.stackTraceToString(exception));
			}
		}
		
		/** Updates the consumer statistics. **/
		private void updateStats() {
			this.counter++;
		}
		
		/** Sleep : **/
		private void Sleep(int milliseconds) {
			try {
				Thread.currentThread();
				Thread.sleep(milliseconds);
			} catch (InterruptedException exception) {
				exception.printStackTrace();
			}
		}
		
		/** Thread entry point : **/
		public void run() {
			ConsumerRecord<String, String> record = null;
			/** Running until server exists. */
			while (true == KafkaMessagesConsumer.this.__runServer) {
				/** Waking up slowly... **/
				Sleep(KakfaMessagesHandler.sleepTimeout);
				/** No new messages, skip processing: **/
				if (true == messagesQueue.isEmpty()) {
					continue;
				}
				/** Handle all available messages in msg queue. **/
				while (null != (record = messagesQueue.poll())) {
					/** Update statistics. **/
					this.updateStats();
					/** Handling message. **/
					this.handleMessage(record);
				} 
			}
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                         MessagesConsumer                                                          //
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	/** Internal MessagesConsumer class : **/
	private class MessagesConsumer implements Runnable {
		/** **/
		private final static long POLL_INTERVAL_SEC = 5000;
		 /** Kafka properties: **/
		private Properties properties = new Properties();
		/** Kafka consumer : **/
		private KafkaConsumer<String, String> consumer = null;
		/** **/
		private Duration kafkaPollDuration = null;

		/** MessagesConsumer class constructor: **/
		public MessagesConsumer() {
			/** Define properties. **/
			properties.put(ConsumerConfig.GROUP_ID_CONFIG, "BROWSER_AUTOTESTS_SERVER_CONSUMER");
			properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
			properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
			properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, nodes.toString().replace("[", "").replace("]", ""));
			
			/** Create KafkaConsumer instance: **/
			this.consumer = new KafkaConsumer<String, String>(properties);
			
			/** **/
			this.kafkaPollDuration = Duration.of(MessagesConsumer.POLL_INTERVAL_SEC, ChronoUnit.SECONDS);
		}

		/** Reads messages from Kafka cluster: **/
		@Override
		public void run() {
			/** Subscribe consumer to the predefined topics. **/
			consumer.subscribe(topics);
			
			/** Consumer records batch: **/
			ConsumerRecords<String, String> records = null;
			
			consumerLogger.info("Kafka messages consumer started...");
			
			/** Running until server exists: **/
			while (true == KafkaMessagesConsumer.this.__runServer) {
	        	records = consumer.poll(this.kafkaPollDuration);
	        	for (ConsumerRecord<String, String> record : records) {
	        		consumerLogger.debug("Handling message from topic " + record.topic());
	        		consumerLogger.debug(record.value());
	        		messagesQueue.add(record);
	        	}
	        }
			
			/** Unsubscribe & Close. **/
	        consumer.unsubscribe();
	        consumer.close();
		}
	}


	/////////////////// JUST 4 TEST ////////////////////
	public static void main(String[] args) throws InterruptedException, IOException {
		
		// TODO: Move somewhere else
		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		
		System.in.close();
        System.out.close();
		
		KafkaMessagesConsumer consumer = new KafkaMessagesConsumer();
		consumer.start();
	}
}
