//============================================================================
// Name        : IRequestParser.java
// Created on  : June 20, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : IRequestParser interface
//============================================================================

package consumer;

import java.sql.SQLException;
import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface IRequestParser {
	/** Parse request : 
	 * @throws MessageFormatExeption 
	 * @throws SQLException **/
	public boolean handleRequest(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption; //, SQLException;
}
