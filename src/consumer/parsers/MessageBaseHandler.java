//============================================================================
// Name        : MessageBaseHandler.java
// Created on  : August 29, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Message base handler abstract class
//============================================================================

package consumer.parsers;

import java.time.LocalDateTime;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import configuration.Configuration;
import consumer.IRequestParser;
import consumer.JSONRequestParserBase;
import consumer.MessageFormatExeption;
import database.connection.DatabaseConnectionPool;
import database.entry.TestType;

/** @class: MessageBaseHandler.  **/
public abstract class MessageBaseHandler extends JSONRequestParserBase 
									     implements IRequestParser {
	/** Database connection pool instance. **/
	protected DatabaseConnectionPool connectionPool = DatabaseConnectionPool.getInstance();
	/** JSONObject message variable. **/
	protected JSONObject jsonMessage = null;
	/** Service configuration keeper. **/
	protected final static Configuration confiuration = Configuration.getConfiguration();
	
	/** Tests management topic name : **/
	protected final static String testManagementTopic = 
			MessageBaseHandler.confiuration.getKafkaTestManagementTopic();
	/** Tests statistics topic name : **/
	protected final static String testStatisticsTopic = 
			MessageBaseHandler.confiuration.getKafkaTestStatisticsTopic();
	
	/** MessageBaseHandler logger. **/
	protected final Logger consumerLogger = LogManager.getLogger("consumer.parsers.MessagesHandler");
	/** Application level logger. **/
	protected final Logger applicationLogger = LogManager.getLogger("ApplicationLogger");
	
	///////////////////  Message parameters :  ///////////////
	
	/** Message type: **/
	protected TestType type = TestType.Undefined;
	/** Corresponding event start timestamp: **/
	protected LocalDateTime startTimestamp = null;
	/** Message unique identifier (uuid4): **/
	protected String uuid = "";
	/** Message unique identifier (uuid4): **/
	protected String browserVersion = "";
	/** Browser repository target branch: **/
	protected String branch = "";
	
    /** MessageBaseHandler class constructor. **/
    public MessageBaseHandler() {
		// TODO. Do something.
    }
    
    /** Parses the message received. **/
	@Override
	public boolean handleRequest(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption {
		try {
			this.jsonMessage = (JSONObject) (new JSONParser().parse(kafkaRecord.value()));
		} catch (ParseException exc) {
			throw new MessageFormatExeption(exc);
		}
		
		/** Extract message type: **/
		this.type = TestType.fromString(this.getType(this.jsonMessage));

		/** Extract the event start time: **/
		this.startTimestamp = this.getStartTimestamp(this.jsonMessage);
		
		/** Extract uuid: **/
		this.uuid = this.getUUID(this.jsonMessage);
		
		/** Extract browser version associated with build: **/
		this.browserVersion = this.getBrowserVersion(this.jsonMessage);
		
		/** Extract branch value: **/
		this.branch = this.getBranch(this.jsonMessage);	

		// Return TRUE if get here.
		return true;
	}
	
	protected void logMessage() {
		String params = "\n   type : " + type;
		params += "\n   startTs : " + startTimestamp + "\n   browserVersion : " + browserVersion;
		params += "\n   branch : " + branch + "\n   uuid : " + uuid;
		
		consumerLogger.debug("Message : " + params);
	}
}
