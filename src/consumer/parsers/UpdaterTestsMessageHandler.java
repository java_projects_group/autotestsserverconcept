//============================================================================
//Name        : UpdaterTestsMessageHandler.java
//Created on  : Aug 27, 2019
//Author      : Tokmakov Andrey
//Version     : 1.0
//Copyright   : Your copyright notice
//Description : Installer tests message parser class
//============================================================================

package consumer.parsers;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import consumer.MessageFormatExeption;
import database.entry.AutotestsRecord;
import database.entry.BuildRecord;
import database.entry.TestStatus;
import database.entry.UnitTestsRecord;
import database.tables.AutotestsTable;
import database.tables.BuildsTable;
import utilities.Utilities;

/** @class : UpdaterTestsMessageHandler. **/
public class UpdaterTestsMessageHandler extends MessageBaseHandler {
	/** Builds table. **/
	protected BuildsTable buildsTable = BuildsTable.getTable();
	/** AutotestsTable table. **/
	protected AutotestsTable autotestsTable = AutotestsTable.getTable();
	
	/** Message parameters : **/
	protected String os = "";	
	protected String testResults = "";
	protected String workerName = "";
	protected TestStatus status = TestStatus.Undefined;
	protected LocalDateTime endTimestamp = null;

	/** UpdaterTestsMessageHandler class constructor : **/
	public UpdaterTestsMessageHandler() {
		// TODO. Add logging.
	} 
	
	protected boolean handleTestManagementMessage(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption {
		applicationLogger.info("[Updater tests consumer] : Message received from topic " + testManagementTopic);
		applicationLogger.info(kafkaRecord.value());
		return false;
	}
	
	protected boolean handleTestStatsMessage(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption  {
		/** Extract OS : **/
		os = this.getOS(jsonMessage);
		
		/** Extract worker_name : **/
		workerName = this.getWorkerName(jsonMessage);
		
		/** Extract 'status': **/
		this.status = TestStatus.fromString(this.getStatus(this.jsonMessage));
		if (TestStatus.Undefined == this.status) {
			throw new MessageFormatExeption("Failed to determine status.");
		}
		
		boolean result = false;
		BuildRecord build = this.buildsTable.get(uuid);
		if (null == build) {
			applicationLogger.error("ERROR. Failed to find corresponding Build record for uuid:" + uuid);
			return result;
		}
		
		AutotestsRecord testRecort  = new AutotestsRecord();
		testRecort.setId(build.getId());
		testRecort.setUuid(this.uuid);
		testRecort.setTestsType(this.type);
		testRecort.setTestStatus(this.status);
		testRecort.setWorkerName(this.workerName);
		testRecort.setOs(os);
		testRecort.setStartTime(this.startTimestamp);
		
		if (TestStatus.TestsStarted == this.status) {
			result = autotestsTable.add(testRecort);
		}
		if (TestStatus.TestsCompleted == this.status) {
			endTimestamp = this.getEndTimestamp(jsonMessage);
			testResults = this.getTestsResults(jsonMessage);
			testRecort.setEndTime(endTimestamp);
			testRecort.setResultJson(testResults);
			parserResults(testResults, testRecort);
			
			result = autotestsTable.update(testRecort);
		}

		this.logMessage();
		return result;
	}
 
	@Override
	public boolean handleRequest(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption {
		/** Put some info. **/
		applicationLogger.debug("Using " + this.getClass().getName() + " to handle message.");
		/** Invoke super class 'handleRequest' method. **/
		super.handleRequest(kafkaRecord);
		
		if (true == kafkaRecord.topic().equals(testManagementTopic)) {
			return this.handleTestManagementMessage(kafkaRecord);
		} else if (true == kafkaRecord.topic().equals(testStatisticsTopic)) {
			return this.handleTestStatsMessage(kafkaRecord);
		}
		// TODO: Logs
		return false;
	}
	
    /**
     * Parse JSON unit test results and sets failed, passed and skipped tests count
     * @return True in case of successful operation.
     * 		   False in case of any kind error.
     */
	public boolean parserResults(final String jsonResult, 
								 AutotestsRecord record)  {
		this.applicationLogger.debug("Parsing updater tests results.  UUID = " + record.getUuid());
		try {
			final JSONObject jsonObj = (JSONObject)new JSONParser().parse(jsonResult);
			JSONArray test_passed = (JSONArray)jsonObj.get("passed");
			JSONArray test_failed = (JSONArray)jsonObj.get("failed");		
			long testsPassed = Arrays.asList(test_passed.toArray()).size();
			long testsfailed = Arrays.asList(test_failed.toArray()).size();
			record.setTestsPassed(testsPassed);
			record.setTestsFailed(testsfailed);
			return true;
		} catch (final ParseException exception) {
			this.applicationLogger.error("Failed to parser JSON test results for AutotestsRecord record with UUID = " + record.getUuid());
			this.applicationLogger.error(Utilities.stackTraceToString(exception));
			return false;
		}
	}
	
	@Override
	protected void logMessage() {
		//super.logMessage();
		String params = "\n   type : " + type;
		params += "\n   status : " + status;
		params += "\n   startTs : " + startTimestamp;
		params += "\n   browserVersion : " + browserVersion;
		params += "\n   branch : " + branch;
		params += "\n   uuid : " + uuid;
		params += "\n   endTS : " + endTimestamp;
	  //params += "\n   testsStatus : " + testsStatus;
		params += "\n   workerName : ";
		params += workerName;
		params += "\n   os : " + os;
		params += "\n   TestResult : " + testResults;
		
		consumerLogger.debug("Message : " + params);
		applicationLogger.debug("Message : " + params);
	}
}
