//============================================================================
// Name        : AVTestsMessageHandler.java
// Created on  : December 17, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Antivirueses tests message parser class
//============================================================================

package consumer.parsers;

import java.time.LocalDateTime;

import org.apache.kafka.clients.consumer.ConsumerRecord;

import consumer.MessageFormatExeption;
import database.entry.TestStatus;
import database.tables.AutotestsTable;
import database.tables.BuildsTable;

/** @class : AVTestsMessageHandler. **/
public class AVTestsMessageHandler extends MessageBaseHandler {
	/** Builds table. **/
	protected BuildsTable buildsTable = BuildsTable.getTable();
	
	/** AutotestsTable table. **/
	protected AutotestsTable autotestsTable = AutotestsTable.getTable();
	
	/** Message parameters : **/
	protected String os = "";	
	protected String testResults = "";
	protected String workerName = "";
	protected TestStatus status = TestStatus.Undefined;
	protected LocalDateTime endTimestamp = null;

	/** UpdaterTestsMessageHandler class constructor : **/
	public AVTestsMessageHandler() {
		// TODO. Add logging.
	}
	
	@Override
	public boolean handleRequest(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption {
		/** Put some info. **/
		applicationLogger.debug("Using " + this.getClass().getName() + " to handle message.");
		/** Invoke super class 'handleRequest' method. **/
		super.handleRequest(kafkaRecord);
		
		
		applicationLogger.debug("Topic: " + kafkaRecord.topic());
		applicationLogger.debug("Message: " + kafkaRecord.value());
		
		/*
		if (true == kafkaRecord.topic().equals(testManagementTopic)) {
			return this.handleTestManagementMessage(kafkaRecord);
		} else if (true == kafkaRecord.topic().equals(testStatisticsTopic)) {
			return this.handleTestStatsMessage(kafkaRecord);
		}*/
		
		
		// TODO: Logs
		return true;
	}
	
	@Override
	protected void logMessage() {
		//super.logMessage();
		String params = "\n   type : " + type;
		params += "\n   status : " + status;
		params += "\n   startTs : " + startTimestamp;
		params += "\n   browserVersion : " + browserVersion;
		params += "\n   branch : " + branch;
		params += "\n   uuid : " + uuid;
		//params += "\n   endTS : " + endTimestamp;
	    //params += "\n   testsStatus : " + testsStatus;
		params += "\n   workerName : ";
		params += workerName;
		params += "\n   os : " + os;
		params += "\n   TestResult : " + testResults;
		
		consumerLogger.debug("Message : " + params);
		applicationLogger.debug("Message : " + params);
	}	
}
