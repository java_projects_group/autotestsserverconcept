//============================================================================
// Name        : BuildbotTestRunHandler.java
// Created on  : June 20, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : BuildbotTestRunParser interface
//============================================================================

package consumer.parsers;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import consumer.MessageFormatExeption;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.entry.UnitTestsRecord;
import database.tables.BuildsTable;
import database.tables.UnitTestsTable;
import utilities.Utilities;

/** IConsumer interface declaration: **/
public class BuildbotTestRunHandler extends MessageBaseHandler {
	/** Builds table. **/
	protected BuildsTable buildsTable = BuildsTable.getTable();
	/** Unit tests table. **/	
	protected UnitTestsTable unitTestTable  = UnitTestsTable.getTable();

	/** Message parameters : **/
	protected BuildStatus status = BuildStatus.Undefined;
	protected LocalDateTime endTimestamp = null;
	protected String testsStatus = "";
	protected List<String> owners = null;
	protected String repository = "";
	protected String revision = "";
	protected String testsResults = "";
	protected String workerName = "";
	protected long buildnumber = -1;
	protected boolean isNightly = false;
    
    /** BuildbotTestRunHandler class constructor : **/
    public BuildbotTestRunHandler() {
		// TODO. Add logging.
    } 
    
	@Override
	public boolean handleRequest(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption {
		/** Put some info. **/
		applicationLogger.debug("Using " + this.getClass().getName() + " to handle message.");
		
		/** Invoke super class 'handleRequest' method. **/
		super.handleRequest(kafkaRecord);
		
		if (true == kafkaRecord.topic().equals(testManagementTopic)) {
			return this.handleTestManagementMessage(kafkaRecord);
		} else if (true == kafkaRecord.topic().equals(testStatisticsTopic)) {
			return this.handleTestStatsMessage(kafkaRecord);
		}
		
		// TODO: Logs
		return false;
	}
	
	protected boolean handleTestManagementMessage(final ConsumerRecord<String, String> kafkaRecord) throws MessageFormatExeption {
		/** Extract 'status': **/
		this.status = BuildStatus.fromString(this.getStatus(this.jsonMessage));
		if (BuildStatus.Undefined == this.status) {
			throw new MessageFormatExeption("Failed to determine status.");
		}
		
		this.isNightly = this.getIsNightly(jsonMessage);
		this.owners = this.getOwners(jsonMessage);
		this.repository = this.getRepository(jsonMessage);
		this.revision = this.getRevision(jsonMessage);
		this.buildnumber = this.getBuildnumber(jsonMessage);

		
		BuildRecord build = new BuildRecord();
		build.setUuid(uuid);
		build.setStatus(status);
		build.setBuildnumber(buildnumber);
		build.setNightly(isNightly);
		build.setBrowserVersion(browserVersion);
		build.setBranch(branch);
		build.setRepository(repository);
		build.setRevision(revision);
		build.setOwners(owners);
		build.setStartTime(this.startTimestamp);
		
		this.logMessage();
		
		if (BuildStatus.BuildStarted == status) {
			buildsTable.add(build);
		} else {
			// Set START_TIMESTAMP to zero since we have to keep original START_TIME
			// in DB retrieved from 'BuildStarted' message
			if (BuildStatus.TestsCompleted == status) {
				endTimestamp = this.getEndTimestamp(jsonMessage);
				build.setEndTime(endTimestamp);
			}
			
			buildsTable.update(build);
		}
		return true;	
	}
	
	protected boolean handleTestStatsMessage(final ConsumerRecord<String, String> kafkaRecord)
												throws MessageFormatExeption 
	{
		endTimestamp = this.getEndTimestamp(jsonMessage);
		testsResults = this.getTestsResults(jsonMessage);
		workerName = this.getWorkerName(jsonMessage);

		BuildRecord build = this.buildsTable.get(uuid);
		if (null == build) {
			applicationLogger.error("ERROR. Failed to find corresponding Build record for uuid:" + uuid);
			return false;
		}
		
		UnitTestsRecord record = new UnitTestsRecord();
		record.setId(build.getId());
		record.setUuid(uuid);
		record.setWorkerName(workerName);
		record.setStartTime(startTimestamp);
		record.setEndTime(endTimestamp);
		record.setResultJson(testsResults);
		this.parseJsonResults(testsResults, record);

		return this.unitTestTable.add(record);
	}

	@Override
	protected void logMessage() {
		//super.logMessage();
		String params = "\n   type : " + type + "\n   status : " + status;
		params += "\n   startTs : " + startTimestamp + "\n   browserVersion : " + browserVersion;
		params += "\n   branch : " + branch + "\n   uuid : " + uuid;
		// TODO : Do we need to include END_TIME
		params += "\n   endTS : " + endTimestamp + "\n   testsStatus : " + testsStatus;
		params += "\n   repository : " + repository + "\n   revision : " + revision;
		params += "\n   buildnumber : " + buildnumber + "\n   isNightly : " + isNightly;
		params += "\n   owner : " + owners;
		
		this.consumerLogger.debug("Message : " + params);
		this.applicationLogger.debug("Message : " + params);
	}
	
    /**
     * Parse JSON unit test results and sets failed, passed and skipped tests count
     * @return True in case of successful operation.
     * 		   False in case of any kind error.
     */
	protected boolean parseJsonResults(final String jsonResult, 
									   UnitTestsRecord record)  {
		this.applicationLogger.debug("Parsing unit tests results.  UUID = " + record.getUuid());
		try {
			final JSONObject jsonObj = (JSONObject)new JSONParser().parse(jsonResult);
			final Set<?> keys = jsonObj.keySet();
			long testsPassedCount = 0, testsFailedCount = 0, testsSkippedount = 0;
			for (final Object obj : keys) {
				final JSONObject results = (JSONObject)jsonObj.get(obj);
				// final String testName = (String)obj;
				testsPassedCount += (long) results.get("passed");
				testsFailedCount += (long) results.get("failed");
				testsSkippedount += (long) results.get("skipped");
			} 
			
			record.setTestsPassed(testsPassedCount);
			record.setTestsFailed(testsFailedCount);
			record.setTestsSkipped (testsSkippedount);
			
			return true;
		} catch (final java.lang.ClassCastException castException) {
			this.applicationLogger.error(Utilities.stackTraceToString(castException));
			return false;		
		} catch (final ParseException parseException) {
			this.applicationLogger.error("Failed to parser JSON test results for UnitTestsRecord record with UUID = " + record.getUuid());
			this.applicationLogger.error(Utilities.stackTraceToString(parseException));
			return false;
		}
	}
}
