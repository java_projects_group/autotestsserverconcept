//============================================================================
// Name        : MessageFormatExeption.java
// Created on  : June 20, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : MessageFormatExeption class
//============================================================================

package consumer;

// TODO : Add all other constructos and reqired methods
// MessageFormatExeption
public class MessageFormatExeption extends Exception {
	/**  **/
	private static final long serialVersionUID = 7297960768363527734L;
	
	/** **/
	public MessageFormatExeption(String reason) {
		super(reason);
	}
	
	/** **/
	public MessageFormatExeption(String mgsType, String missingParamName) {
		super("Parsing message type " + mgsType + " : Failed to find parameter " + missingParamName);
	}
	
	/** **/
    public MessageFormatExeption(Throwable cause) {
    	super(cause);
    }
	
	/** **/
    public MessageFormatExeption(String message, Throwable throwable) {
        super(message, throwable);
    }
}
