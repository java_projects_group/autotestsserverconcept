//============================================================================
// Name        : TestResult.java
// Created on  : Aug 27, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Tests result class
//============================================================================

package consumer;

/** @class TestResult. **/
public class TestResult {
	/** Tests passed. **/
	private long passed;
	/** Tests failed.  **/
	private long failed;
	/** Tests skipped.  **/
	private long skipped;	
	/** Unstable tests. **/
	private long flaky;
	
	/** TestResult class default constructor: **/
	public TestResult() {
		this(0, 0, 0, 0);
	}
	/** TestResult class constructor: **/
	public TestResult(long passed, long failes, long flaky, long skipped) {
		this.passed = passed;
		this.failed = failes;
		this.skipped = skipped;
		this.flaky = flaky;
	}
	
	public long getPassed() {
		return passed;
	}
	public void setPassed(long passed) {
		this.passed = passed;
	}
	public long getFailed() {
		return failed;
	}
	public void setFailed(long failed) {
		this.failed = failed;
	}
	public long getSkipped() {
		return skipped;
	}
	public void setSkipped(long skipped) {
		this.skipped = skipped;
	}	
	public long getFlaky() {
		return flaky;
	}
	public void setFlaky(long flaky) {
		this.flaky = flaky;
	}
	
	/** Overrides toString() Object method:  **/
	@Override 
	public String toString() {
		return "[passed: " + this.getPassed()  + 
				", failed: "  + this.getFailed() + 
				", skipped: " + this.getSkipped() + 
				", flaky: " + this.getFlaky() + "]";
	}
}
