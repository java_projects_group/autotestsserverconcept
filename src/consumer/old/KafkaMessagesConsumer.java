//============================================================================
// Name        : KafkaMessagesConsumer.java
// Created on  : July 23, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : JsonMessageParser class
//=====================KafkaConsumer======================================================

package consumer.old;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

//Kafka imports:
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import consumer.IConsumer;
import consumer.IRequestParser;
import consumer.MessageFormatExeption;
import consumer.MessageHandlersFactory;

/** @class KafkaMessagesConsumer class : **/
public class KafkaMessagesConsumer implements IConsumer {
	/** Run server flag : **/
	private volatile boolean __runServer = false;
    /** Kafka properties: **/
	private Properties properties = new Properties();
	/** Kafka consumer : **/
	private KafkaConsumer<String, String> consumer = null;
	/* List of kafka topics to listen : */
	private List<String> topics = new ArrayList<String>();
	/* List of kafka cluster nodes: */
	private List<String> nodes = new ArrayList<String>();
	/** **/
	private final static long POLL_INTERVAL_SEC = 5000;
	/** **/
	private Duration kafkaPollDuration = null;
	/** Messages handler factory builder instance. **/
	private MessageHandlersFactory handlerFactory = new MessageHandlersFactory();
	/** Request parser instance. **/
	private IRequestParser parser = null;
	
	/** KafkaMessagesConsumer class constructor: **/
	public KafkaMessagesConsumer() {
		/** Add kafka nodes: **/
		nodes.add("100.99.4.168:9092");

		/** Add kafka topics to watch.: **/
		topics.add("TutorialTopic");
		
		/** Define properties. **/
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, "TESTSSTSS");
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, nodes.toString().replace("[", "").replace("]", ""));
		
		/** Create KafkaConsumer instance: **/
		consumer = new KafkaConsumer<String, String>(properties);
		
		/** **/
		kafkaPollDuration = Duration.of(KafkaMessagesConsumer.POLL_INTERVAL_SEC, ChronoUnit.SECONDS);
	}

	/** Starting the consumer service : **/
	@Override
	public void start() throws InterruptedException {
		/** **/
		this.__runServer = true;
		/** **/
		this.Consume();
	}

	/** Stops the consumer service : **/
	@Override
	public void stop() throws InterruptedException {
		this.__runServer = false;
	}
	
	/** Returns the name of consumer : **/
	@Override
	public String getName() {
		return KafkaMessagesConsumer.class.getName();
	}

	/** Returns the consumer description: **/
	@Override
	public String getDescription() {
		return "Reads messages from Kafka cluster.";
	}
	
	/** Reads messages from Fafka cluster: **/
	private void Consume() {
		/** Subscribe consumer to the predefined topics. **/
		consumer.subscribe(topics);
		
		/** Consumer records banch: **/
		ConsumerRecords<String, String> records = null;
		
		/** Running until server exists: **/
		while (true == KafkaMessagesConsumer.this.__runServer) {
        	records = consumer.poll(this.kafkaPollDuration);
        	for (ConsumerRecord<String, String> record : records) {
    			try {
    				parser = handlerFactory.getHandler(record);
    				parser.handleRequest(record);
    			} catch (MessageFormatExeption exception) {
    				exception.printStackTrace();
    				// TODO : Save message somewhere
    			}
        	}
        }
		
		/** Unsubscribe & Close. **/
        consumer.unsubscribe();
        consumer.close();
	}
	
	/////////////////// JUST 4 TEST ////////////////////
	public static void main(String[] args) throws InterruptedException {
		KafkaMessagesConsumer consumer = new KafkaMessagesConsumer();
		consumer.start();
	}
}
