package consumer.old;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

import consumer.IConsumer;
import consumer.MessageFormatExeption;
import consumer.MessageHandlersFactory;

/** @class TestStatisticsConsumer class : **/
public class TestStatisticsConsumer implements IConsumer {
	/** Receive buffer max size: **/
	private final static int BUFFER_SIZE = 1024;
	/** Server bind port: **/
	private final static int DEFAULT_PORT = 52525;
	/** Server bind IP address: **/
	private final static String HOST_ADDRESS = "0.0.0.0";
	/** **/
	private InetSocketAddress socketAddress = null;
	/** **/
	private ConcurrentLinkedQueue<String> messagesQueue = new ConcurrentLinkedQueue<String>();
	/** **/
	private Thread handlerThread = null;
	/** **/
	private Thread receiverThread = null;
	/** **/
	private volatile boolean __runServer = false;
	/** **/
	@SuppressWarnings("unused")
	private MessageHandlersFactory msgHandlersFactory = new MessageHandlersFactory();
	
	/** Server default constructor : **/
	public TestStatisticsConsumer() throws IOException {
		socketAddress = new InetSocketAddress(HOST_ADDRESS, DEFAULT_PORT);
		handlerThread = new Thread(new MessagesHandler());
		receiverThread = new Thread(new MessagesReceiver());
	}
	
	/** Sleep : **/
	private void Sleep(int milliseconds) {
		try {
			Thread.currentThread();
			Thread.sleep(milliseconds);
		} catch (InterruptedException exception) {
			exception.printStackTrace();
		}
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                         MessagesHandler                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/** Internal MessagesHandler class : **/
	class MessagesHandler implements Runnable {
		/** **/
		private final static int sleepTimeout = 1000;
		/** **/
		private int counter = 0;
		
		//private IRequestParser parser = new BuildbotTestRunParser();
		
		/** MessagesHandler class constructor. **/
		protected MessagesHandler() {
			// TODO
			System.out.println("MessagesHandler()");
		}
		
		/** Thread entry point : **/
		public void run() {
			String message = "";
			/** Running until server exists. */
			while (true == TestStatisticsConsumer.this.__runServer) {
				/** Waking up slowly... **/
				Sleep(sleepTimeout);
				/** No new messages, skip processing: **/
				if (true == messagesQueue.isEmpty()) {
					continue;
				}
				/** Handle all available messages in msg queue. **/
				while (null != (message = messagesQueue.poll())) {
					/** Update statistics. **/
					this.updateStats();
					/** Handling message. **/
					this.handleMessage(message);
				} 
			}
		}
		
		/** Messages handler method. **/
		private void handleMessage(final String message) {
			try {
				
				/*
				 
				 TO OLD
				 
				IRequestParser parser = msgHandlersFactory.getHandler(message);
				parser.handleRequest(message);
				
				*/
				throw new MessageFormatExeption("");
			} catch (MessageFormatExeption exception) {
				exception.printStackTrace();
				// TODO : Save message somewhere
			}
		}
		
		/** updateStats : Updates the consumer statistics. **/
		private void updateStats() {
			counter++;
		}

		/** @return the counter. **/
		public int getCounter() {
			return counter;
		}
	}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										   MessagesReceiver                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
	/** Internal MessagesReceiver class : **/
	class MessagesReceiver implements Runnable {
		/** Static CharsetDecoder instance : **/
		private final Charset charset = Charset.forName("ISO-8859-1");
		/** Static CharsetDecoder instance : **/
		private final CharsetDecoder decoder = charset.newDecoder();
		// The buffer into which we'll read data when it's available
		private ByteBuffer readBuffer = null;
		// Socket channel selector instance.
		private Selector selector = null;
		
		/** MessagesReceiver class constructor : **/
		protected MessagesReceiver() throws IOException {
			// TODO
			System.out.println("MessagesReceiver()");
			readBuffer = ByteBuffer.allocate(BUFFER_SIZE);
		    selector = initSelector();
		}
		
		/** MessagesReceiver thread entry point method. **/
		@Override
		public void run() {
			this.runServer();
		}
		
		/** Start server method : **/
		private void runServer() {
		    while (true) {
		        try{
		            selector.select();
		            Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
		            while (selectedKeys.hasNext()) {
		            	/** **/
		                SelectionKey key = selectedKeys.next();
		                selectedKeys.remove();
		                /** **/
		                if (false == key.isValid()) {
		                    continue;
		                }
		                /** Check what event is available and deal with it: **/
		                if (key.isAcceptable()) {
		                    accept(key);
		                } else if (key.isReadable()) {
		                	read(key);
		                } else if (key.isWritable()) {
		                   //write(key);
		                }
		            }
		        } catch (Exception e) {
		            e.printStackTrace();
		            System.exit(1);
		        }
		    }
		}
		
		/** Accepts the connection request. **/
		private void accept(SelectionKey key) throws IOException {
		    ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
		    SocketChannel socketChannel = serverSocketChannel.accept();
		    socketChannel.configureBlocking(false);
		    socketChannel.setOption(StandardSocketOptions.SO_KEEPALIVE, true);
		    socketChannel.setOption(StandardSocketOptions.TCP_NODELAY, true);
		    socketChannel.register(selector, SelectionKey.OP_READ);
		    // TODO: Log client connection information
		}
		
		/** Reads data from socket channel. **/
		private void read(SelectionKey key) throws IOException {
			SocketChannel socketChannel = (SocketChannel)key.channel();
			String request = "";
			try {
				/** Read all data from opened channel. **/
				while (socketChannel.read(readBuffer) > 0) {
					readBuffer.flip();
					request += decoder.decode(readBuffer).toString();
					readBuffer.clear();
				}
				/** Add message to queue. **/
				this.addMessage(request);
				/** Close the channel. **/
				socketChannel.close();
				/** Cancel selection on key. **/
				key.cancel();
			} catch (final IOException exception) {
				exception.printStackTrace();
				socketChannel.close();
		        key.cancel();
			}
		}
		
		/** Add message to messages queue. **/
		private void addMessage(final String msg) {
			messagesQueue.add(msg);
		}
		
		@SuppressWarnings("unused")
		private void write(SelectionKey key) throws IOException {
			
		    SocketChannel socketChannel = (SocketChannel) key.channel();
		    ByteBuffer dummyResponse = ByteBuffer.wrap("TEST\n".getBytes("UTF-8"));
		    socketChannel.write(dummyResponse);
		    if (dummyResponse.remaining() > 0) {
		        System.err.print("Filled UP");
		    }
		    key.interestOps(SelectionKey.OP_READ);
		    
			System.out.println("Write....");
		}
		
		/** Initialize selector. **/
		private Selector initSelector() throws IOException {
		    Selector socketSelector = SelectorProvider.provider().openSelector();
		    ServerSocketChannel serverChannel = ServerSocketChannel.open();
		    serverChannel.configureBlocking(false);
		    serverChannel.socket().bind(socketAddress);
		    serverChannel.register(socketSelector, SelectionKey.OP_ACCEPT);
		    return socketSelector;
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                           
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	

	
	public static void main(String[] args) throws InterruptedException, IOException {
		
		IConsumer statConsumer = new TestStatisticsConsumer();
		statConsumer.start();
	}

	@Override
	public void start() throws InterruptedException {
		this.__runServer = true;
		handlerThread.start();
		receiverThread.start();
		receiverThread.join();
	}

	@Override
	public void stop() throws InterruptedException {
		this.__runServer = false;
		
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}
}