//============================================================================
// Name        : JSONRequestParserBase.java
// Created on  : July 04, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : JsonMessageParser class
//============================================================================

package consumer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;

/** JSONRequestParserBase class. **/
public class JSONRequestParserBase {
	/** Worker name JSON parameter name : **/
	private final static String workerNameParam = "worker_name";
    /** Start time JSON parameter name : **/
	private final static String startTimeParam = "start_time";
    /** End time JSON parameter name : **/
	private final static String endTimeParam = "end_time";
    /** In nightly JSON parameter name : **/
	private final static String isNightlyParam = "is_nightly";
    /** Test result list JSON parameter name : **/
	protected final static String testRunParam = "tests";
	/** Passed test parameter name : **/
	private final static String testsPassedParam = "passed";
	/** Failed test parameter name : **/
	private final static String testsFailedParam = "failed";
	/** Skipped test parameter name : **/
	private final static String testsSkippedParam = "skipped";
	/** Flaky test parameter name : **/
	private final static String testsFlakyParam = "flaky";	
	/** Test type JSON parameter name : **/
	private final static String type = "type";
	/** JSON message 'status' parameter name : **/
	private final static String status = "status";
	/** JSON message 'branch' parameter name : **/
	private final static String branch = "branch";
	/** JSON message 'owners' parameter name : **/
	private final static String owners = "owners";
	/** JSON message 'repository' parameter name : **/
	private final static String repository = "repository";
	/** JSON message 'buildnumber' parameter name : **/
	private final static String buildnumber = "buildnumber";
	/** JSON message 'revision' parameter name : **/
	private final static String revision = "revision";
	/** JSON message 'browser_version' parameter name : **/
	private final static String browser_version = "browser_version";
	/** JSON message 'uuid' parameter name : **/
	private final static String uuid = "uuid";
	/** JSON message 'tests_status' parameter name : **/
	private final static String testsStatus = "tests_status";
	/** JSON message 'os' parameter name : **/
	private final static String os = "os";
	/** JSON message 'result' parameter name : **/
	private final static String result = "result";
	
	/** DateTime formatter: **/
	private final static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	protected TestResult getTestsRunResults(final JSONObject jsonRoot) throws MessageFormatExeption {
		return this.getTestsResult(getJsonParameterValue(jsonRoot, result));
	}
	
	protected TestResult getTestsResult(final JSONObject resultJson) throws MessageFormatExeption {
		TestResult testResult = new TestResult();
		testResult.setPassed(getPassedTestValue(resultJson, 0));
		testResult.setFailed(getFailedTestValue(resultJson, 0));
		testResult.setSkipped(getSkippedTestValue(resultJson, 0));
		testResult.setFlaky(getFlakyTestValue(resultJson, 0));	
		return testResult;
	}
	
	protected String getOS(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, os);
	}
	
	protected String getTestsStatus(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, testsStatus);
	}
	
	protected String getBranch(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, branch);
	}
	
	protected List<String> getOwners(final JSONObject jsonRoot) throws MessageFormatExeption {
		if (null == jsonRoot.get(owners)) {
			return new ArrayList<String>();
		}
		return getStringArray(jsonRoot, owners);
	}
	
	protected String getRepository(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, repository);
	}
	
	protected long getBuildnumber(final JSONObject jsonTestResult) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, buildnumber);
	}
	
	protected String getRevision(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, revision);
	}
	
	protected String getBrowserVersion(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, browser_version);
	}
	
	protected String getUUID(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, uuid);
	}

	protected String getWorkerName(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, workerNameParam);
	}
	
	protected String getType(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, type);
	}
	
	protected String getStatus(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, status);
	}
	
	protected String getTestsResults(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getJsonParameterValue(jsonRoot, testRunParam).toJSONString();
	}
	
	protected String getStartTimestampAsString(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, startTimeParam);
	}
	
	protected LocalDateTime getStartTimestamp(final JSONObject jsonRoot) throws MessageFormatExeption {
		return LocalDateTime.parse(getStartTimestampAsString(jsonRoot), dateTimeFormatter);
	}	
	
	protected String getEndTimestampAsString(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getStringParamValue(jsonRoot, endTimeParam);
	}
	
	protected LocalDateTime getEndTimestamp(final JSONObject jsonRoot) throws MessageFormatExeption {
		return LocalDateTime.parse(getEndTimestampAsString(jsonRoot), dateTimeFormatter);
	}		
	
	protected Boolean getIsNightly(final JSONObject jsonRoot) throws MessageFormatExeption {
		return getBooleanParamValue(jsonRoot, isNightlyParam);
	}
	
	protected long getPassedTestValue(final JSONObject jsonTestResult) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, testsPassedParam);
	}
	
	protected long getPassedTestValue(final JSONObject jsonTestResult,
									  long defaultValue) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, testsPassedParam, defaultValue);
	}	
	
	protected long getFailedTestValue(final JSONObject jsonTestResult) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, testsFailedParam);
	}
	
	protected long getFailedTestValue(final JSONObject jsonTestResult,
			  						  long defaultValue) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, testsFailedParam, defaultValue);
	}	
	
	protected long getSkippedTestValue(final JSONObject jsonTestResult) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, testsSkippedParam);
	}
	
	protected long getSkippedTestValue(final JSONObject jsonTestResult,
					  				   long defaultValue) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, testsSkippedParam, defaultValue);
	}
	
	protected long getFlakyTestValue(final JSONObject jsonTestResult,
			   long defaultValue) throws MessageFormatExeption {
		return getLongParamValue(jsonTestResult, testsFlakyParam, defaultValue);
	}	
	
	protected void getTestsResultsEx(final JSONObject jsonRoot) throws MessageFormatExeption {
		JSONObject tests = (JSONObject) jsonRoot.get(testRunParam);
		for (Object jsonObj : tests.entrySet()) {
			@SuppressWarnings("rawtypes")
    		Map.Entry test = (Map.Entry) jsonObj;
			String testName = (String) test.getKey();
			TestResult testResult = this.getTestsResult((JSONObject) test.getValue());
			System.out.println(testName + " = " + testResult);
		}
	}
	
	/************************************************************************************************/
	
	private JSONObject getJsonParameterValue(final JSONObject jsonRoot, 
		             						 final String paramName) throws MessageFormatExeption {
		if (false == jsonRoot.containsKey(paramName)) {
			throw new MessageFormatExeption("Failed to find parameter " + paramName + "\nOriginal message: " + jsonRoot);
		} 
		return (JSONObject)jsonRoot.get(paramName); 
	}
	
	private List<String> getStringArray(final JSONObject jsonRoot, 
			   					        final String paramName) throws MessageFormatExeption {
		if (false == jsonRoot.containsKey(paramName)) {
			throw new MessageFormatExeption("Failed to find parameter " + paramName + "\nOriginal message: " + jsonRoot);
		} 
		try {
			final JSONArray jsonArray  = (JSONArray)jsonRoot.get(paramName);
			List<String> __list = new ArrayList<String>();
			for(int i = 0; i < jsonArray.size(); i++)
				__list.add((String)jsonArray.get(i));
			return __list;
		} catch (final java.lang.ClassCastException castExc) {
			// TODO REfactor call to get() inside MessageFormatExeption
			// TODO Add info from  ClassCastException
			throw new MessageFormatExeption("Failed to cast parameter '" + paramName 
											+ "' value '" + jsonRoot.get(paramName) + "' to String.");
		} catch (final java.lang.Exception some_other_exception) {
			throw new MessageFormatExeption(some_other_exception);
		}
	}

	private String getStringParamValue(final JSONObject jsonRoot, 
				 					   final String paramName) throws MessageFormatExeption {
		if (false == jsonRoot.containsKey(paramName)) {
			throw new MessageFormatExeption("Failed to find parameter " + paramName + "\nOriginal message: " + jsonRoot);
		} 
		try {
			return (String)jsonRoot.get(paramName);
		} catch (java.lang.ClassCastException castExc) {
			// TODO REfactor call to get() inside MessageFormatExeption
			// TODO Add info from  ClassCastException
			throw new MessageFormatExeption("Failed to cast parameter '" + paramName 
											+ "' value '" + jsonRoot.get(paramName) + "' to String.\nOriginal message: " + jsonRoot);
		} catch (final java.lang.Exception some_other_exception) {
			throw new MessageFormatExeption(some_other_exception);
		}
	}
	
	private Boolean getBooleanParamValue(final JSONObject jsonRoot, 
				   						 final String paramName) throws MessageFormatExeption {
		if (false == jsonRoot.containsKey(paramName)) {
			throw new MessageFormatExeption("Failed to find parameter " + paramName + "\nOriginal message: " + jsonRoot);
		} 
		try {
			return (Boolean)jsonRoot.get(paramName);
		} catch (java.lang.ClassCastException castExc) {
			// TODO REfactor call to get() inside MessageFormatExeption
			// TODO Add info from  ClassCastException
			throw new MessageFormatExeption("Failed to cast parameter '" + paramName 
											+ "' value '" + jsonRoot.get(paramName) + "' to Boolean.\nOriginal message: " + jsonRoot);
		} catch (final java.lang.Exception some_other_exception) {
			throw new MessageFormatExeption(some_other_exception);
		}
	}
	
	@SuppressWarnings("unused")
	private int getIntegerParamValue(final JSONObject jsonRoot, 
				  					 final String paramName) throws MessageFormatExeption {
		if (false == jsonRoot.containsKey(paramName)) {
			throw new MessageFormatExeption("Failed to find parameter " + paramName + "\nOriginal message: " + jsonRoot);
		}
		try {
			return (Integer)jsonRoot.get(paramName);
		} catch (java.lang.ClassCastException castExc) {
			// TODO REfactor call to get() inside MessageFormatExeption
			// TODO Add info from  ClassCastException
			throw new MessageFormatExeption("Failed to cast parameter '" + paramName 
					   + "' value '" + jsonRoot.get(paramName) + "' to Integer.\nOriginal message: " + jsonRoot);
		} catch (final java.lang.Exception some_other_exception) {
			throw new MessageFormatExeption(some_other_exception);
		}
	}
	
	private long getLongParamValue(final JSONObject jsonRoot, 
				 				   final String paramName) throws MessageFormatExeption {
		if (false == jsonRoot.containsKey(paramName)) {
			throw new MessageFormatExeption("Failed to find parameter " + paramName + "\nOriginal message: " + jsonRoot);
		}
		try {
			return (Long)jsonRoot.get(paramName);
		} catch (java.lang.ClassCastException castExc) {
			// TODO REfactor call to get() inside MessageFormatExeption
			// TODO Add info from  ClassCastException
			throw new MessageFormatExeption("Failed to cast parameter '" + paramName 
						   					+ "' value '" + jsonRoot.get(paramName) + "' to Long.\nOriginal message: " + jsonRoot);
		} catch (final java.lang.Exception some_other_exception) {
			throw new MessageFormatExeption(some_other_exception);
		}
	}
	
	private long getLongParamValue(final JSONObject jsonRoot, 
					   			   final String paramName,
					   			   long defaultValue) throws MessageFormatExeption {
		if (false == jsonRoot.containsKey(paramName)) {
			return defaultValue;
		}
		try {
			return (Long)jsonRoot.get(paramName);
		} catch (java.lang.ClassCastException castExc) {
			// TODO REfactor call to get() inside MessageFormatExeption
			// TODO Add info from  ClassCastException
			throw new MessageFormatExeption("Failed to cast parameter '" + paramName 
						   					+ "' value '" + jsonRoot.get(paramName) + "' to Long.\nOriginal message: " + jsonRoot);
		} catch (final java.lang.Exception some_other_exception) {
			throw new MessageFormatExeption(some_other_exception);
		}
	}
}
