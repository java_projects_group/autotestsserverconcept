//============================================================================
// Name        : DiscardsKeeper.java
// Created on  : June 26, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : DiscardsKeeper class implementation
//============================================================================

package consumer;

import java.util.ArrayList;
import java.util.List;

/** DiscardsKeeper class implementation: **/
public class DiscardsKeeper {
	// Messages array list:
	protected List<String> messages = new ArrayList<String>();
	// Thread.
	protected Thread monitorThread = null;	
	
	/** MonitorTask class : **/
	class MonitorTask implements Runnable {
		/** **/
		private final static int sleepTimeout = 1000;
		
		/** array class constructor. **/
		protected MonitorTask() {
			// TODO
			System.out.println("MessagesHandler()");
		}
		
		/** Thread entry point : **/
		public void run() {
        	while (true) {
        		try {
					Thread.currentThread();
					Thread.sleep(MonitorTask.sleepTimeout);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
        		System.out.println("Monitor thread. Messges : " + messages.size());
        	}
		}
	}
	
	/** DiscardsKeeper default constructor : **/
	public DiscardsKeeper() {
		// Start monitor:
		this.monitorThread = new Thread(new MonitorTask());
		this.monitorThread.start();
		
		// TODO: Read saved data
	}
	
	public void putMessage(final String message) {
		messages.add(message);
	}
	


	/////////////////// JUST 4 TEST ////////////////////
	public static void main(String[] args) throws InterruptedException {
		System.out.println("TEST");
		DiscardsKeeper keeper = new DiscardsKeeper();
		
    	while (true) {
    		try { Thread.currentThread();
			Thread.sleep(5000); } catch (InterruptedException e) { /** **/ }
    		keeper.putMessage("TEST");
    	}
	}
}
