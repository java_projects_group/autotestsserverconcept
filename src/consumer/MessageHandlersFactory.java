//============================================================================
// Name        : MessageHandlersFactory.java
// Created on  : July 08, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : MessageHandlersFactory class
//============================================================================

package consumer;

import java.util.HashMap;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import consumer.parsers.AVTestsMessageHandler;
import consumer.parsers.BuildbotTestRunHandler;
import consumer.parsers.InstallerTestsMessageHandler;
import consumer.parsers.UpdaterTestsMessageHandler;
import database.entry.TestType;

/** MessageHandlersFactory. **/
public class MessageHandlersFactory {
	/** Test type JSON parameter name : **/
	private final static String testsType = "type";
	/** Message handlers pool. **/
	private HashMap<TestType, IRequestParser> parserPool = new HashMap<TestType, IRequestParser>();
		
    // Fabric method logic implementation.
    /** @return IRequestParser 
      * @throws MessageFormatExeption **/
    public IRequestParser getHandler(final ConsumerRecord<String, String> kakfaRecord) throws MessageFormatExeption  {
    	TestType type = this.getTestType(kakfaRecord.value());
    	switch (type) {
    		case BuildBotTests:
    			if (false == parserPool.containsKey(type))
    				parserPool.put(type, new BuildbotTestRunHandler());
    			break;
    		case InstallerTests:
    			if (false == parserPool.containsKey(type))
    				parserPool.put(type, new InstallerTestsMessageHandler());
    			break;
    		case UpdaterTests:
    			if (false == parserPool.containsKey(type))
    				parserPool.put(type, new UpdaterTestsMessageHandler());
    			break;	
    		case InstallerAVTests:
    			if (false == parserPool.containsKey(type))
    				parserPool.put(type, new AVTestsMessageHandler());
    			break;
			default:
				// TODO: Add logging
				throw new MessageFormatExeption("Failed to find handler for '" + type + "' message type");
    	}
    	return parserPool.get(type);
    }
    
    /** Extract type type from message. **/
    protected TestType getTestType(final String message) {
    	int pos = message.indexOf(testsType);
    	if (pos > 0 && (pos = message.indexOf(":", pos)) > 0) {
    		int pos2 = message.indexOf(",", pos);
    		final String typeStr = message.substring(pos + 1,  pos2).strip().replace("\"", "");
    		return TestType.fromString(typeStr);
    	}
    	return TestType.Undefined;
    }
    
}
