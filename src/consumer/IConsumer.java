//============================================================================
// Name        : IConsumer.java
// Created on  : June 20, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Messages consumers interface.
//============================================================================

package consumer;

/** IConsumer interface declaration: **/
public interface IConsumer {
	/** @throws InterruptedException  **/
	public void start() throws InterruptedException;
	
	/**  @throws InterruptedException  **/
	public void stop() throws InterruptedException;
	
	/** @return the Name of the consumer.  **/
	public String getName();
	
	/** @return the Name of the consumer.  **/
	public String getDescription();		
}
