package file_server;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** FileCacheStorage class : **/
public class FileCacheStorage {
	/** **/
	private Map<String, CacheFile> cachedFiles = new HashMap<String, CacheFile>();
	/** Static FileCacheStorage class instance : **/
	private static volatile FileCacheStorage __instance = null;	

	/** FileCacheStorage class constructor : **/
	protected FileCacheStorage() {
		System.out.println(this.getClass().getName() + " created.");
	} 
	
	/** getInstance : **/  
	public static FileCacheStorage getInstance() {
		if (null == __instance) {
			synchronized (FileCacheStorage.class) {
				if (null == __instance) {
					__instance = new FileCacheStorage();
				};
			}
		} return __instance;
	}

	public void addFile(final CacheFile file) {
		if (true == file.hasBytesData()) {
			System.out.println("File '" + file.getAbsolutePath() + "' added to cache.");
			this.cachedFiles.put(file.getName(), file);
		}
	}
	
	public void addFile(final String name, final CacheFile file) {
		if (true == file.hasBytesData()) {
			System.out.println("File '" + file.getAbsolutePath() + "' added to cache with name '" + name + "'");
			this.cachedFiles.put(name, file);
		}
	}

	public boolean isFileCached(final String fileName) {
		return this.cachedFiles.containsKey(fileName);
	}

	public CacheFile getFile(final String fileName) {
		return this.cachedFiles.get(fileName);
	}
	
	public void InitTest() throws IOException {
		this.addFolder("webapps");
	}
	
	public void addFolder(final String folderPath) throws IOException {
		List<String> files = utilities.Utilities.GetFileList(folderPath, true);
		final int len = folderPath.length() + 1;
		for (final String filePath : files) {
			String fileName = filePath.substring(len);
			this.addFile(fileName.replace("\\", "/"), new CacheFile(filePath));
		}
	}
}