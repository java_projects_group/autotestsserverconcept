//============================================================================
// Name        : CacheFile.java
// Created on  : June 20, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Cache file class
//============================================================================

package file_server;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import org.apache.commons.io.FileUtils;

/** @class CacheFile : */
public class CacheFile extends File {
    /** Serial id. **/
    private static final long serialVersionUID = -5373458384984923091L;
    /** File bytes. **/
    private byte[] fileBytes;
    /** Valid flag. **/
    private boolean isBytesValid = false;

	/**
	 * CacheFile class constructor.
     * Creates a new <code>File</code> instance from a parent pathname string
     * and a child pathname string.
     *
     * <p> If <code>parent</code> is <code>null</code> then the new
     * <code>File</code> instance is created as if by invoking the
     * single-argument <code>File</code> constructor on the given
     * <code>child</code> pathname string.
     *
     * <p> Otherwise the <code>parent</code> pathname string is taken to denote
     * a directory, and the <code>child</code> pathname string is taken to
     * denote either a directory or a file.  If the <code>child</code> pathname
     * string is absolute then it is converted into a relative pathname in a
     * system-dependent way.  If <code>parent</code> is the empty string then
     * the new <code>File</code> instance is created by converting
     * <code>child</code> into an abstract pathname and resolving the result
     * against a system-dependent default directory.  Otherwise each pathname
     * string is converted into an abstract pathname and the child abstract
     * pathname is resolved against the parent.
     *
     * @param   parent  The parent pathname string
     * @param   child   The child pathname string
     * @throws  NullPointerException
     *          If <code>child</code> is <code>null</code>
     */
    public CacheFile(final File parent, final String child) {
		super(parent, child);
		this.readFileBytes();
    }

	/**
	 * CacheFile class constructor.
     * Creates a new <code>CacheFile</code> instance by converting the given
     * pathname string into an abstract pathname.  If the given string is
     * the empty string, then the result is the empty abstract pathname.
     *
     * @param   pathname  A pathname string
     * @throws  NullPointerException
     *          If the <code>pathname</code> argument is <code>null</code>
     */
    public CacheFile(final String pathname) {
		super(pathname);
		this.readFileBytes();
    }

	/**
	 * CacheFile class constructor.
     * Creates a new {@code File} instance by converting the given
     *
     * <p> The exact form of a {@code file:} URI is system-dependent, hence
     * the transformation performed by this constructor is also
     * system-dependent.
     *
     * <p> For a given abstract pathname <i>f</i> it is guaranteed that
     *
     * so long as the original abstract pathname, the URI, and the new abstract
     * pathname are all created in (possibly different invocations of) the same
     * Java virtual machine.  This relationship typically does not hold,
     * however, when a {@code file:} URI that is created in a virtual machine
     * on one operating system is converted into an abstract pathname in a
     * virtual machine on a different operating system.
     *
     * @param  uri
     *         An absolute, hierarchical URI with a scheme equal to
     *         {@code "file"}, a non-empty path component, and undefined
     *         authority, query, and fragment components
     *
     * @throws  NullPointerException
     *          If {@code uri} is {@code null}
     *
     * @throws  IllegalArgumentException
     *          If the preconditions on the parameter do not hold
     *
     * @see #toURI()
     * @see java.net.URI
     * @since 1.4
     */
    public CacheFile(final URI uri) {
		super(uri);
		this.readFileBytes();
    }

    protected void readFileBytes() {
		try {
			this.fileBytes = FileUtils.readFileToByteArray(this);
			isBytesValid = true;
		} catch (IOException exception) {
			exception.printStackTrace();
			isBytesValid = false;
		}
    }

    public byte[] getFileBytes() {
		return this.fileBytes;
    }

    public boolean hasBytesData() {
		return this.isBytesValid;
    }

    public long bytesBufferLength() {
		return this.fileBytes.length;
    }
}