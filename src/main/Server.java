//============================================================================
// Name        : Server.java
// Created on  : December 12, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Server class
//============================================================================

package main;

import java.io.IOException;
import consumer.KafkaMessagesConsumer;
import database.BrowserBuilds;
import database.tables.AutotestsTable;
import database.tables.BuildsTable;
import database.tables.UnitTestsTable;
import mail.SMTPService;

/** @author a.tokmakov **/
/** @class  Server. **/
public class Server {
	public static void main(String[] args)  throws InterruptedException, IOException {
		// Initialize asynchronous logger context.
		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		
		System.in.close();
        System.out.close();
		
        @SuppressWarnings("unused")
		BuildsTable builds_table = BuildsTable.getTable();
        @SuppressWarnings("unused")
		UnitTestsTable unit_tests_table = UnitTestsTable.getTable();
        @SuppressWarnings("unused")
		AutotestsTable auto_tests_table = AutotestsTable.getTable();
        @SuppressWarnings("unused")
        BrowserBuilds builds = BrowserBuilds.getTable();
        if (false == builds.Initialize())
        	return;
        
        /** Create and register SMTP service. */
        SMTPService.getService().register();
        
		KafkaMessagesConsumer consumer = new KafkaMessagesConsumer();
		consumer.start();
	}
}
