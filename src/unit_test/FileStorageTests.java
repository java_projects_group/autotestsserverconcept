package unit_test;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import file_server.FileCacheStorage;

public class FileStorageTests extends Assert {
	/** **/
	protected static FileCacheStorage fileStorage = FileCacheStorage.getInstance();

	@BeforeClass
	private void setUp() throws IOException {
		System.out.println("Before class");
		fileStorage.InitTest();
	}	
	
	@AfterTest
	void tearDown() {
		System.out.println("@AfterTest");
	}	
	
	@Test
    public void Test_BuildBotTestRunTable_HandleNegativeValue() throws Exception {
        assertEquals(true, fileStorage.isFileCached("css/bootstrap.min.css"));
    }	
}
