
package unit_test.db_entry;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.testng.Assert;
import org.testng.annotations.Test;
import database.entry.AutotestsRecord;
import database.entry.TestStatus;
import database.entry.TestType;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                             AutotestsRecord_Tests                                                            //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class AutotestsRecord_Tests extends Assert {
	
	final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");
	
	
	@Test
	public void SetGet_StartTime_Test() throws Exception {
		final LocalDateTime startTimestamp = LocalDateTime.now();
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setStartTime(startTimestamp);
		
		Assert.assertTrue(isDatesEqual(startTimestamp, rec.getStartTime()));
	}
	
	@Test
	public void SetGet_EndTime_Test() throws Exception {
		final LocalDateTime endTimestamp = LocalDateTime.now();
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setEndTime(endTimestamp);
		
		Assert.assertTrue(isDatesEqual(endTimestamp, rec.getEndTime()));
	}
	
	@Test
	public void SetGet_WorkerName_Test() throws Exception {
		final String workerName = "SOME_TEST_WORKER_NAME";
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setWorkerName(workerName);
		
		assertEquals(workerName, rec.getWorkerName());
	}
	
	@Test
	public void SetGet_ResultJson_Test() throws Exception {
		final String resultJson = "{\"param\":\"value\"}";
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setResultJson(resultJson);
		
		assertEquals(resultJson, rec.getResultJson());
	}
	
	@Test
	public void SetGet_OS_Test() throws Exception {
		final String os = "Windows-10-10.0.17763-SP0";
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setOs(os);
		
		assertEquals(os, rec.getOs());
	}
	
	@Test
	public void SetGet_TestsStatus_Test() throws Exception {
		final TestStatus status = TestStatus.TestsStarted;
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setTestStatus(status);
		
		assertEquals(status, rec.getTestStatus());
	}
	
	@Test
	public void SetGet_TestsType_Test() throws Exception {
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setTestsType(type);
		
		assertEquals(type, rec.getTestsType());
	}
	
	@Test
	public void Equals_Same_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String resultJson = "{\"param\":\"value\"}";
		final String os = "Windows-10-10.0.17763-SP0";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setWorkerName(workerName);
		rec2.setOs(os);
		rec2.setTestStatus(status);
		rec2.setTestsType(type);
		
		Assert.assertTrue(rec1.equals(rec2), "AutotestsRecord objects expected te be equals.");
	}
	
	@Test
	public void Equals_Different_UUID_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String resultJson = "{\"param\":\"value\"}";
		final String os = "Windows-10-10.0.17763-SP0";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid("");
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(status);
		rec2.setTestsType(type);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_Different_ID_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String os = "Windows-10-10.0.17763-SP0";
		final String resultJson = "{\"param\":\"value\"}";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(123);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(status);
		rec2.setTestsType(type);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_Different_StartTime_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String os = "Windows-10-10.0.17763-SP0";
		final String resultJson = "{\"param\":\"value\"}";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(LocalDateTime.parse("9/9/2019 11:22:11", formatter));
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(status);
		rec2.setTestsType(type);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}	
	
	@Test
	public void Equals_Different_EndTime_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String os = "Windows-10-10.0.17763-SP0";
		final String resultJson = "{\"param\":\"value\"}";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(LocalDateTime.parse("9/9/2019 11:22:11", formatter));
		rec2.setResultJson(resultJson);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(status);
		rec2.setTestsType(type);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_Different_ResultJson_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String os = "Windows-10-10.0.17763-SP0";
		final String resultJson = "{\"param\":\"value\"}";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson("{\"param\":\"value1\"}");
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(status);
		rec2.setTestsType(type);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_Different_WorkerName_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String os = "Windows-10-10.0.17763-SP0";
		final String resultJson = "{\"param\":\"value\"}";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setOs(os);
		rec2.setWorkerName("SOME_TEST_WORKER_NEW");
		rec2.setTestStatus(status);
		rec2.setTestsType(type);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}	
	
	@Test
	public void Equals_Different_Status_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String resultJson = "{\"param\":\"value\"}";
		final String os = "Windows-10-10.0.17763-SP0";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(TestStatus.TestsCompleted);
		rec2.setTestsType(type);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_Different_Type_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String resultJson = "{\"param\":\"value\"}";
		final String os = "Windows-10-10.0.17763-SP0";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(status);
		rec2.setTestsType(TestType.BuildBotTests);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}	
	
	
	@Test
	public void Equals_Different_OS_Test() throws Exception {
		
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		final long id = 12345;
		final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
		final String workerName = "SOME_TEST_WORKER_NAME";
		final String resultJson = "{\"param\":\"value\"}";
		final String os = "Windows-10-10.0.17763-SP0";
		final TestStatus status = TestStatus.TestsStarted;
		final TestType type = TestType.InstallerAVTests;
		
		AutotestsRecord rec1 = new AutotestsRecord();
		AutotestsRecord rec2 = new AutotestsRecord();

		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		rec1.setResultJson(resultJson);
		rec1.setOs(os);
		rec1.setWorkerName(workerName);
		rec1.setTestStatus(status);
		rec1.setTestsType(type);
		
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		rec2.setResultJson(resultJson);
		rec2.setOs(os + "1212");
		rec2.setWorkerName(workerName);
		rec2.setTestStatus(status);
		rec2.setTestsType(TestType.BuildBotTests);
		
		Assert.assertFalse(rec1.equals(rec2), "AutotestsRecord objects expected te be different.");
	}	
	
	//////////////////////////////////////////////////////
	
	private boolean isDatesEqual(final LocalDateTime date1, final LocalDateTime date2) {
		return (date1.getDayOfMonth() == date2.getDayOfMonth()) &&
				(date1.getMonthValue() == date2.getMonthValue()) &&
				(date1.getYear() == date2.getYear()) &&
				(date1.getHour() == date2.getHour()) &&
				(date1.getMinute() == date2.getMinute()) &&
				(date1.getSecond() == date2.getSecond());		
	}
}
