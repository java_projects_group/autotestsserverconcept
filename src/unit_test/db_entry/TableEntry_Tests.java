package unit_test.db_entry;

import org.testng.Assert;
import org.testng.annotations.Test;
import database.entry.TableEntry;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                             TableEntry_Tests                                                            //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class TableEntry_Tests extends Assert {
	@Test
	public void SetGet_ID_Test() throws Exception {
		long id = 12345;
		
		TableEntry entry = new TableEntryClass();
		entry.setId(id);
		
		assertEquals(id, entry.getId());
	}
	
	@Test
	public void SetGet_UUID_Test() throws Exception {
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		
		TableEntry entry = new TableEntryClass();
		entry.setUuid(uuid);
		
		assertEquals(uuid, entry.getUuid());
	}
	
	@Test
	public void Equals_Same_Test() throws Exception {
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		long id = 12345;
		
		TableEntry entry1 = new TableEntryClass();
		TableEntry entry2 = new TableEntryClass();
		
		entry1.setId(id);
		entry2.setId(id);
		
		entry1.setUuid(uuid);
		entry2.setUuid(uuid);
		
		Assert.assertTrue(entry1.equals(entry2), "TableEntry object expected te be equals.");
	}
	
	/*
	 
	 Removed since there is no check for ID in TableEntry.equals()
	 
	@Test
	public void Equals_Diffetent_ID_Test() throws Exception {
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		long id = 12345;
		
		TableEntry entry1 = new TableEntryClass();
		TableEntry entry2 = new TableEntryClass();
		
		entry1.setId(id);
		entry2.setId(id + 1);
		
		entry1.setUuid(uuid);
		entry2.setUuid(uuid);
		
		Assert.assertFalse(entry1.equals(entry2), "TableEntry object expected to be different.");
	}*/
	
	@Test
	public void Equals_Diffetent_UUid_Test() throws Exception {
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		long id = 12345;
		
		TableEntry entry1 = new TableEntryClass();
		TableEntry entry2 = new TableEntryClass();
		
		entry1.setId(id);
		entry2.setId(id);
		
		entry1.setUuid(uuid);
		entry2.setUuid(uuid + "TEST");
		
		Assert.assertFalse(entry1.equals(entry2), "TableEntry object expected te be different.");
	}
	
	@Test
	public void Equals_Diffetent_ID_And_UUid_Test() throws Exception {
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		long id = 12345;
		
		TableEntry entry1 = new TableEntryClass();
		TableEntry entry2 = new TableEntryClass();
		
		entry1.setId(id);
		entry2.setId(id + 123);
		
		entry1.setUuid(uuid);
		entry2.setUuid(uuid + "TEST");
		
		Assert.assertFalse(entry1.equals(entry2), "TableEntry object expected te be different.");
	}	
}

/** Class for TableEntry testing. **/
class TableEntryClass extends TableEntry {
	
}