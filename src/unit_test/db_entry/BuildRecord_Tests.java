package unit_test.db_entry;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.Test;
import database.entry.BuildRecord;
import database.entry.BuildStatus;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                             DBEntry_TestRun_Tests                                                            //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class BuildRecord_Tests extends Assert {
	@Test
	public void SetGet_ID_Test() throws Exception {
		long id = 12345;
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setId(id);
		
		assertEquals(id, buildRecord.getId());
	}
	
	@Test
	public void SetGet_UUID_Test() throws Exception {
		final String uuid = "213sdfesf34r77gfb93wre782347y443";
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setUuid(uuid);
		
		assertEquals(uuid, buildRecord.getUuid());
	}
	
	@Test
	public void SetGet_StartTime_Test() throws Exception {
		final LocalDateTime startTimestamp = LocalDateTime.now();
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setStartTime(startTimestamp);
		
		Assert.assertTrue(isDatesEqual(startTimestamp, buildRecord.getStartTime()));
	}
	
	@Test
	public void SetGet_EndTime_Test() throws Exception {
		final LocalDateTime endTimestamp = LocalDateTime.now();
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setEndTime(endTimestamp);
		
		Assert.assertTrue(isDatesEqual(endTimestamp, buildRecord.getEndTime()));
	}
	
	@Test
	public void SetGet_IsNightly_Test() throws Exception {
		boolean is_nightly = false;
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setNightly(is_nightly);
		
		assertEquals(is_nightly, buildRecord.isNightly());
	}
	
	@Test
	public void SetGet_Status_Test() throws Exception {
		BuildStatus status = BuildStatus.TestsCompleted;
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setStatus(status);
		
		assertEquals(status, buildRecord.getStatus());
	}
	
	@Test
	public void SetGet_Buildnumber_Test() throws Exception {
		int buildnumber = 54321;
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setBuildnumber(buildnumber);
		
		assertEquals(buildnumber, buildRecord.getBuildnumber());
	}

	@Test
	public void SetGet_BrowserVersion_Test() throws Exception {
		final String browser_version = "5.12.34.45";
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setBrowserVersion(browser_version);
		
		assertEquals(browser_version, buildRecord.getBrowserVersion());
	}
	
	@Test
	public void SetGet_Branch_Test() throws Exception {
		final String branch = "atom_5.5";
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setBranch(branch);
		
		assertEquals(branch, buildRecord.getBranch());
	}
	
	@Test
	public void SetGet_Repository_Test() throws Exception {
		final String repository = "2321342342r2fr2323f23e34";
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setRepository(repository);
		
		assertEquals(repository, buildRecord.getRepository());
	}
	
	@Test
	public void SetGet_Revision_Test() throws Exception {
		final String revision = "dsdasdsdsdsdsddddddddddddd3233333333333333";
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setRevision(revision);
		
		assertEquals(revision, buildRecord.getRevision());
	}
	
	@Test
	public void SetGet_Owners_Test() throws Exception {
		List<String> owners = new ArrayList<String>(Arrays.asList("Value1", "Value2", "Value3", "Value4", "Value5", "Value6", "Value7"));
		
		BuildRecord buildRecord = new BuildRecord();
		buildRecord.setOwners(owners);

		Assert.assertTrue(owners.containsAll(buildRecord.getOwners()));
		Assert.assertTrue(buildRecord.getOwners().containsAll(owners));
	}
	
	@Test
	public void AddGet_Owners_Test() throws Exception {
		List<String> owners = new ArrayList<String>(Arrays.asList("Value1", "Value2", "Value3", "Value4", "Value5", "Value6", "Value7"));
		
		BuildRecord buildRecord = new BuildRecord();
		for (final String owner : owners)
			buildRecord.addOwner(owner);

		Assert.assertTrue(owners.containsAll(buildRecord.getOwners()));
		Assert.assertTrue(buildRecord.getOwners().containsAll(owners));
	}
	
	private boolean isDatesEqual(final LocalDateTime date1, final LocalDateTime date2) {
		return (date1.getDayOfMonth() == date2.getDayOfMonth()) &&
				(date1.getMonthValue() == date2.getMonthValue()) &&
				(date1.getYear() == date2.getYear()) &&
				(date1.getHour() == date2.getHour()) &&
				(date1.getMinute() == date2.getMinute()) &&
				(date1.getSecond() == date2.getSecond());		
	}
}
