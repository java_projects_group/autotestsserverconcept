package unit_test.db_entry;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.testng.Assert;
import org.testng.annotations.Test;
import database.entry.UnitTestsRecord;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                             UnitTestsRecord_Tests                                                            //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public class UnitTestsRecord_Tests extends Assert {
	
	final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");
	
	final String uuid = "213sdfesf34r77gfb93wre782347y443";
	long id = 12345;
	final String resultJson = "{\"param\":\"value\"}";
	final String workerName = "SOME_TEST_WORKER_NAME";
	final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
	final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
	
	@Test
	public void SetGet_StartTime_Test() throws Exception {
		UnitTestsRecord recUnitTest = new UnitTestsRecord();
		recUnitTest.setStartTime(startTimestamp);
		
		Assert.assertTrue(isDatesEqual(startTimestamp, recUnitTest.getStartTime()));
	}
	
	@Test
	public void SetGet_EndTime_Test() throws Exception {
		UnitTestsRecord recUnitTest = new UnitTestsRecord();
		recUnitTest.setEndTime(endTimestamp);
		
		Assert.assertTrue(isDatesEqual(endTimestamp, recUnitTest.getEndTime()));
	}
	
	@Test
	public void SetGet_WorkerName_Test() throws Exception {
		UnitTestsRecord recUnitTest = new UnitTestsRecord();
		recUnitTest.setWorkerName(workerName);
		
		assertEquals(workerName, recUnitTest.getWorkerName());
	}
	
	@Test
	public void SetGet_ResultJson_Test() throws Exception {
		UnitTestsRecord recUnitTest = new UnitTestsRecord();
		recUnitTest.setResultJson(resultJson);
		
		assertEquals(resultJson, recUnitTest.getResultJson());
	}
	
	@Test
	public void Equals_Test() throws Exception {
		UnitTestsRecord rec1 = new UnitTestsRecord();
		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setWorkerName(workerName);
		rec1.setResultJson(resultJson);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setResultJson(resultJson);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		Assert.assertTrue(rec1.equals(rec2), "UnitTestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_DiffentID_Test() throws Exception {
		UnitTestsRecord rec1 = new UnitTestsRecord();
		rec1.setId(id + 1);
		rec1.setUuid(uuid);
		rec1.setWorkerName(workerName);
		rec1.setResultJson(resultJson);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setResultJson(resultJson);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		Assert.assertFalse(rec1.equals(rec2), "UnitTestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_DiffentUUid_Test() throws Exception {
		UnitTestsRecord rec1 = new UnitTestsRecord();
		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setWorkerName(workerName);
		rec1.setResultJson(resultJson);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid + "2323");
		rec2.setWorkerName(workerName);
		rec2.setResultJson(resultJson);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		Assert.assertFalse(rec1.equals(rec2), "UnitTestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_DiffentWorkerName_Test() throws Exception {
		UnitTestsRecord rec1 = new UnitTestsRecord();
		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setWorkerName(workerName);
		rec1.setResultJson(resultJson);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName + "2323");
		rec2.setResultJson(resultJson);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		Assert.assertFalse(rec1.equals(rec2), "UnitTestsRecord objects expected te be different.");
	}	
	
	@Test
	public void Equals_Diffent_Resultjson_Test() throws Exception {
		UnitTestsRecord rec1 = new UnitTestsRecord();
		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setWorkerName(workerName);
		rec1.setResultJson(resultJson);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setResultJson(resultJson + "sds");
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		// We should not perform check for ResultJSON value
		Assert.assertTrue(rec1.equals(rec2), "UnitTestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_Diffent_StartTime_Test() throws Exception {
		UnitTestsRecord rec1 = new UnitTestsRecord();
		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setWorkerName(workerName);
		rec1.setResultJson(resultJson);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setResultJson(resultJson);
		rec2.setStartTime(LocalDateTime.parse("9/9/2019 11:22:31", formatter));
		rec2.setEndTime(endTimestamp);
		
		Assert.assertFalse(rec1.equals(rec2), "UnitTestsRecord objects expected te be different.");
	}
	
	@Test
	public void Equals_Diffent_EndTime_Test() throws Exception {
		UnitTestsRecord rec1 = new UnitTestsRecord();
		rec1.setId(id);
		rec1.setUuid(uuid);
		rec1.setWorkerName(workerName);
		rec1.setResultJson(resultJson);
		rec1.setStartTime(startTimestamp);
		rec1.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setResultJson(resultJson);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(LocalDateTime.parse("9/9/2019 11:22:31", formatter));
		
		Assert.assertFalse(rec1.equals(rec2), "UnitTestsRecord objects expected te be different.");
	}
	
	//////////////////////////////////////////////////////
	
	private boolean isDatesEqual(final LocalDateTime date1, final LocalDateTime date2) {
		return (date1.getDayOfMonth() == date2.getDayOfMonth()) &&
				(date1.getMonthValue() == date2.getMonthValue()) &&
				(date1.getYear() == date2.getYear()) &&
				(date1.getHour() == date2.getHour()) &&
				(date1.getMinute() == date2.getMinute()) &&
				(date1.getSecond() == date2.getSecond());		
	}
}
