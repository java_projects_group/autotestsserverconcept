package unit_test.tables;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.Test;

import database.entry.AutotestsRecord;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.entry.TestStatus;
import database.entry.TestType;
import database.entry.UnitTestsRecord;
import database.tables.TableCache;

public class TableCache_Tests extends Assert {
	
	final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");

	protected final String uuid = "213sdfesf34r77gfb93wre782347y443";
	protected final long id = 12345;
	protected final LocalDateTime startTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
	protected final LocalDateTime endTimestamp = LocalDateTime.parse("9/9/2019 11:22:33", formatter);
	protected final String workerName = "SOME_TEST_WORKER_NAME";
	protected final String resultJson = "{\"param\":\"value\"}";
	protected final String os = "Windows-10-10.0.17763-SP0";
	protected final TestStatus status = TestStatus.TestsStarted;
	protected final TestType type = TestType.InstallerAVTests;
	
	protected void FillCache(TableCache<BuildRecord> cache, int count)  {
		for (int i = 0; i < count; i++) {
			BuildRecord rec = new BuildRecord();
			rec.setUuid("23242342341421342134234242342__" + i);
			rec.setStatus(BuildStatus.TestsCompleted);
			rec.setBuildnumber(i);
			rec.setNightly(true);
			rec.setBrowserVersion("6.0.0." + i);
			rec.setBranch("atom_2.34." + i);
			rec.setRepository(" git@gitlab.corp.mail.ru:browser" + i + "/browser.git");
			rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6" + i);
			rec.addOwner("Owner" + i);
			
			boolean result = cache.add(rec);
			Assert.assertTrue(true == result);
		}
	}

	@Test
	public void Add_OK_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		
		BuildRecord rec = new BuildRecord();
		rec.setUuid(uuid);
		rec.setStatus(BuildStatus.TestsCompleted);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		
		Assert.assertTrue(cache.isEmpty());
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result);
		Assert.assertTrue(false == cache.isEmpty());
		Assert.assertTrue(1 == cache.Size());
	}
	
	@Test
	public void Add_DuplicateUUID_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		
		Assert.assertTrue(cache.isEmpty());
		
		BuildRecord rec = new BuildRecord();
		rec.setUuid("23242342341421342134234242342");
		
		BuildRecord rec2 = new BuildRecord();
		rec2.setUuid("23242342341421342134234242342");
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result);
		Assert.assertTrue(1 == cache.Size());
		
		result = cache.add(rec2);
		
		Assert.assertTrue(false == result);
		Assert.assertTrue(1 == cache.Size());
	}
	
	@Test
	public void Add_1000_Records_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty());
		
		final int size = 1000;
		for (int i = 0; i < size; i++) {
			BuildRecord rec = new BuildRecord();
			rec.setUuid("23242342341421342134234242342" + i);
			boolean result = cache.add(rec);
			Assert.assertTrue(true == result);
		}

		Assert.assertTrue(false == cache.isEmpty());
		Assert.assertTrue(size == cache.Size());
	}
	
	@Test
	public void GetByUUID_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty());
		
		final String uuid = "23242342341421342134234242342";
		BuildRecord rec = new BuildRecord();
		rec.setUuid(uuid);
		rec.setStatus(BuildStatus.TestsCompleted);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		
		cache.add(rec);
		
		BuildRecord rec_new = cache.get(uuid);
		Assert.assertTrue(null != rec_new);
		
		Assert.assertTrue(rec_new.getUuid().equals(rec.getUuid()));
		Assert.assertTrue(rec_new.getStatus() == rec.getStatus());
		Assert.assertTrue(rec_new.getBuildnumber() == rec.getBuildnumber());
		Assert.assertTrue(rec_new.isNightly() == rec.isNightly());
		Assert.assertTrue(rec_new.getBrowserVersion().equals(rec.getBrowserVersion()));
		Assert.assertTrue(rec_new.getBranch().equals(rec.getBranch()));
		Assert.assertTrue(rec_new.getRepository().equals(rec.getRepository()));
		Assert.assertTrue(rec_new.getRevision().equals(rec.getRevision()));
	}
		
	
	@Test
	public void GetByUUID_NotFound_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty());
		
		final String uuid = "23242342341421342134234242342";
		BuildRecord rec = new BuildRecord();
		rec.setUuid(uuid);
		rec.setStatus(BuildStatus.TestsCompleted);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		
		cache.add(rec);
		
		BuildRecord rec_new = cache.get(uuid + "122");
		
		Assert.assertTrue(null == rec_new, "Check that NULL is returned");
	}
	
	@Test
	public void GetByIndex_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty(), "Cache should be empty to begin with.");
		
		FillCache(cache, 10);
		
		int index = 5;
		BuildRecord rec_new = cache.get(index);
		Assert.assertTrue(null != rec_new, "Check that have some returned data.");
		
		BuildRecord rec_expected = new BuildRecord();
		rec_expected.setUuid("23242342341421342134234242342__" + index);
		rec_expected.setStatus(BuildStatus.TestsCompleted);
		rec_expected.setBuildnumber(index);
		rec_expected.setNightly(true);
		rec_expected.setBrowserVersion("6.0.0." + index );
		rec_expected.setBranch("atom_2.34." + index );
		rec_expected.setRepository(" git@gitlab.corp.mail.ru:browser" + index + "/browser.git");
		rec_expected.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6" + index);
		rec_expected.addOwner("Owner" + index);
		
		assertEquals(rec_new.getUuid(), rec_expected.getUuid(), "Check record UUID.");
		assertEquals(rec_new.getStatus(), rec_expected.getStatus(), "Check record Status.");
		assertEquals(rec_new.getBuildnumber(), rec_expected.getBuildnumber(), "Check record BuildNumber.");
		assertEquals(rec_new.isNightly(), rec_expected.isNightly(), "Check record isNightly value.");
		assertEquals(rec_new.getBrowserVersion(), rec_expected.getBrowserVersion(), "Check record BrowserVersion.");
		assertEquals(rec_new.getBranch(), rec_expected.getBranch(), "Check record Branch.");
		assertEquals(rec_new.getRepository(), rec_expected.getRepository(), "Check record Repository.");
		assertEquals(rec_new.getRevision(), rec_expected.getRevision(), "Check record Revision.");
	}
	
	@Test
	public void GetByIndex_NotFound_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty(), "Cache should be empty to begin with.");
		
		FillCache(cache, 10);
		Assert.assertTrue(10 == cache.Size(), "Check if cache contaions 10 elements");
		Assert.assertTrue(false == cache.isEmpty(), "Cache shall not be empty now");
		
		int index = 10;
		BuildRecord rec_new = cache.get(index);
		Assert.assertTrue(null == rec_new, "Check that NULL is returned");
	}	
	
	@Test
	public void Get_WrongIndex_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty());
		
		final int size = 10;
		for (int i = 0; i < size; i++) {
			BuildRecord rec = new BuildRecord();
			rec.setUuid("23242342341421342134234242342" + i);
			boolean result = cache.add(rec);
			Assert.assertTrue(true == result);
		}

		BuildRecord rec = cache.get(size);
		Assert.assertTrue(null == rec);
		
		BuildRecord rec1 = cache.get(size + 1);
		Assert.assertTrue(null == rec1);
		
		BuildRecord rec2 = cache.get(-1);
		Assert.assertTrue(null == rec2);
	}
	
	@Test
	public void Update_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty());
		FillCache(cache, 10);
		
		final String uuid = "23242342341421342134234242342__5";
		BuildRecord rec = new BuildRecord();
		rec.setUuid(uuid);
		rec.setStatus(BuildStatus.InstallersAssembled);
		rec.setBuildnumber(1234);
		rec.setNightly(false);
		rec.setBrowserVersion("1.2.3.4");
		rec.setBranch("atom_1.2.3.4.5");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser12345/browser.git1");
		rec.setRevision("1234512345123451234512345123451234512345");
		rec.setOwners(Arrays.asList("Value1", "Value2", "Value3", "Value4", "Value5", "Value6", "Value7"));

		
		boolean result = cache.update(rec);
		Assert.assertTrue(true == result);
		Assert.assertEquals(10, cache.Size());
		
		BuildRecord rec_new = cache.get(uuid);
		Assert.assertTrue(null != rec_new);
		
		Assert.assertTrue(rec_new.getUuid().equals(rec.getUuid()));
		Assert.assertTrue(rec_new.getStatus() == rec.getStatus());
		Assert.assertTrue(rec_new.getBuildnumber() == rec.getBuildnumber());
		Assert.assertTrue(rec_new.isNightly() == rec.isNightly());
		Assert.assertTrue(rec_new.getBrowserVersion().equals(rec.getBrowserVersion()));
		Assert.assertTrue(rec_new.getBranch().equals(rec.getBranch()));
		Assert.assertTrue(rec_new.getRepository().equals(rec.getRepository()));
		Assert.assertTrue(rec_new.getRevision().equals(rec.getRevision()));
	}
	
	@Test
	public void Update_NotExistingRecord_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty());
		FillCache(cache, 10);
		
		BuildRecord rec = new BuildRecord();
		rec.setUuid("23242342341421342134234242342__55");

		boolean result = cache.update(rec);
		Assert.assertTrue(false == result);
	}
	
	@Test
	public void ClearChache_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty());
		
		FillCache(cache, 10);
		assertEquals(10, cache.Size(), "Check that cache size is 10");
		
		cache.clear();

		assertEquals(0, cache.Size(), "Check that cache size is 0");
		assertEquals(true, cache.isEmpty(), "Check that cache is empty");
	}
	
	@Test
	public void IsRecordExists_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		final String uuid = "23242342341421342134234242342";
		BuildRecord rec = new BuildRecord();
		rec.setUuid(uuid);
		rec.setStatus(BuildStatus.TestsCompleted);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		boolean exists = cache.isEntryExists(rec);
		Assert.assertTrue(true == exists, "Check that record with uuid = '" + uuid + "' exists.");
	}
	
	@Test
	public void IsRecordExists_ByID_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		final String uuid = "23242342341421342134234242342";
		BuildRecord rec = new BuildRecord();
		rec.setUuid(uuid);
		rec.setStatus(BuildStatus.TestsCompleted);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.isEntryExists_ByID(rec);
		Assert.assertTrue(true == result, "Check that record exists.");
	}	
	
	@Test
	public void IsRecordExists_ByUUID_Test() throws Exception {
		TableCache<BuildRecord> cache = new TableCache<BuildRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		final String uuid = "23242342341421342134234242342";
		BuildRecord rec = new BuildRecord();
		rec.setUuid(uuid);
		rec.setStatus(BuildStatus.TestsCompleted);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.isEntryExists_ByUUID(rec);
		Assert.assertTrue(true == result, "Check that record exists.");
	}	
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                UnitTestsRecord caching tests                                               //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Test
	public void Add_UnitTestsRecord_Test() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setResultJson(resultJson);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}	
	
	@Test
	public void Add_UnitTestsRecord_DuplicateRecord() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(false == result, "ADD should fail");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
	}	
	
	@Test
	public void Add_UnitTestsRecord_DifferentRecords_1() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(startTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id + 1);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(startTimestamp);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
	}	
	
	@Test
	public void Add_UnitTestsRecord_DifferentRecords_2() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid + "2");
		rec2.setWorkerName(workerName);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
	}		
	
	@Test
	public void Add_UnitTestsRecord_DifferentRecords_3() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName+ "2");
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
	}
	
	@Test
	public void Add_UnitTestsRecord_DifferentRecords_4() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setStartTime(LocalDateTime.parse("9/9/2019 11:11:12", formatter));
		rec2.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
	}
	
	@Test
	public void Add_UnitTestsRecord_DifferentRecords_5() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		UnitTestsRecord rec2 = new UnitTestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setWorkerName(workerName);
		rec2.setStartTime(startTimestamp);
		rec2.setEndTime(LocalDateTime.parse("9/9/2019 12:22:22", formatter));
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
	}
	
	@Test
	public void Get_UnitTestsRecord_Test() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		Assert.assertTrue(true == result, "Check ADD operation result.");
		
		UnitTestsRecord rec2 = cache.get(rec);
		Assert.assertFalse(null == rec2, "rec2 != NULL");
		
		assertEquals(rec.getId(), rec2.getId());
		assertEquals(rec.getUuid(), rec2.getUuid());
		assertEquals(rec.getResultJson(), rec2.getResultJson());
		assertEquals(rec.getWorkerName(), rec2.getWorkerName());
		assertEquals(rec.getStartTime(), rec2.getStartTime());
		assertEquals(rec.getEndTime(), rec2.getEndTime());	
	}
	
	@Test
	public void UnitTestsRecord_IsRecordExits_Test() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		Assert.assertTrue(true == result, "Check ADD operation result.");

		result = cache.isEntryExists(rec);
		Assert.assertTrue(true == result, "Record should exists.");
	}
	
	@Test
	public void UnitTestsRecord_IsRecordExits_ByID_Test() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		Assert.assertTrue(true == result, "Check ADD operation result.");

		result = cache.isEntryExists_ByID(rec);
		Assert.assertTrue(true == result, "Record should exists.");
	}
	
	@Test
	public void UnitTestsRecord_IsRecordExits_ByUUID_Test() throws Exception {
		TableCache<UnitTestsRecord> cache = new TableCache<UnitTestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		UnitTestsRecord rec = new UnitTestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setWorkerName(workerName);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		
		boolean result = cache.add(rec);
		Assert.assertTrue(true == result, "Check ADD operation result.");

		result = cache.isEntryExists_ByUUID(rec);
		Assert.assertTrue(true == result, "Record should exists.");
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//                                AutotestsRecord caching tests                                               //
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	@Test
	public void Add_AutotestsRecord_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setEndTime(endTimestamp);
		rec.setResultJson(resultJson);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestStatus(status);
		rec.setTestsType(type);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Same_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(false == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_DifferentId_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id+1);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_UUId_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid + "323");
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_OS_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os + "Win");
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_StartTime_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(LocalDateTime.parse("9/9/2019 22:22:33", formatter));
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_WorkerName_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName + "33");
		rec2.setTestsType(type);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_TestsType_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(TestType.UpdaterAVTests);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(2 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_EndTime_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		rec.setEndTime(endTimestamp);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		rec2.setEndTime(LocalDateTime.parse("9/9/2019 11:11:11", formatter));
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(false == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_ResultJson_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		rec.setResultJson(resultJson);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		rec2.setResultJson(resultJson + "334");
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(false == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void Add_AutotestsRecord_Different_TestStatus_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		rec.setTestStatus(TestStatus.TestsStarted);
		
		AutotestsRecord rec2 = new AutotestsRecord();
		rec2.setId(id);
		rec2.setUuid(uuid);
		rec2.setStartTime(startTimestamp);
		rec2.setOs(os);
		rec2.setWorkerName(workerName);
		rec2.setTestsType(type);
		rec2.setTestStatus(TestStatus.TestsCompleted);
		
		boolean result = cache.add(rec);
		
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.add(rec2);
		
		Assert.assertTrue(false == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
	}
	
	@Test
	public void AutotestsRecord_IsRecordExiss_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		rec.setTestStatus(TestStatus.TestsStarted);
		
		boolean result = cache.add(rec);
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.isEntryExists(rec);
		Assert.assertTrue(true == result, "Record should exists.");
	}
	
	@Test
	public void AutotestsRecord_isEntryExists_ByID_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		rec.setTestStatus(TestStatus.TestsStarted);
		
		boolean result = cache.add(rec);
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.isEntryExists_ByID(rec);
		Assert.assertTrue(true == result, "Record should exists.");
	}
	
	@Test
	public void AutotestsRecord_IsEntryExists_ByUUID_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id);
		rec.setUuid(uuid);
		rec.setStartTime(startTimestamp);
		rec.setOs(os);
		rec.setWorkerName(workerName);
		rec.setTestsType(type);
		rec.setTestStatus(TestStatus.TestsStarted);
		
		boolean result = cache.add(rec);
		Assert.assertTrue(true == result, "Check ADD operation result.");
		Assert.assertTrue(1 == cache.Size(), "Check cache size.");
		Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		
		result = cache.isEntryExists_ByUUID(rec);
		Assert.assertTrue(true == result, "Record should exists.");
	}
	
	@Test
	public void AutotestsRecord_Get_Tests() throws Exception {
		TableCache<AutotestsRecord> cache = new TableCache<AutotestsRecord>();
		Assert.assertTrue(cache.isEmpty(), "Table should be empty");
		
		for (int i = 1; i <= 100; i++) {
			AutotestsRecord rec = new AutotestsRecord();
			rec.setId(id + i);
			rec.setUuid(uuid + i);
			rec.setStartTime(startTimestamp);
			rec.setOs(os + i);
			rec.setWorkerName(workerName + i);
			rec.setTestsType(type);
			rec.setTestStatus(TestStatus.TestsStarted);
			
			boolean result = cache.add(rec);
			Assert.assertTrue(true == result, "Check ADD operation result.");
			assertEquals(i, cache.Size(), "Check cache size");
			Assert.assertTrue(false == cache.isEmpty(), "Check is table empty.");
		}

		
		AutotestsRecord rec = new AutotestsRecord();
		rec.setId(id + 5);
		rec.setUuid(uuid + 5);
		rec.setStartTime(startTimestamp);
		rec.setOs(os + 5);
		rec.setWorkerName(workerName + 5);
		rec.setTestsType(type);
		rec.setTestStatus(TestStatus.TestsStarted);
		
		final AutotestsRecord recResult = cache.get(rec);
		Assert.assertTrue(null != recResult, "We should have some results");
	}	
}