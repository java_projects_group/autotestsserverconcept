package unit_test.json;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.Test;
import consumer.JSONRequestParserBase;
import consumer.MessageFormatExeption;
import consumer.TestResult;

class TestJSONRequestParserBase extends JSONRequestParserBase {
	/** **/
	protected JSONObject jsonMessage = null;
	
	public TestJSONRequestParserBase(final String json) throws MessageFormatExeption {
		try {
			this.jsonMessage = (JSONObject) (new JSONParser().parse(json));
		} catch (ParseException exc) {
			throw new MessageFormatExeption(exc);
		}
	}
	
	public String getType() throws MessageFormatExeption {
		return this.getType(jsonMessage);
	}
	public String getWorkerName() throws MessageFormatExeption {
		return this.getWorkerName(jsonMessage);
	}
	public String getOS() throws MessageFormatExeption {
		return this.getOS(jsonMessage);
	}
	public Boolean getIsNightly() throws MessageFormatExeption {
		return this.getIsNightly(jsonMessage);
	}
	public String getUUID() throws MessageFormatExeption {
		return this.getUUID(jsonMessage);
	}
	public String getBrowserVersion() throws MessageFormatExeption {
		return this.getBrowserVersion(jsonMessage);
	}
	public String getRevision() throws MessageFormatExeption {
		return this.getRevision(jsonMessage);
	}
	public String getStatus() throws MessageFormatExeption {
		return this.getStatus(jsonMessage);
	}
	public String getTestsStatus() throws MessageFormatExeption {
		return this.getTestsStatus(jsonMessage);
	}
	public String getBranch() throws MessageFormatExeption {
		return this.getBranch(jsonMessage);
	}
	public List<String> getOwners() throws MessageFormatExeption {
		return this.getOwners(jsonMessage);
	}
	public String getRepository() throws MessageFormatExeption {
		return this.getRepository(jsonMessage);
	}
	public long getBuildnumber() throws MessageFormatExeption {
		return this.getBuildnumber(jsonMessage);
	}	
	public String getStartTimestampAsString() throws MessageFormatExeption {
		return this.getStartTimestampAsString(jsonMessage);
	}
	public LocalDateTime getStartTimestamp() throws MessageFormatExeption {
		return this.getStartTimestamp(jsonMessage);
	}	
	public String getEndTimestampAsString() throws MessageFormatExeption {
		return this.getEndTimestampAsString(jsonMessage);
	}
	public LocalDateTime getEndTimestamp() throws MessageFormatExeption {
		return this.getEndTimestamp(jsonMessage);
	}
	public TestResult getTestsResult() throws MessageFormatExeption {
		return this.getTestsResult(jsonMessage);
	}
}

/////////////////////////////////////////// TESTS ////////////////////////////////////////////////////////////

public class JsonParaserBase_Tests extends Assert {

	@Test
	public void GetType_Test() throws Exception {
		final String json = "{\"type\": \"UpdaterTests\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String type = jsonParser.getType();
		
		assertEquals(type, "UpdaterTests");
	}
	
	@Test
	public void GetWorkerName_Test() throws Exception {
		final String json = "{\"worker_name\": \"SomeRandomWOrkerName\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String worker_name = jsonParser.getWorkerName();
		
		assertEquals(worker_name, "SomeRandomWOrkerName");
	}
	
	@Test
	public void GetOS_Test() throws Exception {
		final String json = "{\"os\": \"WindowsXXX\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String os = jsonParser.getOS();
		
		assertEquals(os, "WindowsXXX");
	}	
	
	@Test
	public void IsNightly_False_Test() throws Exception {
		final String json = "{\"is_nightly\": false}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final Boolean is_nightly = jsonParser.getIsNightly();
		
		assertEquals(is_nightly, Boolean.FALSE);
	}
	
	@Test
	public void IsNightly_True_Test() throws Exception {
		final String json = "{\"is_nightly\": true}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final Boolean is_nightly = jsonParser.getIsNightly();
		
		assertEquals(is_nightly, Boolean.TRUE);
	}
	
	@Test
	public void GetUUID_Test() throws Exception {
		final String json = "{ \"uuid\": \"dd03a85b-24f9-4966-b491-e5d5649a4be6\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String uuid = jsonParser.getUUID();
		
		assertEquals(uuid,"dd03a85b-24f9-4966-b491-e5d5649a4be6");
	}
	
	@Test
	public void GetBrowserVersion_Test() throws Exception {
		final String json = "{ \"browser_version\": \"4.0.0.42\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String browser_version = jsonParser.getBrowserVersion();
		
		assertEquals(browser_version, "4.0.0.42");
	}
	
	@Test
	public void GetRevision_Test() throws Exception {
		final String json = "{ \"revision\": \"232134123123232132323\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String revision = jsonParser.getRevision();
		
		assertEquals(revision, "232134123123232132323");
	}
	
	@Test
	public void GetStatus_Test() throws Exception {
		final String json = "{ \"status\": \"232134123123232132323\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String status = jsonParser.getStatus();
		
		assertEquals(status, "232134123123232132323");
	}	
	
	@Test
	public void GetTestsStatus_Test() throws Exception {
		final String json = "{ \"tests_status\": \"TestsCompleted\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String testsStatus = jsonParser.getTestsStatus();
		
		assertEquals(testsStatus, "TestsCompleted");
	}	

	@Test
	public void GetBranch_Test() throws Exception {
		final String json = "{ \"branch\": \"atom_5.1.2.34\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String testsStatus = jsonParser.getBranch();
		
		assertEquals(testsStatus, "atom_5.1.2.34");
	}
	
	@Test
	public void GetOwners_Test() throws Exception {
		List<String> ownersExpected = new ArrayList<String>(Arrays.asList("Owner1", "Owner2", "Owner3"));
		final String json = "{\"owners\": [\"Owner1\", \"Owner2\", \"Owner3\"]}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final List<String> owners = jsonParser.getOwners();
		
		Assert.assertTrue(owners.containsAll(ownersExpected));
		Assert.assertTrue(ownersExpected.containsAll(owners));
	}
	
	@Test
	public void GetRepository_Test() throws Exception {
		final String json = "{ \"repository\": \"git@gitlab.corp.mail.ru:browser/browser.git\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String repository = jsonParser.getRepository();
		
		assertEquals(repository, "git@gitlab.corp.mail.ru:browser/browser.git");
	}

	@Test
	public void GetBuildnumber_Test() throws Exception {
		final String json = "{ \"buildnumber\": 123456}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		long repository = jsonParser.getBuildnumber();
		
		assertEquals(repository, 123456);
	}
	
	@Test
	public void GetStartTimestampAsString_Test() throws Exception {
		final String json = "{ \"start_time\": \"2019-08-28 10:02:29\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String start_time = jsonParser.getStartTimestampAsString();
		
		assertEquals(start_time, "2019-08-28 10:02:29");
	}
	
	@Test
	public void GetStartTimestamp_Test() throws Exception {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		final String json = "{ \"start_time\": \"2019-08-28 10:02:29\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		LocalDateTime start_time = jsonParser.getStartTimestamp();
		
		assertEquals(start_time, LocalDateTime.parse("2019-08-28 10:02:29", dateTimeFormatter));
	}

	
	@Test
	public void GetEndTimestampAsString_Test() throws Exception {
		final String json = "{ \"end_time\": \"2019-08-28 10:02:29\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		final String end_time = jsonParser.getEndTimestampAsString();
		
		assertEquals(end_time, "2019-08-28 10:02:29");
	}
	
	@Test
	public void GetEndTimestamp_Test() throws Exception {
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		final String json = "{ \"end_time\": \"2019-08-28 10:02:29\"}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		LocalDateTime end_time = jsonParser.getEndTimestamp();
		
		assertEquals(end_time, LocalDateTime.parse("2019-08-28 10:02:29", dateTimeFormatter));
	}
	
	@Test
	public void GetTestResult_Tests() throws Exception {
		final String json = "{\"result\": {\"passed\": 18, \"failed\": 7, \"skipped\" : 5, \"flaky\" : 2}}";
		
		TestJSONRequestParserBase jsonParser = new TestJSONRequestParserBase(json);
		TestResult testResult = jsonParser.getTestsResult();
		
		assertEquals(testResult.getPassed(), 18);
		assertEquals(testResult.getFailed(), 7);
		assertEquals(testResult.getSkipped(), 5);
		assertEquals(testResult.getFlaky(), 2);
	}	
	
}
