package unit_test.db_tables.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import configuration.Configuration;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.tables.BuildsTable;

public class DatabaseTester {
	/** Database connection pool manager: **/
	protected database.connection.DatabaseConnectionPool dbConnectionPool = 
			database.connection.DatabaseConnectionPool.getInstance();
	
	/** Converts LocalDateTime to JAVA Timestamp: **/
	public Timestamp sqlTimeFromJavaTime(final LocalDateTime time) {
		if (null == time)
			return null;
		return Timestamp.valueOf(time);
	}
	
	/** Converts JAVA Timestamp to LocalDateTime: **/
	public LocalDateTime javaTimeFromSQLTime(final Timestamp timestamp) {
		if (null == timestamp)
			return null;
		return timestamp.toLocalDateTime();
	}
	
	/** Executes the SQL update statement. **/
	protected boolean executeSQLUpdate(final String sqlStatement) {
		Connection connection = null;
		PreparedStatement statement = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = connection.prepareStatement(sqlStatement);
			int result = statement.executeUpdate();
			return result > 0 ? true : false;
		} catch (SQLException exc) {
			System.err.println(exc.getMessage());
			System.err.println(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		} 
		return false;
	}
	
	/** Executes the SQL delete statement. **/
	protected boolean executeSQLDelete(final String sqlStatement) {
		Connection connection = null;
		PreparedStatement statement = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = connection.prepareStatement(sqlStatement);
			statement.executeUpdate();
			return true;
		} catch (SQLException exc) {
			System.err.println(exc.getMessage());
			System.err.println(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		} 
		return false;
	}
	
	/** Executes the SQL select query.
	 *  And fills the SelectResultSet object with data.
	 **/
	public boolean executeSQLSelect(final String sqlStatement, 
								 	   SelectResultSet set) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try { 
			connection = dbConnectionPool.getConnection();
			statement = connection.prepareStatement(sqlStatement);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Cursor paramsMap = new Cursor();
				final ResultSetMetaData metaData = resultSet.getMetaData();
				for (int i = 1; i <= metaData.getColumnCount(); i++) {
					final String name = metaData.getColumnName(i);
					final String value = resultSet.getString(i);
					paramsMap.put(name, value);
				}
				set.add(paramsMap);
			}
			return !set.isEmpty();
		} catch (final SQLException exc) {
			System.err.println(exc.getMessage());
			System.err.println(Arrays.toString(exc.getStackTrace()));
		} finally {
			try {
				if (resultSet != null)
					resultSet.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (SQLException exc) {
				exc.printStackTrace();
			}
		}
		return false;
	}
	
    public boolean checkTableExists(final String tableName) {
        final String sqlStatement =  "SHOW TABLES LIKE '" + tableName + "'";
        SelectResultSet results = new SelectResultSet();
        return executeSQLSelect(sqlStatement, results);	
	}
    
	public boolean clearTable(final String tableName) {
		return executeSQLDelete("delete from " + tableName + ";");
	}
	
	public void ReadTest() throws SQLException {
		SelectResultSet results = new SelectResultSet();
		boolean result = executeSQLSelect("select * from builds;", results);
		if (true == result) {
			String uuid = results.getFirstRecord().get("owners");
			System.out.println(uuid);
		}
		
	}
	
	public void DeleteTest() throws SQLException {
		boolean result = executeSQLUpdate("delete from buil4ds where id = 2;");
		System.out.println(result);
	}
	
	
	/////////////////////////////////////////////////////////////
	
	public static void ReadTest1() throws SQLException {
		DatabaseTester tester = new DatabaseTester();
		tester.ReadTest();
	}
	
	public static void DeleteTest1() throws SQLException {
		DatabaseTester tester = new DatabaseTester();
		tester.DeleteTest();
	}
	
	public static void AddTest() {
		
		// TODO: Move somewhere else
		System.setProperty("Log4jContextSelector", "org.apache.logging.log4j.core.async.AsyncLoggerContextSelector");
		
		BuildsTable table = BuildsTable.getTable();
		
		BuildRecord rec = new BuildRecord();
		rec.setUuid("ad03a85b-24f9-4966-b491-e5d5649a4be6");
		rec.setStatus(BuildStatus.InstallersAssembled);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository(" git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		rec.setOwners(Arrays.asList("Owner1", "Owner2", "Owner3", "Owner5"));
		
		boolean result = false;
		
		result = table.add(rec);
		//result = table.update(rec);
        
		
		System.out.println("Result = " + result );
		/*if (true == result) {
			System.out.println(rec);
		}*/

        

        //boolean result = table.isEntryKeyExists(rec);
        //System.out.println(result);
		
	}
	
	/** main :
	 * @throws SQLException */
	public static void main(String[] args) throws SQLException {
		Configuration.setPropertiesForTesting("src\\unit_test\\db_tables\\test_config.properties");
		// AddTest();
		 ReadTest1();
		// DeleteTest1();
	}
}
