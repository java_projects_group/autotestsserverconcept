package unit_test.db_tables.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

public class Cursor extends HashMap<String,String> {
	/** **/
	private static final long serialVersionUID = -7722047420037489026L;
	
	private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d H:m:s");

	public int getInt(final String name) {
		return Integer.valueOf(this.get(name)).intValue();
	}
	
	public long getLong(final String name) {
		return Long.valueOf(this.get(name)).longValue();
	}
	
	public LocalDateTime getTime(final String name) {
		if (null == this.get(name))
			return null;
		return LocalDateTime.parse(this.get(name), formatter);
	}

}
