package unit_test.db_tables.utils;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectResultSet extends ArrayList<Cursor> {
	/** **/
	private static final long serialVersionUID = 2714372803592718498L;
	
	public Cursor getFirstRecord() {
		if (true == this.isEmpty())
			return null;
		return this.get(0);
	}
}