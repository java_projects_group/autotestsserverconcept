package unit_test.db_tables;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import configuration.Configuration;
import database.entry.AutotestsRecord;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.entry.TestStatus;
import database.entry.TestType;
import database.tables.AutotestsTable;
import database.tables.BuildsTable;
import unit_test.db_tables.utils.Cursor;
import unit_test.db_tables.utils.DatabaseTester;
import unit_test.db_tables.utils.SelectResultSet;

public class AutotestsTable_Tests {
	/** DatabaseTester. **/
	protected DatabaseTester tester = null;
	/** BuildsTable. **/
	protected AutotestsTable table = null;
	/** **/
	private static final String testConfigFile = "src\\unit_test\\db_tables\\test_config.properties";
	/** **/
	private static final String tableName = "auto_tests";
	/** **/
	final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");
	
	private static final String uuid = "aaaaaaa-bbbb-1234-5678-abcdfg12345";
	
	@BeforeClass
	private void setUp() throws IOException {
		/** Define database for testsing. **/
		
		/** Check that auto_tests table exists in database defined in testConfigFile file. **/
		Configuration.setPropertiesForTesting(AutotestsTable_Tests.testConfigFile);
		
		tester = new DatabaseTester();
		boolean result = tester.checkTableExists(tableName);
		Assert.assertEquals(true, result, "Table " + tableName + " do not exists.");
		
		result = tester.clearTable(tableName);
		Assert.assertEquals(true, result, "Failed to clear " + tableName + " table");
		
		this.table = AutotestsTable.getTable();
	}	
	
	@AfterTest
	void tearDown() {
		// TODO. This will be called after tests
	}
	
	@Test
    public void AddRecord() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(261);
		record.setUuid(AutotestsTable_Tests.uuid);
		record.setTestsType(TestType.UpdaterTests);
		record.setTestStatus(TestStatus.TestsStarted);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setOs("Windows-8.1-6.3.9600-SP0");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		// record.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.add(record);
		Assert.assertEquals(result, true, "Table update should succeed");
		
		SelectResultSet results = new SelectResultSet();
		result = tester.executeSQLSelect("select * from " + tableName + " where uuid = '" + AutotestsTable_Tests.uuid + "';", results);
		Assert.assertEquals(true, result);
		
		final Cursor cursor = results.getFirstRecord();
		
		Assert.assertEquals(cursor.getLong("build_id"), 261, "Checking build_id field");
		Assert.assertEquals(cursor.get("uuid"), AutotestsTable_Tests.uuid, "Checking UUIDs");
		Assert.assertEquals(cursor.get("test_type"), TestType.UpdaterTests.toString(), "Checking test_type field value");
		Assert.assertEquals(cursor.get("status"), BuildStatus.TestsStarted.toString(), "Checking tests status field value");
		Assert.assertEquals(cursor.get("worker_name"), "UpdateInstallTesterWin8x64", "Checking worker_name field value");
		Assert.assertEquals(cursor.get("os"), "Windows-8.1-6.3.9600-SP0", "Checking OS field value");
		Assert.assertEquals(cursor.get("results"), "{\"param\":\"value1\"}", "Checking results field value");
		Assert.assertEquals(cursor.getTime("start_time"), LocalDateTime.parse("10/10/2019 08:22:33", formatter), "Checking start_time field value");
		Assert.assertEquals(cursor.getTime("end_time"), null, "Checking end_time field value");
    }
	
	@Test(enabled = true)
    public void UpdateRecord() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(261);
		record.setUuid(AutotestsTable_Tests.uuid);
		record.setTestsType(TestType.UpdaterTests);
		record.setTestStatus(TestStatus.TestsCompleted);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setOs("Windows-8.1-6.3.9600-SP0");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		record.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.update(record);
		Assert.assertEquals(result, true, "Table update should succeed");
		
		SelectResultSet results = new SelectResultSet();
		result = tester.executeSQLSelect("select * from " + tableName + " where uuid = '" + AutotestsTable_Tests.uuid + "';", results);
		Assert.assertEquals(true, result);
		
		final Cursor cursor = results.getFirstRecord();
		
		Assert.assertEquals(cursor.getLong("build_id"), 261, "Checking build_id field");
		Assert.assertEquals(cursor.get("uuid"), AutotestsTable_Tests.uuid, "Checking UUIDs");
		Assert.assertEquals(cursor.get("test_type"), TestType.UpdaterTests.toString(), "Checking test_type field value");
		Assert.assertEquals(cursor.get("status"), BuildStatus.TestsCompleted.toString(), "Checking tests status field value");
		Assert.assertEquals(cursor.get("worker_name"), "UpdateInstallTesterWin8x64", "Checking worker_name field value");
		Assert.assertEquals(cursor.get("os"), "Windows-8.1-6.3.9600-SP0", "Checking OS field value");
		Assert.assertEquals(cursor.get("results"), "{\"param\":\"value1\"}", "Checking results field value");
		Assert.assertEquals(cursor.getTime("start_time"), LocalDateTime.parse("10/10/2019 08:22:33", formatter), "Checking start_time field value");
		Assert.assertEquals(cursor.getTime("end_time"), LocalDateTime.parse("10/10/2019 19:33:55", formatter), "Checking end_time field value");
    }
	
	@Test(enabled = true)
    public void UpdateRecord_Failed_Wrong_ID() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(262);
		record.setUuid(AutotestsTable_Tests.uuid);
		record.setTestsType(TestType.UpdaterTests);
		record.setTestStatus(TestStatus.TestsCompleted);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setOs("Windows-8.1-6.3.9600-SP0");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		record.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.update(record);
		Assert.assertEquals(result, false, "Table update should fail.");
    }
	
	@Test(enabled = true)
    public void UpdateRecord_Failed_Wrong_UUid() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(261);
		record.setUuid("aaaaaaa-bbbb-1234-5678-abcdfg55555");
		record.setTestsType(TestType.UpdaterTests);
		record.setTestStatus(TestStatus.TestsCompleted);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setOs("Windows-8.1-6.3.9600-SP0");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		record.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.update(record);
		Assert.assertEquals(result, false, "Table update should fail.");
    }
	
	@Test(enabled = true)
    public void UpdateRecord_Failed_Wrong_Type() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(261);
		record.setUuid(AutotestsTable_Tests.uuid);
		record.setTestsType(TestType.InstallerAVTests);
		record.setTestStatus(TestStatus.TestsCompleted);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setOs("Windows-8.1-6.3.9600-SP0");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		record.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.update(record);
		Assert.assertEquals(result, false, "Table update should fail.");
    }
	
	@Test(enabled = true)
    public void UpdateRecord_Failed_Wrong_WorkerName() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(261);
		record.setUuid(AutotestsTable_Tests.uuid);
		record.setTestsType(TestType.UpdaterTests);
		record.setTestStatus(TestStatus.TestsCompleted);
		record.setWorkerName("UpdateInstallTesterWin7x64");
		record.setOs("Windows-8.1-6.3.9600-SP0");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		record.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.update(record);
		Assert.assertEquals(result, false, "Table update should fail.");
    }
	
	@Test(enabled = true)
    public void UpdateRecord_Failed_Wrong_OS() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(261);
		record.setUuid(AutotestsTable_Tests.uuid);
		record.setTestsType(TestType.UpdaterTests);
		record.setTestStatus(TestStatus.TestsCompleted);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setOs("Windows-7.1-6.3.9612-SP4");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		record.setEndTime(LocalDateTime.parse("10/10/2019 22:33:55", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.update(record);
		Assert.assertEquals(result, false, "Table update should fail.");
    }
	
	@Test(enabled = true)
    public void UpdateRecord_Failed_Wrong_StartTime() throws Exception {
		AutotestsRecord record  = new AutotestsRecord();
		record.setId(261);
		record.setUuid(AutotestsTable_Tests.uuid);
		record.setTestsType(TestType.UpdaterTests);
		record.setTestStatus(TestStatus.TestsCompleted);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setOs("Windows-8.1-6.3.9600-SP0");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:00", formatter));
		
		boolean result = table.update(record);
		Assert.assertEquals(result, false, "Table update should fail.");
    }	
}
