package unit_test.db_tables;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import configuration.Configuration;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.tables.BuildsTable;
import unit_test.db_tables.utils.Cursor;
import unit_test.db_tables.utils.DatabaseTester;
import unit_test.db_tables.utils.SelectResultSet;

public class BuildsTests extends Assert {
	/** DatabaseTester. **/
	protected DatabaseTester tester = null;
	/** BuildsTable. **/
	protected BuildsTable table = null;
	/** **/
	private static final String testConfigFile = "src\\unit_test\\db_tables\\test_config.properties";
	/** **/
	private static final String tableName = "builds";

	@BeforeClass
	private void setUp() throws IOException {
		/** Define database for testsing. **/
		
		/** Check that builds table exists in database defined in testConfigFile file. **/
		Configuration.setPropertiesForTesting(BuildsTests.testConfigFile);
		
		tester = new DatabaseTester();
		boolean result = tester.checkTableExists(tableName);
		Assert.assertEquals(true, result, "Table " + tableName + " do not exists.");
		
		result = tester.clearTable(tableName);
		Assert.assertEquals(true, result, "Failed to clear " + tableName + " table");
		
		this.table = BuildsTable.getTable();
	}	
	
	@AfterTest
	void tearDown() {
		// TODO. This will be called after tests
	}	
	
	@Test
    public void AddBuildRecods() throws Exception {
		BuildRecord rec = new BuildRecord();
		rec.setUuid("ad03a85b-24f9-4966-b491-e5d5649a4be6");
		rec.setStatus(BuildStatus.InstallersAssembled);
		rec.setBuildnumber(233);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.1");
		rec.setBranch("atom_2.34");
		rec.setRepository("git@gitlab.corp.mail.ru:browser/browser.git1");
		rec.setRevision("8f4b42241b363482450a55cb3c0d2c9d76dc04d6");
		rec.setOwners(Arrays.asList("Owner1", "Owner2", "Owner3", "Owner5"));

		boolean result = table.add(rec);
		Assert.assertEquals(true, result);
		
		SelectResultSet results = new SelectResultSet();
		result = tester.executeSQLSelect("select * from builds where uuid = 'ad03a85b-24f9-4966-b491-e5d5649a4be6';", results);
		Assert.assertEquals(true, result);
		
		final Cursor cursor = results.getFirstRecord();
		
		Assert.assertEquals(cursor.getLong("id"), rec.getId(), "Checking UUIDs");
		Assert.assertEquals(cursor.get("uuid"), "ad03a85b-24f9-4966-b491-e5d5649a4be6", "Checking UUIDs");
		Assert.assertEquals(cursor.getInt("buildnumber"), 233, "Checking buildnumber");
		Assert.assertEquals(cursor.get("browser_version"), "6.0.0.1", "Checking browser_version field value");
		Assert.assertEquals(cursor.get("branch"), "atom_2.34", "Checking branch field value");
		Assert.assertEquals(cursor.get("status"), BuildStatus.InstallersAssembled.toString(), "Checking the Build's status");
		Assert.assertEquals(cursor.get("repository"), "git@gitlab.corp.mail.ru:browser/browser.git1", "Checking repository field value");
		Assert.assertEquals(cursor.get("revision"), "8f4b42241b363482450a55cb3c0d2c9d76dc04d6", "Checking revision field value");
		Assert.assertEquals(cursor.get("owners"), "[\"Owner1\",\"Owner2\",\"Owner3\",\"Owner5\"]", "Checking owners field value");
    }
	
	
	@Test
    public void UpdateBuildRecods() throws Exception {
		BuildRecord rec = new BuildRecord();
		rec.setUuid("ad03a85b-24f9-4966-b491-e5d5649a4be6");
		rec.setStatus(BuildStatus.TestsStarted);
		rec.setBuildnumber(12331);
		rec.setNightly(true);
		rec.setBrowserVersion("6.0.0.11");
		rec.setBranch("11atom_2.34");
		rec.setRepository("git@gitlab.corp.mail.ru:browser/browser.git123");
		rec.setRevision("3333333333333333333333333");
		rec.setOwners(Arrays.asList("Owner1", "Owner2", "Owner3", "Owner4", "Owner5"));

		boolean result = table.update(rec);
		Assert.assertEquals(true, result);
		
		SelectResultSet results = new SelectResultSet();
		result = tester.executeSQLSelect("select * from builds where uuid = 'ad03a85b-24f9-4966-b491-e5d5649a4be6';", results);
		Assert.assertEquals(true, result);
		
		final Cursor cursor = results.getFirstRecord();
		
		Assert.assertEquals(cursor.getLong("id"), rec.getId(), "Checking UUIDs");
		Assert.assertEquals(cursor.get("uuid"), "ad03a85b-24f9-4966-b491-e5d5649a4be6", "Checking UUIDs");
		Assert.assertEquals(cursor.getInt("buildnumber"), 12331, "Checking buildnumber");
		Assert.assertEquals(cursor.get("browser_version"), "6.0.0.11", "Checking browser_version field value");
		Assert.assertEquals(cursor.get("branch"), "11atom_2.34", "Checking branch field value");
		Assert.assertEquals(cursor.get("status"), BuildStatus.TestsStarted.toString(), "Checking the Build's status");
		Assert.assertEquals(cursor.get("repository"), "git@gitlab.corp.mail.ru:browser/browser.git123", "Checking repository field value");
		Assert.assertEquals(cursor.get("revision"), "3333333333333333333333333", "Checking revision field value");
		Assert.assertEquals(cursor.get("owners"), "[\"Owner1\",\"Owner2\",\"Owner3\",\"Owner4\",\"Owner5\"]", "Checking owners field value");
    }	
}
