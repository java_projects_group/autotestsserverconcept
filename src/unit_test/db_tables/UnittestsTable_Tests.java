package unit_test.db_tables;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import configuration.Configuration;
import database.entry.UnitTestsRecord;
import database.entry.BuildRecord;
import database.entry.BuildStatus;
import database.entry.TestStatus;
import database.entry.TestType;
import database.tables.BuildsTable;
import database.tables.UnitTestsTable;
import unit_test.db_tables.utils.Cursor;
import unit_test.db_tables.utils.DatabaseTester;
import unit_test.db_tables.utils.SelectResultSet;

public class UnittestsTable_Tests {
	/** DatabaseTester. **/
	protected DatabaseTester tester = null;
	/** BuildsTable. **/
	protected UnitTestsTable table = null;
	/** **/
	private static final String testConfigFile = "src\\unit_test\\db_tables\\test_config.properties";
	/** **/
	private static final String tableName = "unit_tests";
	/** **/
	final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("M/d/yyyy H:m:s");
	
	private static final String uuid = "aaaaaaa-bbbb-1234-5678-abcdfg12345";
	
	@BeforeClass
	private void setUp() throws IOException {
		/** Define database for testsing. **/
		
		/** Check that auto_tests table exists in database defined in testConfigFile file. **/
		Configuration.setPropertiesForTesting(UnittestsTable_Tests.testConfigFile);
		
		tester = new DatabaseTester();
		boolean result = tester.checkTableExists(tableName);
		Assert.assertEquals(true, result, "Table " + tableName + " do not exists.");
		
		result = tester.clearTable(tableName);
		Assert.assertEquals(true, result, "Failed to clear " + tableName + " table");
		
		this.table = UnitTestsTable.getTable();
	}	
	
	@AfterTest
	void tearDown() {
		// TODO. This will be called after tests
	}
	
	@Test
    public void AddRecord() throws Exception {
		UnitTestsRecord record  = new UnitTestsRecord();
		record.setId(261);
		record.setUuid(UnittestsTable_Tests.uuid);
		record.setWorkerName("UpdateInstallTesterWin8x64");
		record.setStartTime(LocalDateTime.parse("10/10/2019 11:22:33", formatter));
		record.setEndTime(LocalDateTime.parse("10/10/2019 14:11:22", formatter));
		record.setResultJson("{\"param\":\"value1\"}");
		
		boolean result = table.add(record);
		Assert.assertEquals(result, true, "Table update should succeed");
		
		SelectResultSet results = new SelectResultSet();
		result = tester.executeSQLSelect("select * from " + tableName + " where uuid = '" + UnittestsTable_Tests.uuid + "';", results);
		Assert.assertEquals(true, result);
		
		final Cursor cursor = results.getFirstRecord();
		
		Assert.assertEquals(cursor.getLong("build_id"), 261, "Checking build_id field");
		Assert.assertEquals(cursor.get("uuid"), UnittestsTable_Tests.uuid, "Checking UUIDs");
		Assert.assertEquals(cursor.get("worker_name"), "UpdateInstallTesterWin8x64", "Checking worker_name field value");
		Assert.assertEquals(cursor.get("results"), "{\"param\":\"value1\"}", "Checking results field value");
		Assert.assertEquals(cursor.getTime("start_time"), LocalDateTime.parse("10/10/2019 08:22:33", formatter), "Checking start_time field value");
		Assert.assertEquals(cursor.getTime("end_time"), LocalDateTime.parse("10/10/2019 11:11:22", formatter), "Checking end_time field value");
    }
}
