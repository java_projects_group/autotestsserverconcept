//============================================================================
// Name        : Configuration.java
// Created on  : October 31, 2019
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Configuration class
//============================================================================

package configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/** @class DefaultValues : */
class DefaultValues {
	/** Database server host IP address. **/
	public static final String dbHost = "100.99.4.54";
	
	/** Database server connection port. **/	
	public static final int dbPort = 3306;
	
	/** Database scheme name. **/	
	public static final String dbScheme = "autotesting";
	
	/** Database authorization user name. **/
	public static final String dbUserLogin = "tester";
	
	/** Database authorization password. **/
	public static final String dbUserPassword = "tester";

	/** Kafka cluster nodes list default value: **/
	public static final String kafkaNodesList = "[100.99.4.168:9092]";	
	
	/** Kafka cluster test management topic default value: **/
	public static final String kafkaTestManagementTopic = "kafka.test_management_topic";	
	
	/** Kafka cluster test statistics topic default value:**/
	public static final String kafkaTestStatisticsTopic = "kafka.test_statistics_topic";
	
	/** SMTP server host default value. **/	
	public static final String smtpDefaultHost = "smtp.yandex.ru";
	
	/** SMTP server port default value. **/	
	public static final int smtpDefaultPort = 465;
	
	/** Mail DEBUG switch flag default value. **/	
	public static final boolean smtpDefaultDebug = false;
	
	/** SMTP authorization default value. **/	
	public static final boolean smtpDefaultAuth = false;
	
	/** SMTP java socket factory port default value. **/	
	public static final int smtpDefaultSocketFactoryPort = 465;
	
	/** SMTP java socket factory class default value. **/	
	public static final String smtpDefaultSocketFactoryClass = "mail.smtp.socketFactory.class";
	
	/** SMTP java socket factory fallback default value. **/	
	public static final boolean smtpDefaultSocketFactoryFallback = false;
}

/** @class PropertiesNames : */
class PropertiesNames {
	/** Database server host IP parameter name: **/
	public static final String dbHostParamName = "db.host";
	
	/** Database server connection port parameter name: **/
	public static final String dbPortParamName = "db.port";
	
	/** Database scheme parameter name: **/
	public static final String dbSchemeParamName = "db.scheme";
	
	/** Database authorization user name: **/
	public static final String dbLoginParamName = "db.login";
	
	/** Database authorization password parameter name: **/
	public static final String dbPasswordParamName = "db.password";
	
	/** Kafka cluster nodes list parameter name: **/
	public static final String kafkaNodesListParamName = "kafka.nodes";	
	
	/** Kafka cluster test management topic parameter name: **/
	public static final String kafkaTestManagementTopicParamName = "kafka.test_management_topic";	
	
	/** Kafka cluster test statistics topic parameter name: **/
	public static final String kafkaTestStatisticsTopicParamName = "kafka.test_statistics_topic";
	
	/** SMTP server host parameter name. **/
	public static final String smtpHostParameterName = "mail.smtp.host";
	
	/** SMTP server port parameter name. **/
	public static final String smtpPortParameterName = "mail.smtp.port";
	
	/** Mail DEBUG swith flag parameter name. **/
	public static final String mailDebugParameterName = "mail.debug";
	
	/** SMTP authorization parameter name. **/
	public static final String smtpAuthParameterName = "mail.smtp.auth";
	
	/** SMTP java socket factory port parameter name. **/
	public static final String smtpSocketFactoryPortParameterName = "mail.smtp.socketFactory.port";
	
	/** SMTP java socket factory class parameter name. **/
	public static final String smtpSocketFactoryClassParameterName = "mail.smtp.socketFactory.class";
	
	/** SMTP java socket factory fallback parameter name. **/
	public static final String smtpSocketFactoryFallbackParameterName = "mail.smtp.socketFactory.fallback";	
}

/** @class Configuration : */
public class Configuration {
	/** Configuration static instance. **/
	private static Configuration __instance = null;
	/** Default configuration file name :**/
	private static String DEFAULT_PROPERTIES_FILE = "src/config.properties";
	/** Properties: **/
	protected Properties property = new Properties();
	/** **/
	protected List<String> kafkaNodes = new ArrayList<String>();

	/** Configuration class constructor: **/
	protected Configuration(final String propFile) {
		try {
			FileInputStream fileInputStream = new FileInputStream(propFile);
			property.load(fileInputStream);
		}  catch (final IOException exc) {
			System.err.println("Failed to read file " + propFile);
		}
	}
	
	/** JUST FOR TESTING PURPOSES !!!!!!!!!!! **/
	public static void setPropertiesForTesting(final String propsFile) {
		Configuration.DEFAULT_PROPERTIES_FILE = propsFile;
	}

	/**  **/  
	public static Configuration getConfiguration() {
		if (null == __instance) {
			synchronized (Configuration.class) {
				if (null == __instance) {
					__instance = new Configuration(Configuration.DEFAULT_PROPERTIES_FILE);
				}
			}
		}
		return __instance;
	}

	public String getDbHost() {
		final String value = property.getProperty(PropertiesNames.dbHostParamName);
		return null == value ? DefaultValues.dbHost : value;
	}

	public int getDbPort() {
		final String value = property.getProperty(PropertiesNames.dbPortParamName);
		return null == value ? DefaultValues.dbPort : Integer.valueOf(value);
	}

	public String getDbScheme() {
		final String value = property.getProperty(PropertiesNames.dbSchemeParamName);
		return null == value ? DefaultValues.dbScheme : value;
	}
	
	public String getDbLogin() {
		final String value = property.getProperty(PropertiesNames.dbLoginParamName);
		return null == value ? DefaultValues.dbUserLogin : value;
	}

	public String getDbPassword() {
		final String value = property.getProperty(PropertiesNames.dbPasswordParamName);
		return null == value ? DefaultValues.dbUserPassword : value;
	}
	
	public List<String> getKafkaNodes() {
		String nodesStr = property.getProperty(PropertiesNames.kafkaNodesListParamName);
		if (null == nodesStr)
			return new ArrayList<String>();
		nodesStr = nodesStr.substring(1, nodesStr.length() - 1);
		return new ArrayList<String>(Arrays.asList(nodesStr.split(",")));
	}

	public String getKafkaTestManagementTopic() {
		final String value = property.getProperty(PropertiesNames.kafkaTestManagementTopicParamName);
		return null == value ? DefaultValues.kafkaTestManagementTopic : value;
	}
	
	public String getKafkaTestStatisticsTopic() {
		final String value = property.getProperty(PropertiesNames.kafkaTestStatisticsTopicParamName);
		return null == value ? DefaultValues.kafkaTestStatisticsTopic : value;
	}
	
	public String getSmtpHost() {
		final String value = property.getProperty(PropertiesNames.smtpHostParameterName);
		return null == value ? DefaultValues.smtpDefaultHost : value;
	}

	public int getSmtpPort() {
		final String value = property.getProperty(PropertiesNames.smtpPortParameterName);
		return null == value ? DefaultValues.smtpDefaultPort : Integer.valueOf(value);
	}

	public boolean isDebug() {
		final String value = property.getProperty(PropertiesNames.mailDebugParameterName);
		return null == value ? DefaultValues.smtpDefaultDebug : Boolean.valueOf(value);
	}
	
	public boolean isAuthorizationRequired() { 
		final String value = property.getProperty(PropertiesNames.smtpAuthParameterName);
		return null == value ? DefaultValues.smtpDefaultAuth : Boolean.valueOf(value);
	}

	public int getSocketFactoryPort () {
		final String value = property.getProperty(PropertiesNames.smtpSocketFactoryPortParameterName);
		return null == value ? DefaultValues.smtpDefaultSocketFactoryPort : Integer.valueOf(value);
	}	
	
	public String getSocketFactoryClass () {
		final String value = property.getProperty(PropertiesNames.smtpSocketFactoryClassParameterName);
		return null == value ? DefaultValues.smtpDefaultSocketFactoryClass : value;
	}	
	
	public boolean getSocketFactoryFallback () {
		final String value = property.getProperty(PropertiesNames.smtpSocketFactoryFallbackParameterName);
		return null == value ? DefaultValues.smtpDefaultSocketFactoryFallback : Boolean.valueOf(value);
	}	
	
	////////////////////////////////////

	public static void main(String args[]) {
		Configuration config = Configuration.getConfiguration();
		System.out.println(PropertiesNames.dbHostParamName + " = " + config.getDbHost());
		System.out.println(PropertiesNames.dbPortParamName + " = " + config.getDbPort());
		System.out.println(PropertiesNames.dbSchemeParamName + " = " + config.getDbScheme());
		System.out.println(PropertiesNames.dbLoginParamName + " = " + config.getDbLogin());
		System.out.println(PropertiesNames.dbPasswordParamName + " = " + config.getDbPassword());
		

		List<String> nodes =config.getKafkaNodes();
		for (final String node: nodes)
			System.out.println(node);
		System.out.println(PropertiesNames.kafkaTestManagementTopicParamName + " = " + config.getKafkaTestManagementTopic());
		System.out.println(PropertiesNames.kafkaTestStatisticsTopicParamName + " = " + config.getKafkaTestStatisticsTopic());
		
	}
}