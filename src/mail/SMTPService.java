package mail;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import configuration.Configuration;
import database.BrowserBuilds;
import database.entry.BuildStatus;
import database.view.AutotestsView;
import database.view.BuildView;
import database.view.UnittestsView;
import utilities.observer.IObservable;
import utilities.observer.IObserver;

/** MailIObserver class. **/
class MailObserver implements IObserver {
	/** **/
	private SMTPService smtpServive = null;
	
	/** MailObserver constructor. **/
	public MailObserver(final SMTPService smtpServive) {
		this.smtpServive = smtpServive;
	}
	
	@Override
	public void update(final IObservable observable, 
					   final Object arguments) {
		// TODO: Check type cast
		BuildView buildView = (BuildView)arguments;
		smtpServive.handleBuildChange(buildView);
	}

}

public class SMTPService {
	/** SMTPService static instance. **/
	private static SMTPService __instance = null;	
	/** **/
	protected Configuration configuration =  Configuration.getConfiguration();
	/** Application level logger. **/
	protected final Logger applicationLogger = LogManager.getLogger("ApplicationLogger");
	
	/** SMTPService class constructor: **/
	protected SMTPService() {
		// TODO. Put some logs maybe.
	}
	
	public void register() {
		BrowserBuilds.getTable().addObserver(new MailObserver(this));
	}
	
	/** Returns the existing SMTPService class instance. **/  
	public static SMTPService getService() {
		if (null == __instance) {
			synchronized (SMTPService.class) {
				if (null == __instance) {
					__instance = new SMTPService();
				}
			}
		}
		return __instance;
	}	
	
	public void handleBuildChange(final BuildView build) {
		
		this.applicationLogger.debug("SMTPService: Handling event. " + build);
		
		if (BuildStatus.BuildStarted == build.getStatus()) {
			try {
				this.SendEmailTest(build);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (BuildStatus.TestsCompleted == build.getStatus()) {
			try {
				this.SendEmailTest(build);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @throws IOException  **/
    private void SendEmailTest(final BuildView build) throws IOException
    {
        final String destAddress = "a.tokmakov@corp.mail.ru";
        final String srcAddress = "browser-build@corp.mail.ru";
        final String username = srcAddress;
        final String password = "umjkrfazivteqfme";

        Properties props = new Properties();
        props.put("mail.smtp.host", configuration.getSmtpHost());        
        props.put("mail.debug", String.valueOf(configuration.isDebug()));
        props.put("mail.smtp.auth", String.valueOf(configuration.isAuthorizationRequired()));
        props.put("mail.smtp.port", configuration.getSmtpPort());
        props.put("mail.smtp.socketFactory.port", configuration.getSocketFactoryPort());
        props.put("mail.smtp.socketFactory.class", configuration.getSocketFactoryClass());
        props.put("mail.smtp.socketFactory.fallback", String.valueOf(configuration.getSocketFactoryFallback()));

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
        		protected PasswordAuthentication getPasswordAuthentication() {
        				return new PasswordAuthentication(username, password);
             }
        });
        
        

        try {
            Message message = new MimeMessage(session);
            InternetAddress from = new InternetAddress(srcAddress);
            InternetAddress to   = new InternetAddress(destAddress);

            String message_body = "<html><body>"; 
            message_body += "        <h4>Build properties.</h4>\r\n"; 
            message_body += "        <table cellspacing=\"0\" style=\"border: 2px dashed #FB4314; width: 100%;\"> \r\n"; 
            message_body += "           <tr><th>Project name:</th><td >Atom browser</td></tr> \r\n"; 
            message_body += "			<tr><th>Status:</th><td>" + build.getStatus() + "</td></tr> \r\n"; 
            message_body += "			<tr><th>Start time:</th><td>" + build.getStartTime() + "</td></tr> \r\n";
            if (null != build.getEndTime())
            	message_body += "			<tr><th>End time:</th><td>" + build.getEndTime() + "</td></tr> \r\n";
            if (false == build.isNightly()) 
            	message_body += "			<tr><th>Builds reason:</th><td>Git commit</td></tr> \r\n";
            else
				message_body += "			<tr><th>isNightly:</th><td>" + build.isNightly() + "</td></tr> \r\n"; 
            message_body += "			<tr><th>Browser Version:</th><td>"+ build.getBrowserVersion() + "</td></tr> \r\n"; 
            message_body += "			<tr><th>Owners:</th><td>" + build.getOwners() + "</td></tr> \r\n"; 
            message_body += "			<tr><th>Branch:</th><td>"+ build.getBranch() + "</td></tr> \r\n"; 
            message_body += "			<tr><th>Repository:</th><td>"+ build.getRepository() + "</td></tr> \r\n"; 
            message_body += "			<tr><th>Revision:</th><td>"+ build.getRevision()+ "</td></tr> \r\n"; 
            message_body += "			<tr><th>Uuid:</th><td>"+ build.getUuid() + "</td></tr> \r\n"; 
            message_body += "			<tr><th>Changes:</th><td>....................</td></tr> \r\n"; 
            message_body += "        </table> \r\n"; 
            
            message_body += "       <h4>Unit tests summary.</h4>\r\n"; 
            message_body += "		<table cellspacing=\"0\" style=\"border: 2px dashed #FB4314; width: 100%;\"> \r\n"; 
	        final List<UnittestsView> unitTests = build.getUnittests();
	        for (UnittestsView utView : unitTests) {
	        	message_body += "			<tr><th>Worker: " + utView.getWorkerName() + ": </th><td align=\"left\">"; 
	            message_body += " [Passed: " + utView.getTestsPassed();
	            message_body += ", Failed, : " + utView.getTestsFailed();
	            message_body += ", Skipped : " + utView.getTestsSkipped();
	            message_body += "]</td></tr>\r\n"; 
	        }
            message_body += "        </table>"; 
            
            /** If its the commit tests, we should add the autotests block. **/
            if (false == build.isNightly()) {
                message_body += "       <h4>Auto tests summary.</h4>\r\n"; 
                message_body += "		<table cellspacing=\"0\" style=\"border: 2px dashed #FB4314; width: 90%;\"> \r\n"; 
                final List<AutotestsView> autoTests = build.getAutotests();
    	        for (final AutotestsView test : autoTests) {
    	        	message_body += "			<tr><th>Worker: " + test.getWorkerName() + ": </th><td align=\"left\">"; 
    	            message_body += " [Passed: " + test.getTestsPassed();
    	            message_body += ", Failed, : " + test.getTestsFailed();
    	            message_body += "]</td></tr>\r\n"; 
                }
                message_body += "        </table>"; 
            } 
            
            message_body += "    </body></html>";

            message.setFrom(from);
            message.setRecipient(Message.RecipientType.TO, to);
            
            message.setSubject("<TITLE> " + build.getBrowserVersion() + " results");
            message.setContent(message_body, "text/html");
            
            // message.setText(message_text);
            Transport.send(message);
        } catch (final MessagingException exc) {
        	exc.printStackTrace();
        }
    }
}
