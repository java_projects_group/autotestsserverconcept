//============================================================================
// Name        : Utiliteis.java
// Created on  : December 05, 2018
// Author      : Tokmakov Andrey
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : File list utilities test class
//============================================================================

package utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Utilities {
	/** StringWriter instance: **/
	private final static StringWriter stringWriter = new StringWriter();
	/** PrintWriter instance: **/
	private final static PrintWriter printWriter = new PrintWriter(stringWriter);
    
	/** List files : 
	 * @throws IOException **/
	public static String stackTraceToString(final Exception exception) {
	    exception.printStackTrace(printWriter);
	    return stringWriter.toString();
	}
	
	/** List files : 
	 * @throws IOException **/
	public static List<String> GetFileList(final String dirPath, 
									       boolean recursive) throws IOException {
		 File dir = new File(dirPath);  
		 ArrayList<String> files = new ArrayList<String>();
		 ListFiles(dir, files, recursive);
		 return files;
	}

	/** List files : 
	 * @throws IOException **/
	public static void ListFiles(final File folder,
								 ArrayList<String> fileList, 
								 boolean recursive) throws IOException {
	    for (final File fileEntry : folder.listFiles()) {
	        if (true == fileEntry.isDirectory()) {
	        	if (true == recursive) {
	        		ListFiles(fileEntry, fileList, recursive);
	        	}
	        } else {
	        	//System.out.println(fileEntry.getAbsolutePath());
	        	//fileList.add(fileEntry.getAbsolutePath());
	        	fileList.add(fileEntry.getPath());
	        }
	    }
	}
	
	/** List files : 
	 * @throws IOException **/
	public List<String> GetFileLines(final String fileName) throws IOException {
		ArrayList<String> lines = new ArrayList<String>();
		try (Stream<String> linesStream = Files.lines(new File(fileName).toPath())) {
			linesStream.forEach(line -> { lines.add(line); });
		}
		return lines;
	}
	
	/** List files : 
	 * @return 
	 * @throws IOException **/	
	public ArrayList<String> GetFileLinesEx(final String fileName) throws IOException {
		ArrayList<String> lines = new ArrayList<String>();
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				lines.add(line);
			}
		}
		return lines;
	}
}
